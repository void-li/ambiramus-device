#!/bin/sh
platformio project init --ide qtcreator --board nodemcuv2

echo                                                 >> platformio.pro
echo 'INCLUDEPATH += "/usr/include/qt"'              >> platformio.pro
echo 'INCLUDEPATH += "/usr/include/qt/QtCore"'       >> platformio.pro
echo 'INCLUDEPATH += "/usr/include/qt/QtHttpServer"' >> platformio.pro
echo 'INCLUDEPATH += "/usr/include/qt/QtNetwork"'    >> platformio.pro

echo                                                 >> platformio.pro

echo 'SOURCES += $$files(test/*.cpp)'                >> platformio.pro
echo 'HEADERS += $$files(test/*.hpp)'                >> platformio.pro
