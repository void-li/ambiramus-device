Import("env")

env.Append(
  LINKFLAGS = [
    '-lQt5Core',
    '-lQt5HttpServer',
    '-lQt5Network'
  ]
)
