/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <cstring>

#include "string.hpp"

using namespace lvd::ambiramus;

// ----------

#define EACH for (String* string : { &string0, &strref0 })
#define ESTR *string

// ----------

ATEST_CASE(Constructor) {
  String string0;
  ATEST_ASSERT_TR(string0.empty());
  ATEST_ASSERT_EQ(string0.size(), 0);

  String string1(3);
  ATEST_ASSERT_FA(string1.empty());
  ATEST_ASSERT_EQ(string1.size(), 3);

  String string2(3, 'X');
  ATEST_ASSERT_FA(string2.empty());
  ATEST_ASSERT_EQ(string2.size(), 3);

  String string3(STRREF("Mera Luna"));
  ATEST_ASSERT_FA(string3.empty());
  ATEST_ASSERT_EQ(string3.size(), strlen("Mera Luna"));
}

ATEST_CASE(Construct) {
  String string0 = String::construct("Mera Luna");
  ATEST_ASSERT_FA(string0.empty());
  ATEST_ASSERT_EQ(string0.size(), strlen("Mera Luna"));
}

ATEST_CASE(Size) {
  String string0 = String::construct("Mera Luna");
  String strref0 = STRREF           ("Mera Luna");

  EACH {
    const String string1 = ESTR;
    ATEST_ASSERT_EQ(string1.size(), strlen("Mera Luna"));

    const String string2;
    ATEST_ASSERT_EQ(string2.size(), 0);
  }

  ATEST_ASSERT_EQ(string0, String::construct("Mera Luna"));
  ATEST_ASSERT_EQ(strref0, String::construct("Mera Luna"));
}

ATEST_CASE(Empty) {
  String string0 = String::construct("Mera Luna");
  String strref0 = STRREF           ("Mera Luna");

  EACH {
    const String string1 = ESTR;
    ATEST_ASSERT_FA(string1.empty());

    const String string2;
    ATEST_ASSERT_TR(string2.empty());
  }

  ATEST_ASSERT_EQ(string0, String::construct("Mera Luna"));
  ATEST_ASSERT_EQ(strref0, String::construct("Mera Luna"));
}

ATEST_CASE(Data) {
  String string0 = String::construct("Mera Luna");
  String strref0 = STRREF           ("Mera Luna");

  EACH {
    const String string1 = ESTR;
    ATEST_ASSERT_EQ(strcmp(string1.data(), "Mera Luna"), 0);
  }

  ATEST_ASSERT_EQ(string0, String::construct("Mera Luna"));
  ATEST_ASSERT_EQ(strref0, String::construct("Mera Luna"));
}

ATEST_CASE(Front) {
  String string0 = String::construct("Mera Luna");
  String strref0 = STRREF           ("Mera Luna");

  EACH {
    const String string1 = ESTR;
    ATEST_ASSERT_EQ(string1.front(), 'M');

    String string2 = ESTR;
    string2.front() = 'm';
    ATEST_ASSERT_EQ(string2.front(), 'm');
  }

  ATEST_ASSERT_EQ(string0, String::construct("Mera Luna"));
  ATEST_ASSERT_EQ(strref0, String::construct("Mera Luna"));
}

ATEST_CASE(Back) {
  String string0 = String::construct("Mera Luna");
  String strref0 = STRREF           ("Mera Luna");

  EACH {
    const String string1 = ESTR;
    ATEST_ASSERT_EQ(string1.back(), 'a');

    String string2 = ESTR;
    string2.back() = 'A';
    ATEST_ASSERT_EQ(string2.back(), 'A');
  }

  ATEST_ASSERT_EQ(string0, String::construct("Mera Luna"));
  ATEST_ASSERT_EQ(strref0, String::construct("Mera Luna"));
}

ATEST_CASE(BeginAndEnd) {
  String string0 = String::construct("Mera Luna");
  String strref0 = STRREF           ("Mera Luna");

  EACH {
    const String string1 = ESTR;
    int index1 = 0;
    for (auto it = string1.begin(); it != string1.end(); it++) {
      ATEST_ASSERT_EQ(*it, string1[index1++]);
    }

    String string2 = ESTR;
    for (auto it = string2.begin(); it != string2.end(); it++) {
      *it = 'X';
    }

    ATEST_ASSERT_EQ(string2, String::construct("XXXXXXXXX"));
  }

  ATEST_ASSERT_EQ(string0, String::construct("Mera Luna"));
  ATEST_ASSERT_EQ(strref0, String::construct("Mera Luna"));
}

ATEST_CASE(Clear) {
  String string0 = String::construct("Mera Luna");
  String strref0 = STRREF           ("Mera Luna");

  EACH {
    String string1 = ESTR;
    string1.clear();
    ATEST_ASSERT_TR(string1.empty());
    ATEST_ASSERT_EQ(string1.size(), 0);
  }

  ATEST_ASSERT_EQ(string0, String::construct("Mera Luna"));
  ATEST_ASSERT_EQ(strref0, String::construct("Mera Luna"));
}

ATEST_CASE(Resize) {
  String string0 = String::construct("Mera Luna");
  String strref0 = STRREF           ("Mera Luna");

  EACH {
    String string1 = ESTR;
    string1.resize(4);
    ATEST_ASSERT_EQ(string1.size(), strlen("Mera"));
    ATEST_ASSERT_EQ(string1, String::construct("Mera" ));

    String string2 = string1;
    string2.resize(5);
    string2[4] = ' ';
    ATEST_ASSERT_EQ(string2.size(), 5);
    ATEST_ASSERT_EQ(string2, String::construct("Mera "));
  }

  ATEST_ASSERT_EQ(string0, String::construct("Mera Luna"));
  ATEST_ASSERT_EQ(strref0, String::construct("Mera Luna"));
}

ATEST_CASE(Reserve) {
  String string0 = String::construct("Mera Luna");
  String strref0 = STRREF           ("Mera Luna");

  EACH {
    String string1 = ESTR;
    string1.reserve(42);
    ATEST_ASSERT_EQ(string0.size(), strlen("Mera Luna"));
  }

  ATEST_ASSERT_EQ(string0, String::construct("Mera Luna"));
  ATEST_ASSERT_EQ(strref0, String::construct("Mera Luna"));
}

ATEST_CASE(PushBack) {
  String string0 = String::construct("Mera Luna");
  String strref0 = STRREF           ("Mera Luna");

  EACH {
    String string1 = ESTR;
    string1.push_back('X');
    ATEST_ASSERT_EQ(string1, String::construct("Mera LunaX"));
  }

  ATEST_ASSERT_EQ(string0, String::construct("Mera Luna"));
  ATEST_ASSERT_EQ(strref0, String::construct("Mera Luna"));
}

ATEST_CASE(Append) {
  String string0 = String::construct("Mera Luna");
  String strref0 = STRREF           ("Mera Luna");

  EACH {
    String string1 = ESTR;
    string1.append(String::construct("X"));
    ATEST_ASSERT_EQ(string1, String::construct("Mera LunaX"));

    String string2 = ESTR;
    string2.append(       "X" );
    ATEST_ASSERT_EQ(string2, String::construct("Mera LunaX"));
  }

  ATEST_ASSERT_EQ(string0, String::construct("Mera Luna"));
  ATEST_ASSERT_EQ(strref0, String::construct("Mera Luna"));
}

ATEST_CASE(Insert) {
  String string0 = String::construct("Mera Luna");
  String strref0 = STRREF           ("Mera Luna");

  EACH {
    String string1 = ESTR;
    string1.insert(string1.begin(), 1, 'X');
    ATEST_ASSERT_EQ(string1,  String::construct("XMera Luna" ));

    String string2 = ESTR;
    string2.insert(string2.end()-0, 1, 'X');
    ATEST_ASSERT_EQ(string2,  String::construct("Mera LunaX" ));

    String string3 = ESTR;
    string3.insert(string3.end()-2, 3, 'X');
    ATEST_ASSERT_EQ(string3, String::construct("Mera LuXXXna"));
  }

  ATEST_ASSERT_EQ(string0, String::construct("Mera Luna"));
  ATEST_ASSERT_EQ(strref0, String::construct("Mera Luna"));
}

ATEST_CASE(Replace) {
  String string0 = String::construct("Mera Luna");
  String strref0 = STRREF           ("Mera Luna");

  EACH {
    String string1 = ESTR;
    string1.replace(3, 3, const_cast<char*>("A l"));
    ATEST_ASSERT_EQ(string1, String::construct("MerA luna"));
  }

  ATEST_ASSERT_EQ(string0, String::construct("Mera Luna"));
  ATEST_ASSERT_EQ(strref0, String::construct("Mera Luna"));
}

ATEST_CASE(FindAndRfind) {
  String string0 = String::construct("Mera Luna");
  String strref0 = STRREF           ("Mera Luna");

  EACH {
    String string1 = ESTR;
    ATEST_ASSERT_EQ(string1. find("a"), 3);
    ATEST_ASSERT_EQ(string1. find("x"), std::string::npos);

    String string2 = ESTR;
    ATEST_ASSERT_EQ(string2.rfind("a"), 8);
    ATEST_ASSERT_EQ(string2.rfind("x"), std::string::npos);
  }

  ATEST_ASSERT_EQ(string0, String::construct("Mera Luna"));
  ATEST_ASSERT_EQ(strref0, String::construct("Mera Luna"));
}

ATEST_CASE(OprAccess) {
  String string0 = String::construct("Mera Luna");
  String strref0 = STRREF           ("Mera Luna");

  EACH {
    const String string1 = ESTR;
    auto strval1 = string1[0];
    ATEST_ASSERT_EQ(strval1, 'M');

    String string2 = ESTR;
    string2[8] = 'A';
    ATEST_ASSERT_EQ(string2, String::construct("Mera LunA"));
  }

  ATEST_ASSERT_EQ(string0, String::construct("Mera Luna"));
  ATEST_ASSERT_EQ(strref0, String::construct("Mera Luna"));
}

ATEST_CASE(OprPlus) {
  String string0 = String::construct("Mera Luna");
  String strref0 = STRREF           ("Mera Luna");

  EACH {
    String string1 = ESTR;
    string1 = string0 + String::construct(" Moon");
    ATEST_ASSERT_EQ(string1, String::construct("Mera Luna Moon"));
  }

  ATEST_ASSERT_EQ(string0, String::construct("Mera Luna"));
  ATEST_ASSERT_EQ(strref0, String::construct("Mera Luna"));
}

ATEST_CASE(OprPlusEqual) {
  String string0 = String::construct("Mera Luna");
  String strref0 = STRREF           ("Mera Luna");

  EACH {
    String string1 = ESTR;
    string1 += String::construct(" Moon");
    ATEST_ASSERT_EQ(string1, String::construct("Mera Luna Moon"));
  }

  ATEST_ASSERT_EQ(string0, String::construct("Mera Luna"));
  ATEST_ASSERT_EQ(strref0, String::construct("Mera Luna"));
}

ATEST_CASE(AsStdString) {
  String string0 = String::construct("Mera Luna");
  String strref0 = STRREF           ("Mera Luna");

  EACH {
    String string1 = ESTR;
    ATEST_ASSERT_TR(string1.toStdString() == "Mera Luna");
  }

  ATEST_ASSERT_EQ(string0, String::construct("Mera Luna"));
  ATEST_ASSERT_EQ(strref0, String::construct("Mera Luna"));
}

ATEST_CASE(OprEquality) {
  ATEST_ASSERT_TR(String::construct("Mera" ) == String::construct("Mera" ));
  ATEST_ASSERT_TR(String::construct("Mera" ) != String::construct("Luna" ));

  ATEST_ASSERT_TR(String(    STRREF("Mera")) == String::construct("Mera" ));
  ATEST_ASSERT_TR(String(    STRREF("Mera")) != String::construct("Luna" ));

  ATEST_ASSERT_TR(String::construct("Mera" ) == String(    STRREF("Mera")));
  ATEST_ASSERT_TR(String::construct("Mera" ) != String(    STRREF("Luna")));

  ATEST_ASSERT_TR(String(    STRREF("Mera")) == String(    STRREF("Mera")));
  ATEST_ASSERT_TR(String(    STRREF("Mera")) != String(    STRREF("Luna")));
}

// ----------

ATEST_UNIT(String);
