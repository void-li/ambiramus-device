/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <algorithm>
#include <array>
#include <cassert>
#include <cstddef>
#include <cstring>

#include <unity.h>

#include "logger.hpp"

// ----------

namespace test {
using namespace lvd::ambiramus;

class ATestCase {
 public:
  using File = const char*;
  using Func = UnityTestFunction;
  using Name = const char*;

  using Line = uint32_t;

 public:
  ATestCase() = default;
  ~ATestCase() = default;

  ATestCase(File file,
            Func func,
            Name name,
            Line line)
      : file_(file),
        func_(func),
        name_(name),
        line_(line) {}

 public:
  File file() const { return file_; }
  Func func() const { return func_; }
  Name name() const { return name_; }

  Line line() const { return line_; }

 private:
  File file_;
  Func func_;
  Name name_;

  Line line_;
};

class ATestUnit {
 public:
  using File = const char*;
  using Func = UnityTestFunction;
  using Name = const char*;

 public:
  ATestUnit() = default;
  ~ATestUnit() = default;

  ATestUnit(File file,
            Func func,
            Name name)
      : file_(file),
        func_(func),
        name_(name) {}

 public:
  File file() const { return file_; }
  Func func() const { return func_; }
  Name name() const { return name_; }

 private:
  File file_;
  Func func_;
  Name name_;
};

// ----------

#define ATEST_CASE_COUNT 150
#define ATEST_UNIT_COUNT 30

std::array<ATestCase, ATEST_CASE_COUNT>* ATEST_CASES();
std::size_t* ATEST_CASES_INDEX();

std::array<ATestUnit, ATEST_UNIT_COUNT>* ATEST_UNITS();
std::size_t* ATEST_UNITS_INDEX();

// ----------

class ATestCaseUtil {
 public:
  ATestCaseUtil(ATestCase::File file,
                ATestCase::Func func,
                ATestCase::Name name,
                ATestCase::Line line) {
    assert((*ATEST_CASES_INDEX()) < (*ATEST_CASES()).size());
    (*ATEST_CASES())[(*ATEST_CASES_INDEX())++] = ATestCase(file, func, name, line);
  }
};

class ATestUnitUtil {
 public:
  ATestUnitUtil(ATestUnit::File file,
                ATestUnit::Func func,
                ATestUnit::Name name) {
    assert((*ATEST_UNITS_INDEX()) < (*ATEST_UNITS()).size());
    (*ATEST_UNITS())[(*ATEST_UNITS_INDEX())++] = ATestUnit(file, func, name      );
  }
};

// ----------

class ATestHeapStats {
 public:
  uint32_t free = 0;
  uint16_t full = 0;
  uint8_t  frag = 0;
};

auto takeATestHeapStats() -> ATestHeapStats;
void showATestHeapStats(const ATestHeapStats& s0, const ATestHeapStats& s1);

// ----------

void printSpace();

// ----------

extern const char* __ATEST_CASE_FILTER__;
extern const char* __ATEST_UNIT_FILTER__;

#define ATEST_STR(S) ATEST_STR2(S)
#define ATEST_STR2(S) #S

#define ATEST_CASE_FILTER(CASE) ::test::__ATEST_CASE_FILTER__ = ATEST_STR(CASE)
#define ATEST_UNIT_FILTER(UNIT) ::test::__ATEST_UNIT_FILTER__ = ATEST_STR(UNIT)

// ----------

#define ATEST_CASE(T) ATEST_CASE2(T, __FILE__, __LINE__)
#define ATEST_CASE2(T, F, L) ATEST_CASE3(T, F, L)
#define ATEST_CASE3(T, F, L)                                                  \
  static void test##T();                                                      \
  static test::ATestCaseUtil aTestCaseUtil##L(F, test##T, #T, L);             \
  static void test##T()

#define ATEST_UNIT(T) ATEST_UNIT2(T, __FILE__          )
#define ATEST_UNIT2(T, F   ) ATEST_UNIT3(T, F   )
#define ATEST_UNIT3(T, F   )                                                  \
  static void T##Test();                                                      \
  static test::ATestUnitUtil aTestUnitUtil##L(F, T##Test, #T   );             \
  static void T##Test() {                                                     \
    UnitySetTestFile(__FILE__);                                               \
    for (std::size_t i = 0; i < (*test::ATEST_CASES_INDEX()); i++) {          \
      if (strcmp((*test::ATEST_CASES())[i].file(), __FILE__) == 0) {          \
        if (::test::__ATEST_CASE_FILTER__ != nullptr) {                       \
          if (strcmp(::test::__ATEST_CASE_FILTER__, (*test::ATEST_CASES())[i].name()) != 0) { \
            continue;                                                         \
          }                                                                   \
        }                                                                     \
        LOG_A("Test") << #T << (*test::ATEST_CASES())[i].name();              \
        auto s0 = ::test::takeATestHeapStats();                               \
        UnityDefaultTestRun(                                                  \
          (*test::ATEST_CASES())[i].func(),                                   \
          (*test::ATEST_CASES())[i].name(),                                   \
          (*test::ATEST_CASES())[i].line()                                    \
        );                                                                    \
        auto s1 = ::test::takeATestHeapStats();                               \
        showATestHeapStats(s0, s1);                                           \
      }                                                                       \
    }                                                                         \
  }; void* T##TestDummy

#define ATEST_MAIN                                                            \
  ::test::printSpace();                                                       \
  std::stable_sort((*test::ATEST_CASES()).begin(),                            \
                   (*test::ATEST_CASES()).begin() + (*test::ATEST_CASES_INDEX()), \
                   [](const test::ATestCase& lhs, const test::ATestCase& rhs) {   \
    return strcmp(lhs.file(), rhs.file()) < 0;                                \
  });                                                                         \
  std::stable_sort((*test::ATEST_UNITS()).begin(),                            \
                   (*test::ATEST_UNITS()).begin() + (*test::ATEST_UNITS_INDEX()), \
                   [](const test::ATestUnit& lhs, const test::ATestUnit& rhs) {   \
    return strcmp(lhs.file(), rhs.file()) < 0;                                \
  });                                                                         \
  for (std::size_t i = 0; i < (*test::ATEST_UNITS_INDEX()); i++) {            \
    if (::test::__ATEST_UNIT_FILTER__ != nullptr) {                           \
      if (strcmp(::test::__ATEST_UNIT_FILTER__, (*test::ATEST_UNITS())[i].name()) != 0) { \
        continue;                                                             \
      }                                                                       \
    }                                                                         \
    (*test::ATEST_UNITS())[i].func()();                                       \
  } (void)nullptr

// ----------

inline void ATestAssertFailure(Logger::LogProxy&& logProxy,
                               const char* exp, const String& exv) {
  logProxy << "ATEST_ASSERT failed for:"
      << Logger::NOSPACE
      << "'"
      << Logger::NOSPACE
      << exp
      << "'"
      << "with"
      << Logger::DOQUOTE
      << exp << "=" << exv
      ;
}

inline void ATestAssertFailure(Logger::LogProxy&& logProxy,
                               const char* lhs, const String& lhv,
                               const char* opr,
                               const char* rhs, const String& rhv) {
  logProxy << "ATEST_ASSERT failed for:"
      << Logger::NOSPACE
      << "'"
      << lhs
      << opr
      << Logger::NOSPACE
      << rhs
      << "'"
      << "with"
      << Logger::DOQUOTE
      << lhs << "=" << lhv
      << "and"
      << "with"
      << Logger::DOQUOTE
      << rhs << "=" << rhv
      ;
}

// ----------

#define ATEST_STRING(S) #S

// ----------

#define ATEST_ASSERT_IMPL_A0(N, O)                                            \
  template <class EXP>                                                        \
  bool N(Logger::LogProxy&& logProxy,                                         \
         EXP&& exp, const char* expStr) {                                     \
    if (!O(exp)) {                                                            \
      ATestAssertFailure(std::move(logProxy),                                 \
                         expStr, toString(exp));                              \
      return false;                                                           \
    }                                                                         \
    return true;                                                              \
  }

#define ATEST_ASSERT_IMPL_A1(N, EXP) {                                        \
  if (!test::N(LOG_A("TEST"),                                                 \
               EXP, ATEST_STRING(EXP))) {                                     \
    TEST_FAIL();                                                              \
    return;                                                                   \
  }                                                                           \
}

ATEST_ASSERT_IMPL_A0(ATestAssertTR,  )
#define ATEST_ASSERT_TR(EXP) \
  ATEST_ASSERT_IMPL_A1(ATestAssertTR, EXP)

ATEST_ASSERT_IMPL_A0(ATestAssertFA, !)
#define ATEST_ASSERT_FA(EXP) \
  ATEST_ASSERT_IMPL_A1(ATestAssertFA, EXP)

// ----------

#define ATEST_ASSERT_IMPL_B0(N, O)                                            \
  template <class LHS, class RHS>                                             \
  bool N(Logger::LogProxy&& logProxy,                                         \
         LHS&& lhs, const char* lhsStr,                                       \
         RHS&& rhs, const char* rhsStr) {                                     \
    if (! (lhs O rhs)) {                                                      \
      ATestAssertFailure(std::move(logProxy),                                 \
                         lhsStr, toString(lhs),                               \
                         #O,                                                  \
                         rhsStr, toString(rhs));                              \
      return false;                                                           \
    }                                                                         \
    return true;                                                              \
  }

#define ATEST_ASSERT_IMPL_B1(N, LHS, RHS) {                                   \
  if (!test::N(LOG_A("TEST"),                                                 \
               LHS, ATEST_STRING(LHS),                                        \
               RHS, ATEST_STRING(RHS))) {                                     \
    TEST_FAIL();                                                              \
    return;                                                                   \
  }                                                                           \
}

ATEST_ASSERT_IMPL_B0(ATestAssertEQ, ==)
#define ATEST_ASSERT_EQ(LHS, RHS) \
  ATEST_ASSERT_IMPL_B1(ATestAssertEQ, LHS, RHS)

ATEST_ASSERT_IMPL_B0(ATestAssertNE, !=)
#define ATEST_ASSERT_NE(LHS, RHS) \
  ATEST_ASSERT_IMPL_B1(ATestAssertNE, LHS, RHS)

ATEST_ASSERT_IMPL_B0(ATestAssertLT, < )
#define ATEST_ASSERT_LT(LHS, RHS) \
  ATEST_ASSERT_IMPL_B1(ATestAssertLT, LHS, RHS)

ATEST_ASSERT_IMPL_B0(ATestAssertLE, <=)
#define ATEST_ASSERT_LE(LHS, RHS) \
  ATEST_ASSERT_IMPL_B1(ATestAssertLE, LHS, RHS)

ATEST_ASSERT_IMPL_B0(ATestAssertGT, > )
#define ATEST_ASSERT_GT(LHS, RHS) \
  ATEST_ASSERT_IMPL_B1(ATestAssertGT, LHS, RHS)

ATEST_ASSERT_IMPL_B0(ATestAssertGE, >=)
#define ATEST_ASSERT_GE(LHS, RHS) \
  ATEST_ASSERT_IMPL_B1(ATestAssertGE, LHS, RHS)

}  // namespace test
