/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "bytes.hpp"
#include "finally.hpp"
#include "persistence.hpp"

using namespace lvd::ambiramus;

// ----------

#if defined(NATIVE) or defined(FLASHT)

ATEST_CASE(Erase) {
  Persistence::SharedPtr persistence0 = Persistence::instance();
  FINALLY { Persistence::instance_release(); };

  bool ok;

  ok = persistence0->erase();
  ATEST_ASSERT_TR(ok);

  Bytes bytes0;
  bytes0.resize(Persistence::space());

  ok = persistence0->read(bytes0.data(), 0x00, bytes0.size());
  ATEST_ASSERT_TR(ok);

  for (size_t i = 0; i < bytes0.size(); i++) {
    ATEST_ASSERT_EQ(bytes0[i], 0xFF);
  }
}

ATEST_CASE(WriteRead) {
  Persistence::SharedPtr persistence0 = Persistence::instance();
  FINALLY { Persistence::instance_release(); };

  bool ok;

  ok = persistence0->erase();
  ATEST_ASSERT_TR(ok);

  Bytes bytes0;
  bytes0.resize(0x10);

  for (size_t i = 0; i < bytes0.size(); i++) {
    bytes0[i] = i;
  }

  ok = persistence0->write(bytes0.data(), 0x20, bytes0.size());
  ATEST_ASSERT_TR(ok);

  Bytes bytes1;
  bytes1.resize(0x30);

  ok = persistence0->read (bytes1.data(), 0x10, bytes1.size());
  ATEST_ASSERT_TR(ok);

  for (size_t i = 0x00; i < 0x10; i++) {
    ATEST_ASSERT_EQ(bytes1[i], 0xFF);
  }

  for (size_t i = 0x10; i < 0x20; i++) {
    ATEST_ASSERT_EQ(bytes1[i], i - 0x10);
  }

  for (size_t i = 0x20; i < 0x30; i++) {
    ATEST_ASSERT_EQ(bytes1[i], 0xFF);
  }
}

ATEST_CASE(WriteReadInvalid) {
  Persistence::SharedPtr persistence0 = Persistence::instance();
  FINALLY { Persistence::instance_release(); };

  bool ok;

//ok = persistence->erase();
//ATEST_ASSERT_TR(ok);

  Bytes bytes0;
  bytes0.resize(0x10);

  for (size_t i = 0; i < bytes0.size(); i++) {
    bytes0[i] = i;
  }

  Bytes bytes1;
  bytes1.resize(Persistence::space());

  ok = persistence0->write(bytes1.data(), 0x00, bytes1.size() - 2);
  ATEST_ASSERT_FA(ok);

  ok = persistence0->read (bytes1.data(), 0x00, bytes1.size() - 2);
  ATEST_ASSERT_FA(ok);

  ok = persistence0->write(bytes1.data(), 0xF0, bytes1.size());
  ATEST_ASSERT_FA(ok);

  ok = persistence0->read (bytes1.data(), 0xF0, bytes1.size());
  ATEST_ASSERT_FA(ok);
}

#endif

// ----------

ATEST_UNIT(Persistence);
