/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "config.hpp"
#include "output.hpp"
#include "output_manager.hpp"
#include "persistence.hpp"
#include "preferences.hpp"
#include "test_persistence.hpp"

using namespace lvd::ambiramus;

// ----------

ATEST_CASE(Outputs) {
  Persistence::SharedPtr persistence0 = TestPersistence::constructShared();
  Preferences::SharedPtr preferences0 =     Preferences::constructShared(persistence0);

  auto outputManager0 = OutputManager::constructShared(preferences0);
  ATEST_ASSERT_EQ(outputManager0->outputs().size(), ARRAY_SIZE(config::OUTPUTS));
}

ATEST_CASE(Search) {
  Persistence::SharedPtr persistence0 = TestPersistence::constructShared();
  Preferences::SharedPtr preferences0 =     Preferences::constructShared(persistence0);

  auto outputManager0 = OutputManager::constructShared(preferences0);

  auto outputs = outputManager0->outputs();
  ATEST_ASSERT_GT(outputs.size(), 0);

  auto output = outputManager0->search(outputs[0]->uuid());
  ATEST_ASSERT_EQ(outputs[0]->outputPreferences(), output->outputPreferences());
}

ATEST_CASE(Update) {
  Name name0 = Name::construct(STRREF("Mera Luna")).value();

  Persistence::SharedPtr persistence0 = TestPersistence::constructShared();
  Preferences::SharedPtr preferences0 =     Preferences::constructShared(persistence0);

  auto outputManager0 = OutputManager::constructShared(preferences0);

  auto outputs0 = outputManager0->outputs();
  ATEST_ASSERT_GT(outputs0.size(), 0);

  OutputPreferences outputPreferences = outputs0[0]->outputPreferences();
  outputPreferences.setName(name0);

  outputManager0->update(outputPreferences.uuid(), outputPreferences);

  auto outputs1 = outputManager0->outputs();
  ATEST_ASSERT_GT(outputs1.size(), 0);
  ATEST_ASSERT_EQ(outputs1[0]->name(), name0);

  auto outputManager2 = OutputManager::constructShared(preferences0);

  auto outputs2 = outputManager2->outputs();
  ATEST_ASSERT_GT(outputs2.size(), 0);
  ATEST_ASSERT_EQ(outputs2[0]->name(), name0);
}

// ----------

ATEST_UNIT(Output);
