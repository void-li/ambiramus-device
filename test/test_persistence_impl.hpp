/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include "bytes.hpp"
#include "persistence.hpp"
#include "persistence_impl.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class TestPersistenceImpl : public Persistence::Impl {
 public:
  TestPersistenceImpl(Persistence* persistence);

 public:
  bool read (      Byte* bytes, std::size_t index, std::size_t count) override;
  bool write(const Byte* bytes, std::size_t index, std::size_t count) override;
  bool erase()                                                        override;

 private:
  Bytes bytes_;
};

}  // namespace ambiramus
}  // namespace lvd
