/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "uuid.hpp"
#include "uuid_util.hpp"

using namespace lvd::ambiramus;

// ----------

ATEST_CASE(Constructor) {
  Uuid uuid0;
  ATEST_ASSERT_FA(uuid0.valid());
}

ATEST_CASE(Constructor2) {
  uint64_t part0 = 0x1122334455667788;
  uint64_t part1 = 0x9900aabbccddeeff;

  String string = String::construct("11223344-5566-7788-9900-aabbccddeeff");

  Uuid uuid = Uuid(part0, part1);
  ATEST_ASSERT_TR(uuid.valid());
  ATEST_ASSERT_EQ(uuid.part0(), part0);
  ATEST_ASSERT_EQ(uuid.part1(), part1);
  ATEST_ASSERT_EQ(toString(uuid), string);
}

ATEST_CASE(Construct) {
  uint64_t part0 = 0x1122334455667788;
  uint64_t part1 = 0x9900aabbccddeeff;

  String string = String::construct("11223344-5566-7788-9900-aabbccddeeff");

  Uuid uuid = Uuid::construct(string).value_or(Uuid());
  ATEST_ASSERT_TR(uuid.valid());
  ATEST_ASSERT_EQ(uuid.part0(), part0);
  ATEST_ASSERT_EQ(uuid.part1(), part1);
  ATEST_ASSERT_EQ(toString(uuid), string);
}

ATEST_CASE(ConstructInvalid) {
  String string0 = String::construct("11223344-5566-7788-9900-aabbccddee"    );
  Uuid uuid0 = Uuid::construct(string0).value_or(Uuid());
  ATEST_ASSERT_FA(uuid0.valid());

  String string1 = String::construct("11223344-5566-7788-9900-aabbccddeeff00");
  Uuid uuid1 = Uuid::construct(string1).value_or(Uuid());
  ATEST_ASSERT_FA(uuid1.valid());

  String string2 = String::construct("11?!3344-5566-7788-9900-aabbccddeeff");
  Uuid uuid2 = Uuid::construct(string2).value_or(Uuid());
  ATEST_ASSERT_FA(uuid1.valid());

  String string3 = String::construct("11223344-5566-7788-9900-aabbccdd?!ff");
  Uuid uuid3 = Uuid::construct(string3).value_or(Uuid());
  ATEST_ASSERT_FA(uuid1.valid());
}

ATEST_CASE(CreateRandom) {
  Uuid uuid0 = UuidUtil::constructRandom();
  ATEST_ASSERT_TR(uuid0.valid());
}

ATEST_CASE(OprEquality) {
  ATEST_ASSERT_TR(Uuid::construct(String::construct("11223344-5566-7788-9900-aabbccddeeff")) == Uuid::construct(String::construct("11223344-5566-7788-9900-aabbccddeeff")));
  ATEST_ASSERT_TR(Uuid::construct(String::construct("11223344-5566-7788-9900-aabbccddeeff")) != Uuid::construct(String::construct("00000000-0000-0000-0000-000000000000")));
}

// ----------

ATEST_UNIT(Uuid);
