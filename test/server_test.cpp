/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test.hpp"
#include "core.hpp"  // IWYU pragma: keep

#ifdef NATIVE
#include <QHostAddress>
#include <QUdpSocket>
#endif

#include "bytes.hpp"
#include "config.hpp"
#include "finally.hpp"
#include "server.hpp"

using namespace lvd::ambiramus;

// ----------

#ifdef NATIVE

ATEST_CASE(Server) {
  auto server0 = Server::constructShared();
  server0->sigLoopAlways.setSignal([](Module*) {});
  server0->sigLoopCyclic.setSignal([](Module*) {});
  server0->sigLoopIdling.setSignal([](Module*) {});

  server0->setup();
  FINALLY {
    server0->close();
  };

  uint32_t expectedAddr = 0x7F000001;
  Bytes    expectedData = Bytes::construct("Mera Luna");

  uint32_t receivedAddr;
  Bytes    receivedData;

  server0->sigMessage.setSignal([&](uint32_t _addr, const Bytes& _data) {
    receivedAddr = _addr;
    receivedData = _data;
  });

#ifdef NATIVE
  QUdpSocket qUdpSocket;
  qUdpSocket.writeDatagram(
      reinterpret_cast<char*>(expectedData.data()),
      expectedData.size(),
      QHostAddress::LocalHost,
      config::SERVER_PORT
  );

  qUdpSocket.flush();
#else
  ATEST_ASSERT_TR(false);
#endif

  server0->loop();

  ATEST_ASSERT_EQ(receivedAddr, expectedAddr);
  ATEST_ASSERT_EQ(receivedData, expectedData);
}

#endif

// ----------

ATEST_UNIT(Server);
