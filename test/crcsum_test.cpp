/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "bytes.hpp"
#include "crcsum.hpp"

using namespace lvd::ambiramus;

// ----------

ATEST_CASE(CrcSum) {
  const char* text0 = "Mera Luna";
  Bytes bytes0 = Bytes::construct(text0);

  CrcSum crcSum0 = crcsum(bytes0);
  ATEST_ASSERT_EQ(crcSum0, 0x6A723601);
}

ATEST_CASE(CrcSum2) {
  const char* text0 = "Luna Mera";
  Bytes bytes0 = Bytes::construct(text0);

  CrcSum crcSum0 = crcsum(bytes0);
  ATEST_ASSERT_EQ(crcSum0, 0xE3217C9B);
}

// ----------

ATEST_UNIT(CrcSum);
