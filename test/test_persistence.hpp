/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include "object.hpp"
#include "persistence.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class TestPersistence : public Persistence, public Object<TestPersistence> {
 public:
  using Construction   = Object<TestPersistence>::Construction;

  using ConstSharedPtr = Object<TestPersistence>::ConstSharedPtr;
  using      SharedPtr = Object<TestPersistence>::     SharedPtr;

  using ConstUniquePtr = Object<TestPersistence>::ConstUniquePtr;
  using      UniquePtr = Object<TestPersistence>::     UniquePtr;

  template <class... U>
  static SharedPtr constructShared(U&&... args) {
    return Object<TestPersistence>::constructShared(std::forward(args)...);
  }

  template <class... U>
  static UniquePtr constructUnique(U&&... args) {
    return Object<TestPersistence>::constructUnique(std::forward(args)...);
  }

 public:
  TestPersistence(Construction);
};

}  // namespace ambiramus
}  // namespace lvd
