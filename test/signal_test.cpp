/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <unity.h>

#include "signal.hpp"

using namespace lvd::ambiramus;

// ----------

ATEST_CASE(Signal) {
  bool called0 = false;
  Signal<void(       )> signal0;
  signal0.setSignal([&]                { called0 = true; return      ; });

  signal0();
  ATEST_ASSERT_TR(called0);
}

ATEST_CASE(Signal1) {
  bool called0 = false;
  Signal<int(void    )> signal0;
  signal0.setSignal([&] (            ) { called0 = true; return 0    ; });

  int result0 = signal0();
  ATEST_ASSERT_TR(called0);
  ATEST_ASSERT_EQ(result0, 00          );
}

ATEST_CASE(Signal2) {
  bool called0 = false;
  Signal<int(int     )> signal0;
  signal0.setSignal([&] (int a       ) { called0 = true; return a    ; });

  int result0 = signal0(42    );
  ATEST_ASSERT_TR(called0);
  ATEST_ASSERT_EQ(result0, 00 + 42     );
}

ATEST_CASE(Signal3) {
  bool called0 = false;
  Signal<int(int, int)> signal0;
  signal0.setSignal([&] (int a, int b) { called0 = true; return a + b; });

  int result0 = signal0(42, 23);
  ATEST_ASSERT_TR(called0);
  ATEST_ASSERT_EQ(result0, 00 + 42 + 23);
}

// ----------

ATEST_UNIT(Signal);
