/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <unity.h>

#include "timer.hpp"

using namespace lvd::ambiramus;

// ----------

ATEST_CASE(Timer) {
  Timer timer;

  Timer::Instant instant = std::chrono::system_clock::now();
  timer.setInstant(instant);
  ATEST_ASSERT_TR(timer.instant().has_value());
  ATEST_ASSERT_EQ(timer.instant().value(), instant);

  Timer::Timeout timeout = std::chrono::seconds(42);
  timer.setTimeout(timeout);
  ATEST_ASSERT_TR(timer.timeout().has_value());
  ATEST_ASSERT_EQ(timer.timeout().value(), timeout);

  timer.acquire();
  ATEST_ASSERT_FA(timer.instant().has_value());

  timer.release();
  ATEST_ASSERT_TR(timer.instant().has_value());
  ATEST_ASSERT_GE(timer.instant().value(), instant + timeout);

  timer = Timer();

  timer.acquire();
  ATEST_ASSERT_FA(timer.instant().has_value());

  timer.release();
  ATEST_ASSERT_FA(timer.instant().has_value());
}

// ----------

ATEST_UNIT(Timer);
