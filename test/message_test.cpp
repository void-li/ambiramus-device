/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <cstring>

#include <unity.h>

#include "message.hpp"
#include "name.hpp"
#include "vers.hpp"

using namespace lvd::ambiramus;

// ----------

ATEST_CASE(Announce) {
  auto t0 = std::chrono::system_clock::now();

  auto name0 = Name::construct(STRREF("Name")).value();
  auto vers0 = Vers::construct(STRREF("Vers")).value();

  auto devicePreferences0Uuid = Uuid::construct(STRREF("6c351c0b-c76f-4ff6-89d5-75c4f2da1db9")).value();
  auto devicePreferences0Name = Name::construct(STRREF("Device 0")).value();

  auto outputPreferences0Uuid = Uuid::construct(STRREF("078bf990-e97d-4077-9cc3-82c7c034d92d")).value();
  auto outputPreferences0Name = Name::construct(STRREF("Output 0")).value();
  auto outputPreferences0Leds = 42;

  auto outputPreferences1Uuid = Uuid::construct(STRREF("2a84f4c8-d932-4cd0-bd9c-3a48b9c918ad")).value();
  auto outputPreferences1Name = Name::construct(STRREF("Output 1")).value();
  auto outputPreferences1Leds = 23;

  auto message0 = Message::Announce::constructUnique();

  message0->setName(name0);
  message0->setVers(vers0);

  DevicePreferences devicePreferences0;
  devicePreferences0.setUuid(devicePreferences0Uuid);
  devicePreferences0.setName(devicePreferences0Name);
  message0->setDevicePreferencesItem(devicePreferences0);

  OutputPreferences outputPreferences0;
  outputPreferences0.setUuid(outputPreferences0Uuid);
  outputPreferences0.setName(outputPreferences0Name);
  outputPreferences0.setLeds(outputPreferences0Leds);
  message0->addOutputPreferencesItem(outputPreferences0);

  OutputPreferences outputPreferences1;
  outputPreferences1.setUuid(outputPreferences1Uuid);
  outputPreferences1.setName(outputPreferences1Name);
  outputPreferences1.setLeds(outputPreferences1Leds);
  message0->addOutputPreferencesItem(outputPreferences1);

  Bytes bytes0 = message0->encode();
  auto message1 = Message::decode(bytes0);

  ATEST_ASSERT_TR(message1);
  ATEST_ASSERT_EQ(message1->species(), Message::Species::ANNOUNCE);

  auto message2 = std::unique_ptr<Message::Announce>(static_cast<Message::Announce*>(message1.release()));

  ATEST_ASSERT_EQ(message2->name(), name0);
  ATEST_ASSERT_EQ(message2->vers(), vers0);

  ATEST_ASSERT_EQ(message2->devicePreferencesItem(), devicePreferences0);

  ATEST_ASSERT_EQ(message2->outputPreferencesList().size(), 2);
  ATEST_ASSERT_EQ(message2->outputPreferencesList()[0], outputPreferences0);
  ATEST_ASSERT_EQ(message2->outputPreferencesList()[1], outputPreferences1);

  auto t1 = std::chrono::system_clock::now();

  ATEST_ASSERT_LT(t0                 , message0->instant());
  ATEST_ASSERT_EQ(message0->instant(), message2->instant());
  ATEST_ASSERT_LT(message2->instant(), t1                 );
}

ATEST_CASE(AnnounceResponse) {
  auto t0 = std::chrono::system_clock::now();

  auto masterPreferences0Uuid = Uuid::construct(STRREF("4421d93a-2ced-44e0-913d-28fa0271c4bb")).value();
  auto masterPreferences0Name = Name::construct(STRREF("Master 0")).value();

  auto message0 = Message::AnnounceResponse::constructUnique();

  MasterPreferences masterPreferences0;
  masterPreferences0.setUuid(masterPreferences0Uuid);
  masterPreferences0.setName(masterPreferences0Name);
  message0->setMasterPreferencesItem(masterPreferences0);

  Bytes bytes0 = message0->encode();
  auto message1 = Message::decode(bytes0);

  ATEST_ASSERT_TR(message1);
  ATEST_ASSERT_EQ(message1->species(), Message::Species::ANNOUNCE_RESPONSE);

  auto message2 = std::unique_ptr<Message::AnnounceResponse>(static_cast<Message::AnnounceResponse*>(message1.release()));

  ATEST_ASSERT_EQ(message2->masterPreferencesItem(), masterPreferences0);

  auto t1 = std::chrono::system_clock::now();

  ATEST_ASSERT_LT(t0                 , message0->instant());
  ATEST_ASSERT_EQ(message0->instant(), message2->instant());
  ATEST_ASSERT_LT(message2->instant(), t1                 );
}

ATEST_CASE(Colorize) {
  auto t0 = std::chrono::system_clock::now();

  auto deviceUuid0 = Uuid::construct(STRREF("2b98a495-0548-4661-8764-f90a5d3329b7")).value();
  auto outputUuid0 = Uuid::construct(STRREF("728e8fed-7f77-4af5-b803-8df9ee04ccd9")).value();

  auto colors0 = Colors();
  colors0.push_back(Color(0xAA, 0xBB, 0xCC));
  colors0.push_back(Color(0xDD, 0xEE, 0xFF));

  auto message0 = Message::Colorize::constructUnique();

//message0->setDeviceUuid(deviceUuid0);
  message0->setOutputUuid(outputUuid0);

  message0->setColors(colors0);

  Bytes bytes0 = message0->encode();
  auto message1 = Message::decode(bytes0);

  ATEST_ASSERT_TR(message1);
  ATEST_ASSERT_EQ(message1->species(), Message::Species::COLORIZE);

  auto message2 = std::unique_ptr<Message::Colorize>(static_cast<Message::Colorize*>(message1.release()));

//ATEST_ASSERT_EQ(message2->deviceUuid(), deviceUuid0);
  ATEST_ASSERT_EQ(message2->outputUuid(), outputUuid0);

  ATEST_ASSERT_EQ(message2->colors(), colors0);

  auto t1 = std::chrono::system_clock::now();

  ATEST_ASSERT_LT(t0                 , message0->instant());
  ATEST_ASSERT_EQ(message0->instant(), message2->instant());
  ATEST_ASSERT_LT(message2->instant(), t1                 );
}

ATEST_CASE(ColorizeResponse) {
  auto t0 = std::chrono::system_clock::now();

  auto deviceUuid0 = Uuid::construct(STRREF("fad39e1d-4058-4d02-aeb6-e30ef022f039")).value();
  auto outputUuid0 = Uuid::construct(STRREF("2f28e3c6-1cd0-4413-8d80-486ab0de9c75")).value();

  auto colors0 = Colors();
  colors0.push_back(Color(0xAA, 0xBB, 0xCC));
  colors0.push_back(Color(0xDD, 0xEE, 0xFF));

  auto message0 = Message::ColorizeResponse::constructUnique();

//message0->setDeviceUuid(deviceUuid0);
  message0->setOutputUuid(outputUuid0);

  message0->setColors(colors0);

  Bytes bytes = message0->encode();
  auto message1 = Message::decode(bytes);

  ATEST_ASSERT_TR(message1);
  ATEST_ASSERT_EQ(message1->species(), Message::Species::COLORIZE_RESPONSE);

  auto message2 = std::unique_ptr<Message::ColorizeResponse>(static_cast<Message::ColorizeResponse*>(message1.release()));

//ATEST_ASSERT_EQ(message2->deviceUuid(), deviceUuid0);
  ATEST_ASSERT_EQ(message2->outputUuid(), outputUuid0);

  ATEST_ASSERT_EQ(message2->colors(), colors0);

  auto t1 = std::chrono::system_clock::now();

  ATEST_ASSERT_LT(t0                 , message0->instant());
  ATEST_ASSERT_EQ(message0->instant(), message2->instant());
  ATEST_ASSERT_LT(message2->instant(), t1                 );
}

// ----------

ATEST_UNIT(Message);
