/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "vers.hpp"

using namespace lvd::ambiramus;

// ----------

ATEST_CASE(Construct) {
  Vers vers0;
  ATEST_ASSERT_FA(vers0.valid());
}

ATEST_CASE(Construct2) {
  String string0 = STRREF("Mera Luna");

  Vers vers0 = Vers::construct(string0).value_or(Vers());
  ATEST_ASSERT_TR(vers0.valid());
  ATEST_ASSERT_EQ(toString(vers0), string0);
}

ATEST_CASE(ConstructInvalid) {
  String string0 = STRREF("Mera Luna Mera Luna Mera Luna Mera Luna");

  Vers vers0 = Vers::construct(string0).value_or(Vers());
  ATEST_ASSERT_FA(vers0.valid());
}

ATEST_CASE(OprEquality) {
  ATEST_ASSERT_TR(Vers::construct(String::construct("Mera")) == Vers::construct(String::construct("Mera")));
  ATEST_ASSERT_TR(Vers::construct(String::construct("Mera")) != Vers::construct(String::construct("Luna")));
}

// ----------

ATEST_UNIT(Vers);
