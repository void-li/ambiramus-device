/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "number.hpp"

using namespace lvd::ambiramus;

// ----------

ATEST_CASE(Construct) {
  auto number0 = Number::construct(STRREF( "42"));
  ATEST_ASSERT_TR(number0);
  ATEST_ASSERT_EQ(number0.value_or(0).to<uint64_t>(),  42);

  auto number1 = Number::construct(STRREF("+42"));
  ATEST_ASSERT_TR(number1);
  ATEST_ASSERT_EQ(number1.value_or(0).to<uint64_t>(), +42);

  auto number2 = Number::construct(STRREF("-42"));
  ATEST_ASSERT_TR(number2);
  ATEST_ASSERT_EQ(number2.value_or(0).to<uint64_t>(), -42);
}

ATEST_CASE(ConstructInvalid) {
  auto number0 = Number::construct(STRREF("ABC"));
  ATEST_ASSERT_FA(number0);
}

// ----------

ATEST_UNIT(Number);
