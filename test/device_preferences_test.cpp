/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "device_preferences.hpp"
#include "device_preferences_manager.hpp"
#include "name.hpp"
#include "test_persistence.hpp"
#include "uuid.hpp"
#include "uuid_util.hpp"

using namespace lvd::ambiramus;

// ----------

ATEST_CASE(DevicePreferences) {
  Uuid uuid = UuidUtil::constructRandom();
  Name name = Name    ::construct(STRREF("Device0")).value();

  DevicePreferences devicePreferences0;

  devicePreferences0.setUuid(uuid);
  ATEST_ASSERT_EQ(devicePreferences0.uuid(), uuid);

  devicePreferences0.setName(name);
  ATEST_ASSERT_EQ(devicePreferences0.name(), name);

  Persistence::SharedPtr persistence0 = TestPersistence::constructShared();
  Preferences::SharedPtr preferences0 =     Preferences::constructShared(persistence0);

  DevicePreferencesManager devicePreferencesManager0(preferences0, DeviceId::TEST0);
  devicePreferencesManager0.save(devicePreferences0);

  DevicePreferencesManager devicePreferencesManager1(preferences0, DeviceId::TEST0);
  DevicePreferences devicePreferences1 = devicePreferencesManager1.load();

  ATEST_ASSERT_EQ(devicePreferences1.uuid(), uuid  );
  ATEST_ASSERT_EQ(devicePreferences1.name(), name  );

  DevicePreferencesManager devicePreferencesManager2(preferences0, DeviceId::TEST1);
  DevicePreferences devicePreferences2 = devicePreferencesManager2.load();

  ATEST_ASSERT_EQ(devicePreferences2.uuid(), Uuid());
  ATEST_ASSERT_EQ(devicePreferences2.name(), Name());
}

// ----------

ATEST_UNIT(DevicePreferences);
