/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test.hpp"
#include "core.hpp"  // IWYU pragma: keep

#ifdef DEVICE
#include <Arduino.h>
#endif

#include "logger.hpp"

// ----------

namespace test {

std::array<ATestCase, ATEST_CASE_COUNT>* ATEST_CASES() {
  static std::array<ATestCase, ATEST_CASE_COUNT> ATEST_CASES;
  return                                        &ATEST_CASES;
}

std::size_t* ATEST_CASES_INDEX() {
  static std::size_t ATEST_CASES_INDEX = 0;
  return            &ATEST_CASES_INDEX;
}

std::array<ATestUnit, ATEST_UNIT_COUNT>* ATEST_UNITS() {
  static std::array<ATestUnit, ATEST_UNIT_COUNT> ATEST_UNITS;
  return                                        &ATEST_UNITS;
}

std::size_t* ATEST_UNITS_INDEX() {
  static std::size_t ATEST_UNITS_INDEX = 0;
  return            &ATEST_UNITS_INDEX;
}

// ----------

const char* __ATEST_CASE_FILTER__ = nullptr;
const char* __ATEST_UNIT_FILTER__ = nullptr;

// ----------

auto takeATestHeapStats() -> ATestHeapStats {
  ATestHeapStats atestHeapStats;

#ifdef DEVICE
  ESP.getHeapStats(&atestHeapStats.free, &atestHeapStats.full, &atestHeapStats.frag);
#endif

  return atestHeapStats;
}

void showATestHeapStats(const ATestHeapStats& s0, const ATestHeapStats& s1) {
#ifdef DEVICE
  if (s0.free != s1.free || s0.full != s1.full || s0.frag != s1.frag) {
    LOG_A("HEAP") << "Free:" << s0.free << s1.free;
    LOG_A("HEAP") << "Full:" << s0.full << s1.full;
    LOG_A("HEAP") << "Frag:" << s0.frag << s1.frag;
  }
#endif
}

// ----------

void printSpace() {
#ifdef DEVICE
  Serial.println();
#endif
}

}  // namespace test
