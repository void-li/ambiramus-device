/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "portal_preferences.hpp"
#include "portal_preferences_manager.hpp"
#include "string.hpp"
#include "test_persistence.hpp"

using namespace lvd::ambiramus;

// ----------

ATEST_CASE(PortalPreferences) {
  String username = STRREF("Mera");
  String password = STRREF("Luna");

  PortalPreferences portalPreferences0;

  portalPreferences0.setUsername(username);
  ATEST_ASSERT_EQ(portalPreferences0.username(), username);

  portalPreferences0.setPassword(password);
  ATEST_ASSERT_EQ(portalPreferences0.password(), password);

  Persistence::SharedPtr persistence0 = TestPersistence::constructShared();
  Preferences::SharedPtr preferences0 =     Preferences::constructShared(persistence0);

  PortalPreferencesManager portalPreferencesManager0(preferences0);
  portalPreferencesManager0.save(portalPreferences0);

  PortalPreferencesManager portalPreferencesManager1(preferences0);
  PortalPreferences portalPreferences1 = portalPreferencesManager1.load();

  ATEST_ASSERT_EQ(portalPreferences1.username(), username);
  ATEST_ASSERT_EQ(portalPreferences1.password(), password);
}

// ----------

ATEST_UNIT(PortalPreferences);
