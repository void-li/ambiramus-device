/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "main.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <unity.h>

#include "logger.hpp"
#include "test.hpp"

// ----------

#ifdef NATIVE
#include <QCoreApplication>
#endif

// ----------

void setup() {
  lvd::ambiramus::Logger::setLogLevel(
    lvd::ambiramus::Logger::LogLevel::D
  );
}

void loop() {
  UNITY_BEGIN();

  ATEST_MAIN;

  auto ret = UNITY_END();
  (void)ret;

#ifdef NATIVE
  QCoreApplication::exit(ret);
#endif
}

void close() {
  lvd::ambiramus::Logger::setLogLevel(
    lvd::ambiramus::Logger::LogLevel::W
  );
}
