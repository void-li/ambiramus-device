/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test_persistence.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "test_persistence_impl.hpp"

// ----------

namespace lvd {
namespace ambiramus {

TestPersistence::TestPersistence(Construction)
    : Persistence(Persistence::Construction()) {
  impl_ = std::make_unique<TestPersistenceImpl>(this);
}

}  // namespace ambiramus
}  // namespace lvd
