/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test_persistence_impl.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <cstring>
#include <stdexcept>

// ----------

namespace lvd {
namespace ambiramus {

TestPersistenceImpl::TestPersistenceImpl(Persistence* persistence)
    : Impl(persistence) {
  erase();
}

bool TestPersistenceImpl::read (      Byte* bytes, std::size_t index, std::size_t count) {
  auto bound = index + count;
  if (bound > bytes_.size()) {
#ifdef NATIVE
    throw std::runtime_error("FATAL");
#else
    return false;
#endif
  }

  std::memcpy(bytes, bytes_.data() + index, count);
  return true;
}

bool TestPersistenceImpl::write(const Byte* bytes, std::size_t index, std::size_t count) {
  auto bound = index + count;
  if (bound > bytes_.size()) {
#ifdef NATIVE
    throw std::runtime_error("FATAL");
#else
    return false;
#endif
  }

  for (std::size_t i = 0; i < count; i++) {
    if (bytes[i] & ~bytes_[i + index]) {
#ifdef NATIVE
      throw std::runtime_error("FATAL");
#else
      return false;
#endif
    }
  }

  std::memcpy(bytes_.data() + index, bytes, count);
  return true;
}

bool TestPersistenceImpl::erase() {
  bytes_.resize(space());

  std::memset(bytes_.data(), 0xFF, bytes_.size());
  return true;
}

}  // namespace ambiramus
}  // namespace lvd
