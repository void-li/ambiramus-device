/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "colors.hpp"

using namespace lvd::ambiramus;

// ----------

ATEST_CASE(Constructor) {
  Colors colors0;
  ATEST_ASSERT_TR(colors0.empty());
  ATEST_ASSERT_EQ(colors0.size(), 0);

  Colors colors1(3);
  ATEST_ASSERT_FA(colors1.empty());
  ATEST_ASSERT_EQ(colors1.size(), 3);
}

ATEST_CASE(Size) {
  Colors colors0 = Colors(3, Color(0x00, 0x42, 0xAA));

  const Colors colors1 = colors0;
  ATEST_ASSERT_EQ(colors1.size(), 3);

  const Colors colors2;
  ATEST_ASSERT_EQ(colors2.size(), 0);

  ATEST_ASSERT_EQ(colors0, Colors(3, Color(0x00, 0x42, 0xAA)));
}

ATEST_CASE(Empty) {
  Colors colors0 = Colors(3, Color(0x00, 0x42, 0xAA));

  const Colors colors1 = colors0;
  ATEST_ASSERT_FA(colors1.empty());

  const Colors colors2;
  ATEST_ASSERT_TR(colors2.empty());

  ATEST_ASSERT_EQ(colors0, Colors(3, Color(0x00, 0x42, 0xAA)));
}

ATEST_CASE(Data) {
  Colors colors0 = Colors(3, Color(0x00, 0x42, 0xAA));

  Color colordata1[] = { Color(0x00, 0x42, 0xAA), Color(0x00, 0x42, 0xAA), Color(0x00, 0x42, 0xAA) };
  const Colors colors1 = colors0;
  ATEST_ASSERT_EQ(memcmp(colors1.data(), colordata1, sizeof(colordata1)), 0);

  Color colordata2[] = { Color(0xAA, 0x42, 0x00), Color(0x00, 0x42, 0xAA), Color(0x00, 0x42, 0xAA) };
  Colors colors2 = colors0;
  colors2.data()[0] = Color(0xAA, 0x42, 0x00);
  ATEST_ASSERT_EQ(memcmp(colors2.data(), colordata2, sizeof(colordata2)), 0);

  ATEST_ASSERT_EQ(colors0, Colors(3, Color(0x00, 0x42, 0xAA)));
}

ATEST_CASE(Front) {
  Colors colors0 = Colors(3, Color(0x00, 0x42, 0xAA));

  const Colors colors1 = colors0;
  ATEST_ASSERT_EQ(colors1.front(), Color(0x00, 0x42, 0xAA));

  Colors colors2 = colors0;
  colors2.front() = Color(0xAA, 0x42, 0x00);
  ATEST_ASSERT_EQ(colors2.front(), Color(0xAA, 0x42, 0x00));

  ATEST_ASSERT_EQ(colors0, Colors(3, Color(0x00, 0x42, 0xAA)));
}

ATEST_CASE(Back) {
  Colors colors0 = Colors(3, Color(0x00, 0x42, 0xAA));

  const Colors colors1 = colors0;
  ATEST_ASSERT_EQ(colors1.back(), Color(0x00, 0x42, 0xAA));

  Colors colors2 = colors0;
  colors2.back() = Color(0xAA, 0x42, 0x00);
  ATEST_ASSERT_EQ(colors2.back(), Color(0xAA, 0x42, 0x00));

  ATEST_ASSERT_EQ(colors0, Colors(3, Color(0x00, 0x42, 0xAA)));
}

ATEST_CASE(BeginAndEnd) {
  Colors colors0 = Colors(3, Color(0x00, 0x42, 0xAA));

  const Colors colors1 = colors0;
  int index1 = 0;
  for (auto it = colors1.begin(); it != colors1.end(); it++) {
    ATEST_ASSERT_EQ(*it, colors1[index1++]);
  }

  Colors colors2 = colors0;
  for (auto it = colors2.begin(); it != colors2.end(); it++) {
    *it = Color(0xAA, 0x42, 0x00);
  }

  ATEST_ASSERT_EQ(colors2, Colors(3, Color(0xAA, 0x42, 0x00)));

  ATEST_ASSERT_EQ(colors0, Colors(3, Color(0x00, 0x42, 0xAA)));
}

ATEST_CASE(Clear) {
  Colors colors0 = Colors(3, Color(0x00, 0x42, 0xAA));

  Colors colors1 = colors0;
  colors1.clear();
  ATEST_ASSERT_TR(colors1.empty());
  ATEST_ASSERT_EQ(colors1.size(), 0);

  ATEST_ASSERT_EQ(colors0, Colors(3, Color(0x00, 0x42, 0xAA)));
}

ATEST_CASE(Resize) {
  Colors colors0 = Colors(3, Color(0x00, 0x42, 0xAA));

  Color colordata1[] = { Color(0x00, 0x42, 0xAA), Color(0x00, 0x42, 0xAA) };
  Colors colors1 = colors0;
  colors1.resize(2);
  ATEST_ASSERT_EQ(colors1.size(), 2);
  ATEST_ASSERT_EQ(memcmp(colors1.data(), colordata1, sizeof(colordata1)), 0);

  Color colordata2[] = { Color(0x00, 0x42, 0xAA), Color(0x00, 0x42, 0xAA), Color(0x00, 0x00, 0x00) };
  Colors colors2 = colors1;
  colors2.resize(3);
  ATEST_ASSERT_EQ(colors2.size(), 3);
  ATEST_ASSERT_EQ(memcmp(colors2.data(), colordata2, sizeof(colordata2)), 0);

  Color colordata3[] = { Color(0x00, 0x42, 0xAA), Color(0x00, 0x42, 0xAA), Color(0xFF, 0xFF, 0xFF) };
  Colors colors3 = colors1;
  colors3.resize(3, Color(0xFF, 0xFF, 0xFF));
  ATEST_ASSERT_EQ(colors3.size(), 3);
  ATEST_ASSERT_EQ(memcmp(colors3.data(), colordata3, sizeof(colordata3)), 0);

  ATEST_ASSERT_EQ(colors0, Colors(3, Color(0x00, 0x42, 0xAA)));
}

ATEST_CASE(Reserve) {
  Colors colors0 = Colors(3, Color(0x00, 0x42, 0xAA));

  Colors colors1 = colors0;
  colors1.reserve(42);
  ATEST_ASSERT_EQ(colors1.size(), 3);

  ATEST_ASSERT_EQ(colors0, Colors(3, Color(0x00, 0x42, 0xAA)));
}

ATEST_CASE(PushBack) {
  Colors colors0 = Colors(3, Color(0x00, 0x42, 0xAA));

  Color colordata1[] = { Color(0x00, 0x42, 0xAA), Color(0x00, 0x42, 0xAA), Color(0x00, 0x42, 0xAA), Color(0xFF, 0xFF, 0xFF) };
  Colors colors1 = colors0;
  colors1.push_back(Color(0xFF, 0xFF, 0xFF));
  ATEST_ASSERT_EQ(colors1.size(), 4);
  ATEST_ASSERT_EQ(memcmp(colors1.data(), colordata1, sizeof(colordata1)), 0);

  ATEST_ASSERT_EQ(colors0, Colors(3, Color(0x00, 0x42, 0xAA)));
}

ATEST_CASE(OprAccess) {
  Colors colors0 = Colors(3, Color(0x00, 0x42, 0xAA));

  const Colors colors1 = colors0;
  auto colval1 = colors1[0];
  ATEST_ASSERT_EQ(colval1, Color(0x00, 0x42, 0xAA));

  Color colordata2[] = { Color(0x00, 0x42, 0xAA), Color(0x00, 0x42, 0xAA), Color(0xAA, 0x42, 0x00) };
  Colors colors2 = colors0;
  colors2[2] = Color(0xAA, 0x42, 0x00);
  ATEST_ASSERT_EQ(colors2.size(), 3);
  ATEST_ASSERT_EQ(memcmp(colors2.data(), colordata2, sizeof(colordata2)), 0);

  ATEST_ASSERT_EQ(colors0, Colors(3, Color(0x00, 0x42, 0xAA)));
}

ATEST_CASE(OprEquality) {
  ATEST_ASSERT_EQ(Colors(3, Color(0x00, 0x42, 0xAA)), Colors(3, Color(0x00, 0x42, 0xAA)));
  ATEST_ASSERT_NE(Colors(3, Color(0x00, 0x42, 0xAA)), Colors(3, Color(0xAA, 0x42, 0x00)));
}

// ----------

ATEST_UNIT(Colors);
