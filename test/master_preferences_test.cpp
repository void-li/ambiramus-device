/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "master_preferences.hpp"
#include "name.hpp"
#include "uuid.hpp"
#include "uuid_util.hpp"

using namespace lvd::ambiramus;

// ----------

ATEST_CASE(MasterPreferences) {
  Uuid uuid = UuidUtil::constructRandom();
  Name name = Name    ::construct(STRREF("Master0")).value();

  MasterPreferences masterPreferences0;

  masterPreferences0.setUuid(uuid);
  ATEST_ASSERT_EQ(masterPreferences0.uuid(), uuid);

  masterPreferences0.setName(name);
  ATEST_ASSERT_EQ(masterPreferences0.name(), name);
}

// ----------

ATEST_UNIT(MasterPreferences);
