/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <cstring>

#include "bytes.hpp"

using namespace lvd::ambiramus;

// ----------

ATEST_CASE(Constructor) {
  Bytes bytes0;
  ATEST_ASSERT_TR(bytes0.empty());
  ATEST_ASSERT_EQ(bytes0.size(), 0);

  Bytes bytes1(3);
  ATEST_ASSERT_FA(bytes1.empty());
  ATEST_ASSERT_EQ(bytes1.size(), 3);

  Bytes bytes2(9, (Byte*) "Mera Luna");
  ATEST_ASSERT_FA(bytes2.empty());
  ATEST_ASSERT_EQ(bytes2.size(), 9);

  ATEST_ASSERT_EQ(bytes2, Bytes::construct("Mera Luna"));
}

ATEST_CASE(Construct) {
  Bytes bytes0 = Bytes::construct("Mera Luna");
  ATEST_ASSERT_FA(bytes0.empty());
  ATEST_ASSERT_EQ(bytes0.size(), strlen("Mera Luna"));
}

ATEST_CASE(Size) {
  Bytes bytes0 = Bytes::construct("Mera Luna");

  const Bytes bytes1 = bytes0;
  ATEST_ASSERT_EQ(bytes1.size(), strlen("Mera Luna"));

  const Bytes bytes2;
  ATEST_ASSERT_EQ(bytes2.size(), 0);

  ATEST_ASSERT_EQ(bytes0, Bytes::construct("Mera Luna"));
}

ATEST_CASE(Empty) {
  Bytes bytes0 = Bytes::construct("Mera Luna");

  const Bytes bytes1 = bytes0;
  ATEST_ASSERT_FA(bytes1.empty());

  const Bytes bytes2;
  ATEST_ASSERT_TR(bytes2.empty());

  ATEST_ASSERT_EQ(bytes0, Bytes::construct("Mera Luna"));
}

ATEST_CASE(Data) {
  Bytes bytes0 = Bytes::construct("Mera Luna");

  const Bytes bytes1 = bytes0;
  ATEST_ASSERT_EQ(memcmp(bytes1.data(), "Mera Luna", strlen("Mera Luna")), 0);

  Bytes bytes2 = bytes0;
  bytes2.data()[0] = 'm';
  ATEST_ASSERT_EQ(memcmp(bytes2.data(), "mera Luna", strlen("mera Luna")), 0);

  ATEST_ASSERT_EQ(bytes0, Bytes::construct("Mera Luna"));
}

ATEST_CASE(Front) {
  Bytes bytes0 = Bytes::construct("Mera Luna");

  const Bytes bytes1 = bytes0;
  ATEST_ASSERT_EQ(bytes1.front(), 'M');

  Bytes bytes2 = bytes0;
  bytes2.front() = 'm';
  ATEST_ASSERT_EQ(bytes2.front(), 'm');

  ATEST_ASSERT_EQ(bytes0, Bytes::construct("Mera Luna"));
}

ATEST_CASE(Back) {
  Bytes bytes0 = Bytes::construct("Mera Luna");

  const Bytes bytes1 = bytes0;
  ATEST_ASSERT_EQ(bytes1.back(), 'a');

  Bytes bytes2 = bytes0;
  bytes2.back() = 'A';
  ATEST_ASSERT_EQ(bytes2.back(), 'A');

  ATEST_ASSERT_EQ(bytes0, Bytes::construct("Mera Luna"));
}

ATEST_CASE(BeginAndEnd) {
  Bytes bytes0 = Bytes::construct("Mera Luna");

  const Bytes bytes1 = bytes0;
  int index1 = 0;
  for (auto it = bytes1.begin(); it != bytes1.end(); it++) {
    ATEST_ASSERT_EQ(*it, bytes1[index1++]);
  }

  Bytes bytes2 = bytes0;
  for (auto it = bytes2.begin(); it != bytes2.end(); it++) {
    *it = 'X';
  }

  ATEST_ASSERT_EQ(bytes2, Bytes::construct("XXXXXXXXX"));

  ATEST_ASSERT_EQ(bytes0, Bytes::construct("Mera Luna"));
}

ATEST_CASE(Clear) {
  Bytes bytes0 = Bytes::construct("Mera Luna");

  Bytes bytes1 = bytes0;
  bytes1.clear();
  ATEST_ASSERT_TR(bytes1.empty());
  ATEST_ASSERT_EQ(bytes1.size(), 0);

  ATEST_ASSERT_EQ(bytes0, Bytes::construct("Mera Luna"));
}

ATEST_CASE(Resize) {
  Bytes bytes0 = Bytes::construct("Mera Luna");

  Bytes bytes1 = bytes0;
  bytes1.resize(4);
  ATEST_ASSERT_EQ(bytes1.size(), strlen("Mera"));
  ATEST_ASSERT_EQ(memcmp(bytes1.data(), "Mera" , strlen("Mera" )), 0);

  Bytes bytes2 = bytes1;
  bytes2.resize(5);
  bytes2[4] = ' ';
  ATEST_ASSERT_EQ(bytes2.size(), 5);
  ATEST_ASSERT_EQ(memcmp(bytes2.data(), "Mera ", strlen("Mera ")), 0);

  Bytes bytes3 = bytes1;
  bytes3.resize(5, ' ');
  ATEST_ASSERT_EQ(bytes3.size(), 5);
  ATEST_ASSERT_EQ(memcmp(bytes3.data(), "Mera ", strlen("Mera ")), 0);

  ATEST_ASSERT_EQ(bytes0, Bytes::construct("Mera Luna"));
}

ATEST_CASE(Reserve) {
  Bytes bytes0 = Bytes::construct("Mera Luna");

  Bytes bytes1 = bytes0;
  bytes1.reserve(42);
  ATEST_ASSERT_EQ(bytes1.size(), strlen("Mera Luna"));

  ATEST_ASSERT_EQ(bytes0, Bytes::construct("Mera Luna"));
}

ATEST_CASE(PushBack) {
  Bytes bytes0 = Bytes::construct("Mera Luna");

  Bytes bytes1 = bytes0;
  bytes1.push_back('X');
  ATEST_ASSERT_EQ(bytes1, Bytes::construct("Mera LunaX"));

  ATEST_ASSERT_EQ(bytes0, Bytes::construct("Mera Luna"));
}

ATEST_CASE(OprAccess) {
  Bytes bytes0 = Bytes::construct("Mera Luna");

  const Bytes bytes1 = bytes0;
  auto byval1 = bytes1[0];
  ATEST_ASSERT_EQ(byval1, 'M');

  Bytes bytes2 = bytes0;
  bytes2[8] = 'A';
  ATEST_ASSERT_EQ(bytes2, Bytes::construct("Mera LunA"));

  ATEST_ASSERT_EQ(bytes0, Bytes::construct("Mera Luna"));
}

ATEST_CASE(OprEquality) {
  ATEST_ASSERT_TR(Bytes::construct("Mera") == Bytes::construct("Mera"));
  ATEST_ASSERT_TR(Bytes::construct("Mera") != Bytes::construct("Luna"));
}

// ----------

ATEST_CASE(Encoder) {
  Bytes bytes0 = Bytes::construct("Mera Luna");

  Bytes::Encoder encoder0;
  encoder0.encode(bytes0);

  Bytes bytes1 = encoder0.bytes();
  ATEST_ASSERT_EQ(memcmp(bytes1.data(), "\x00\x09Mera Luna", 11), 0);

  ATEST_ASSERT_EQ(bytes0, Bytes::construct("Mera Luna"));
}

ATEST_CASE(Decoder) {
  Bytes bytes0 = Bytes::construct("Mera Luna");

  Bytes::Encoder encoder0;
  encoder0.encode(bytes0);

  Bytes bytes1 = encoder0.bytes();
  ATEST_ASSERT_EQ(memcmp(bytes1.data(), "\x00\x09Mera Luna", 11), 0);

  Bytes::Decoder decoder(bytes1);
  auto bytes2 = decoder.decode<Bytes>();

  ATEST_ASSERT_TR(bytes2);
  ATEST_ASSERT_EQ(bytes2.value_or(Bytes()), bytes0);

  ATEST_ASSERT_EQ(bytes0, Bytes::construct("Mera Luna"));
}

// ----------

ATEST_UNIT(Bytes);
