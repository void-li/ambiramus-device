/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test.hpp"
#include "core.hpp"  // IWYU pragma: keep

#ifdef NATIVE
#include <QByteArray>
#include <QFile>
#endif

#include "update.hpp"
#include "finally.hpp"

using namespace lvd::ambiramus;

// ----------

#ifdef NATIVE

ATEST_CASE(Update) {
  auto update0 = Update::constructShared();
  update0->sigLoopAlways.setSignal([](Module*) {});
  update0->sigLoopCyclic.setSignal([](Module*) {});
  update0->sigLoopIdling.setSignal([](Module*) {});

  bool success = false;
  bool failure = false;

  update0->sigSuccess.setSignal([&success] { success = true; });
  update0->sigFailure.setSignal([&failure] { failure = true; });

  update0->setup();
  FINALLY {
    update0->close();
  };

  update0->updateSetup();

  update0->loop();

  Bytes bytes0 = Bytes::construct("Mera");
  update0->updateData(bytes0);

  update0->loop();

  Bytes bytes1 = Bytes::construct(" ");
  update0->updateData(bytes1);

  update0->loop();

  Bytes bytes2 = Bytes::construct("Luna");
  update0->updateData(bytes2);

  update0->loop();

  update0->updateClose();

  ATEST_ASSERT_TR(success);
  ATEST_ASSERT_FA(failure);

#ifdef NATIVE
  QFile qfile("update.dat");
  qfile.open(QFile::ReadOnly);

  ATEST_ASSERT_TR(qfile.isOpen());

  QByteArray qByteArray = qfile.readAll();
  ATEST_ASSERT_TR(qByteArray == "Mera Luna");
#else
  ATEST_ASSERT_TR(false);
#endif
}

ATEST_CASE(UpdateInvalid) {
  auto update0 = Update::constructShared();
  update0->sigLoopAlways.setSignal([](Module*) {});
  update0->sigLoopCyclic.setSignal([](Module*) {});
  update0->sigLoopIdling.setSignal([](Module*) {});

  bool success = false;
  bool failure = false;

  update0->sigSuccess.setSignal([&success] { success = true; });
  update0->sigFailure.setSignal([&failure] { failure = true; });

  update0->setup();
  FINALLY {
    update0->close();
  };

  update0->updateSetup();

  Bytes bytes0 = Bytes::construct("Mera");
  update0->updateData(bytes0);

  update0->updateAbort();

  ATEST_ASSERT_FA(success);
  ATEST_ASSERT_FA(failure);
}

#endif

// ----------

ATEST_UNIT(Update);
