/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "config.hpp"
#include "device.hpp"
#include "device_manager.hpp"
#include "persistence.hpp"
#include "preferences.hpp"
#include "test_persistence.hpp"

using namespace lvd::ambiramus;

// ----------

ATEST_CASE(Devices) {
  Persistence::SharedPtr persistence0 = TestPersistence::constructShared();
  Preferences::SharedPtr preferences0 =     Preferences::constructShared(persistence0);

  auto deviceManager0 = DeviceManager::constructShared(preferences0);
  ATEST_ASSERT_EQ(deviceManager0->devices().size(), ARRAY_SIZE(config::DEVICES));
}

ATEST_CASE(Search) {
  Persistence::SharedPtr persistence0 = TestPersistence::constructShared();
  Preferences::SharedPtr preferences0 =     Preferences::constructShared(persistence0);

  auto deviceManager0 = DeviceManager::constructShared(preferences0);

  auto devices = deviceManager0->devices();
  ATEST_ASSERT_GT(devices.size(), 0);

  auto device = deviceManager0->search(devices[0]->uuid());
  ATEST_ASSERT_EQ(devices[0]->devicePreferences(), device->devicePreferences());
}

ATEST_CASE(Update) {
  Name name0 = Name::construct(STRREF("Mera Luna")).value();

  Persistence::SharedPtr persistence0 = TestPersistence::constructShared();
  Preferences::SharedPtr preferences0 =     Preferences::constructShared(persistence0);

  auto deviceManager0 = DeviceManager::constructShared(preferences0);

  auto devices0 = deviceManager0->devices();
  ATEST_ASSERT_GT(devices0.size(), 0);

  DevicePreferences devicePreferences = devices0[0]->devicePreferences();
  devicePreferences.setName(name0);

  deviceManager0->update(devicePreferences.uuid(), devicePreferences);

  auto devices1 = deviceManager0->devices();
  ATEST_ASSERT_GT(devices1.size(), 0);
  ATEST_ASSERT_EQ(devices1[0]->name(), name0);

  auto deviceManager2 = DeviceManager::constructShared(preferences0);

  auto devices2 = deviceManager2->devices();
  ATEST_ASSERT_GT(devices2.size(), 0);
  ATEST_ASSERT_EQ(devices2[0]->name(), name0);
}

// ----------

ATEST_UNIT(Device);
