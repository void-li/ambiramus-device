/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "output_preferences.hpp"
#include "output_preferences_manager.hpp"
#include "name.hpp"
#include "test_persistence.hpp"
#include "uuid.hpp"
#include "uuid_util.hpp"

using namespace lvd::ambiramus;

// ----------

ATEST_CASE(OutputPreferences) {
  Uuid uuid = UuidUtil::constructRandom();
  Name name = Name    ::construct(STRREF("Output0")).value();

  OutputPreferences outputPreferences0;

  outputPreferences0.setUuid(uuid);
  ATEST_ASSERT_EQ(outputPreferences0.uuid(), uuid);

  outputPreferences0.setName(name);
  ATEST_ASSERT_EQ(outputPreferences0.name(), name);

  outputPreferences0.setLeds(0x42);
  ATEST_ASSERT_EQ(outputPreferences0.leds(), 0x42);

  Persistence::SharedPtr persistence0 = TestPersistence::constructShared();
  Preferences::SharedPtr preferences0 =     Preferences::constructShared(persistence0);

  OutputPreferencesManager outputPreferencesManager0(preferences0, OutputId::TEST0);
  outputPreferencesManager0.save(outputPreferences0);

  OutputPreferencesManager outputPreferencesManager1(preferences0, OutputId::TEST0);
  OutputPreferences outputPreferences1 = outputPreferencesManager1.load();

  ATEST_ASSERT_EQ(outputPreferences1.uuid(), uuid  );
  ATEST_ASSERT_EQ(outputPreferences1.name(), name  );
  ATEST_ASSERT_EQ(outputPreferences1.leds(), 0x42  );

  OutputPreferencesManager outputPreferencesManager2(preferences0, OutputId::TEST1);
  OutputPreferences outputPreferences2 = outputPreferencesManager2.load();

  ATEST_ASSERT_EQ(outputPreferences2.uuid(), Uuid());
  ATEST_ASSERT_EQ(outputPreferences2.name(), Name());
  ATEST_ASSERT_EQ(outputPreferences2.leds(), 0x00  );
}

// ----------

ATEST_UNIT(OutputPreferences);
