/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "finally.hpp"

using namespace lvd::ambiramus;

// ----------

ATEST_CASE(Finally) {
  int value = 42;

  {
    ATEST_ASSERT_EQ(value, 42);

    FINALLY {
      value = 23;
    };

    ATEST_ASSERT_EQ(value, 42);
  }

  ATEST_ASSERT_EQ(value, 23);
}

// ----------

ATEST_UNIT(Finally);
