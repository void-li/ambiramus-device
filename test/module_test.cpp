/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <chrono>

#include <unity.h>

#include "module.hpp"
#include "timer.hpp"

using namespace lvd::ambiramus;

// ----------

namespace {

class TestModule : public Module {

 public:
  TestModule()
      : Module(STRREF("TestModule")) {}

 public:
  virtual void setup() {}
  virtual void loop () {}
  virtual void close() {}

 public:
  void loopAlways() {
    Module::loopAlways(STRREF("TestModule"));
  }
  void loopCyclic(Timer::Timeout timeout) {
    Module::loopCyclic(STRREF("TestModule"), timeout);
  }
  void loopIdling() {
    Module::loopIdling(STRREF("TestModule"));
  }

 public:
  void activity  () {
    Module::activity  (STRREF("TestModule"));
  }
  void dormant   () {
    Module::dormant   (STRREF("TestModule"));
  }

 public:
  auto activityTime() -> TimePoint {
    return Module::activityTime();
  }
  bool activityFlag() {
    return Module::activityFlag();
  }
};

}  // namespace

template <>
String lvd::ambiramus::toString(const TestModule& testModule) {
  return toString(*static_cast<const Module*>(&testModule));
}

// ----------

ATEST_CASE(LoopAlways) {
  TestModule testModule0;
  Module*    module0 = nullptr;

  testModule0.sigLoopAlways.setSignal([&module0](Module* moduleArg) {
    module0 = moduleArg;
  });

  testModule0.loopAlways();
  ATEST_ASSERT_EQ(module0, &testModule0);
}

ATEST_CASE(LoopCyclic) {
  TestModule testModule0;
  Module*    module0 = nullptr;

  testModule0.sigLoopCyclic.setSignal([&module0](Module* moduleArg) {
    module0 = moduleArg;
  });

  testModule0.loopCyclic(std::chrono::seconds(42));
  ATEST_ASSERT_EQ(module0, &testModule0);

  auto timeout = testModule0.timer().timeout();
  ATEST_ASSERT_EQ(timeout, std::chrono::seconds(42));
}

ATEST_CASE(LoopIdling) {
  TestModule testModule0;
  Module*    module0 = nullptr;

  testModule0.sigLoopIdling.setSignal([&module0](Module* moduleArg) {
    module0 = moduleArg;
  });

  testModule0.loopIdling();
  ATEST_ASSERT_EQ(module0, &testModule0);
}

ATEST_CASE(Activity) {
  TestModule testModule0;

  auto t0 = std::chrono::system_clock::now();
  testModule0.activity();
  auto t1 = std::chrono::system_clock::now();

  ATEST_ASSERT_TR(testModule0.activityFlag());

  ATEST_ASSERT_GT(testModule0.activityTime(), t0);
  ATEST_ASSERT_LT(testModule0.activityTime(), t1);
}

ATEST_CASE(Dormant) {
  TestModule testModule0;

  testModule0.activity();
  ATEST_ASSERT_TR( testModule0.activityFlag());

  testModule0.dormant ();
  ATEST_ASSERT_TR(!testModule0.activityFlag());
}

// ----------

ATEST_UNIT(Module);
