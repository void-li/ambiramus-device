/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test.hpp"
#include "core.hpp"  // IWYU pragma: keep

#ifdef NATIVE
#include <QHostAddress>
#include <QNetworkDatagram>
#include <QUdpSocket>
#endif

#include "bytes.hpp"
#include "client.hpp"
#include "config.hpp"
#include "device_manager.hpp"
#include "finally.hpp"
#include "message.hpp"
#include "persistence.hpp"
#include "preferences.hpp"
#include "test_persistence.hpp"

using namespace lvd::ambiramus;

// ----------

#ifdef NATIVE

ATEST_CASE(Client) {
  auto client0 = Client::constructShared();
  client0->sigLoopAlways.setSignal([](Module*) {});
  client0->sigLoopCyclic.setSignal([](Module*) {});
  client0->sigLoopIdling.setSignal([](Module*) {});

  client0->setup();
  FINALLY {
    client0->close();
  };

  Persistence::SharedPtr persistence0 = TestPersistence::constructShared();
  Preferences::SharedPtr preferences0 =     Preferences::constructShared(persistence0);

  auto deviceManager = DeviceManager::constructShared(preferences0);
  Signal<DeviceManager*()> reqDeviceManager;
  reqDeviceManager.setSignal([deviceManager] {
    return deviceManager.get();
  });
  client0->reqDeviceManager = reqDeviceManager;

  auto outputManager = OutputManager::constructShared(preferences0);
  Signal<OutputManager*()> reqOutputManager;
  reqOutputManager.setSignal([outputManager] {
    return outputManager.get();
  });
  client0->reqOutputManager = reqOutputManager;

#ifdef NATIVE
  QUdpSocket qUdpSocket;
  ATEST_ASSERT_TR(
    qUdpSocket.bind(QHostAddress::Any, config::CLIENT_PORT, QAbstractSocket::ShareAddress)
  );

  FINALLY {
    qUdpSocket.close();
  };
#else
  ATEST_ASSERT_TR(false);
#endif

  client0->loop();

  Bytes bytes0;

#ifdef NATIVE
  ATEST_ASSERT_TR(qUdpSocket.waitForReadyRead());

  auto datagram = qUdpSocket.receiveDatagram();
  auto data     = datagram  .       data    ();

  bytes0.resize(data.size());
  std::memcpy(bytes0.data(), data.data(), bytes0.size());
#else
  ATEST_ASSERT_TR(false);
#endif

  Message::UniquePtr message = Message::decode(bytes0);

  ATEST_ASSERT_TR(message);
  ATEST_ASSERT_EQ(message->species(), Message::Species::ANNOUNCE);

  auto messageAnnounce = Message::Announce::UniquePtr(
        static_cast<Message::Announce*>(message.release())
  );

  ATEST_ASSERT_EQ(messageAnnounce->name(), Name::construct(String::construct(config::AMBIRA_NAME)));
  ATEST_ASSERT_EQ(messageAnnounce->vers(), Vers::construct(String::construct(config::AMBIRA_VERS)));
}

ATEST_CASE(ClientAirmail) {
  auto client0 = Client::constructShared();
  client0->sigLoopAlways.setSignal([](Module*) {});
  client0->sigLoopCyclic.setSignal([](Module*) {});
  client0->sigLoopIdling.setSignal([](Module*) {});

  client0->setup();
  FINALLY {
    client0->close();
  };

#ifdef NATIVE
  QUdpSocket qUdpSocket;
  ATEST_ASSERT_TR(
    qUdpSocket.bind(QHostAddress::Any, config::CLIENT_PORT, QAbstractSocket::ShareAddress)
  );

  FINALLY {
    qUdpSocket.close();
  };
#else
  ATEST_ASSERT_TR(false);
#endif

  Bytes bytes0 = Bytes::construct("Mera Luna");
  client0->airmailMessage(0x7F000001, bytes0);

  Bytes bytes1;

#ifdef NATIVE
  ATEST_ASSERT_TR(qUdpSocket.waitForReadyRead());

  auto datagram = qUdpSocket.receiveDatagram();
  auto data     = datagram  .       data    ();

  bytes1.resize(data.size());
  std::memcpy(bytes1.data(), data.data(), bytes1.size());
#else
  ATEST_ASSERT_TR(false);
#endif

  ATEST_ASSERT_EQ(bytes0, bytes1);
}

#endif

// ----------

ATEST_UNIT(Client);
