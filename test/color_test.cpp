/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "color.hpp"
#include "string.hpp"

using namespace lvd::ambiramus;

// ----------

ATEST_CASE(Constructor) {
  Color color0 = Color(                );
  ATEST_ASSERT_EQ(color0.r(), 0x00);
  ATEST_ASSERT_EQ(color0.g(), 0x00);
  ATEST_ASSERT_EQ(color0.b(), 0x00);
}

ATEST_CASE(Constructor2) {
  Color color0 = Color(0x00, 0x42, 0xAA);
  ATEST_ASSERT_EQ(color0.r(), 0x00);
  ATEST_ASSERT_EQ(color0.g(), 0x42);
  ATEST_ASSERT_EQ(color0.b(), 0xAA);
}

ATEST_CASE(Construct) {
  Color color = Color(0x00, 0x42, 0xAA);

  auto color0 = Color::construct(STRREF("#0042AA"));
  ATEST_ASSERT_TR(color0);
  ATEST_ASSERT_EQ(color0.value_or(Color()), color);
}

ATEST_CASE(Construct2) {
  Color color = Color(0x00, 0x42, 0xAA);

  auto color0 = Color::construct(STRREF("#0042aa"));
  ATEST_ASSERT_TR(color0);
  ATEST_ASSERT_EQ(color0.value_or(Color()), color);
}

ATEST_CASE(ConstructInvalid) {
  auto color0 = Color::construct(STRREF("#0042"    ));
  ATEST_ASSERT_FA(color0);

  auto color1 = Color::construct(STRREF("#0042AA00"));
  ATEST_ASSERT_FA(color1);

  auto color2 = Color::construct(STRREF("?0042AA"));
  ATEST_ASSERT_FA(color2);

  auto color3 = Color::construct(STRREF("#?!42AA"));
  ATEST_ASSERT_FA(color3);

  auto color4 = Color::construct(STRREF("#00?!AA"));
  ATEST_ASSERT_FA(color4);

  auto color5 = Color::construct(STRREF("#0042?!"));
  ATEST_ASSERT_FA(color5);
}

ATEST_CASE(ToHex) {
  Color color0 = Color(                );
  ATEST_ASSERT_EQ(color0.toHex(), STRREF("#000000"));
}

ATEST_CASE(ToHex2) {
  Color color0 = Color(0x00, 0x42, 0xAA);
  ATEST_ASSERT_EQ(color0.toHex(), STRREF("#0042AA"));
}

ATEST_CASE(OprEquality) {
  ATEST_ASSERT_TR(Color(0x00, 0x42, 0xAA) == Color(0x00, 0x42, 0xaa));
  ATEST_ASSERT_TR(Color(0x00, 0x42, 0xAA) != Color(0x00, 0x23, 0xAA));
}

// ----------

ATEST_UNIT(Color);
