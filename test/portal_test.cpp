/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test.hpp"
#include "core.hpp"  // IWYU pragma: keep

#ifdef NATIVE
#include <QEventLoop>
#include <QFile>
#include <QTcpSocket>
#include <QThread>
#include <QTimer>
#endif

#include "config.hpp"
#include "finally.hpp"
#include "output_manager.hpp"
#include "persistence.hpp"
#include "portal.hpp"
#include "portal_preferences.hpp"
#include "portal_preferences_manager.hpp"
#include "preferences.hpp"
#include "test_persistence.hpp"
#include "update.hpp"
#include "wifiap.hpp"
#include "wifiap_preferences.hpp"
#include "wifiap_preferences_manager.hpp"

using namespace lvd::ambiramus;

// ----------

namespace {

void portalRequest(const String& path, const String& data, const String& type = String()) {
#ifdef NATIVE
  QTcpSocket qTcpSocket;
  qTcpSocket.connectToHost("localhost", config::PORTAL_PORT);
  qTcpSocket.waitForConnected();

  qTcpSocket.write("POST ");
  qTcpSocket.write(path.data());
  qTcpSocket.write(" HTTP/1.1");
  qTcpSocket.write("\r\n" );

  qTcpSocket.write("Content-Length: ");
  qTcpSocket.write(QByteArray().setNum(static_cast<int>(data.size())));
  qTcpSocket.write("\r\n");

  if (!type.empty()) {
    qTcpSocket.write("Content-Type: ");
    qTcpSocket.write(type.data());
    qTcpSocket.write("\r\n");
  }

  qTcpSocket.write("\r\n");
  qTcpSocket.write(data.data());

  qTcpSocket.waitForBytesWritten();

  QEventLoop qEventLoop;

  QTimer::singleShot(std::chrono::milliseconds(10), [&qEventLoop] {
    qEventLoop.exit();
  });

  qEventLoop.exec();
#else
  ATEST_ASSERT_TR(false);
#endif
}

}  // namespace

// ----------

#ifdef NATIVE

ATEST_CASE(Colour) {
  Persistence::SharedPtr persistence0 = TestPersistence::constructShared();
  Preferences::SharedPtr preferences0 =     Preferences::constructShared(persistence0);

  auto outputManager0 = OutputManager::constructShared(preferences0);
  auto output0        = outputManager0->outputs().front();

  auto outputPreferences0 = output0->outputPreferences();
  outputPreferences0.setLeds(3);
  outputManager0->update(output0->uuid(), outputPreferences0);

  auto portalPreferences0 = PortalPreferences();
  auto portal0            = Portal::constructShared(preferences0, portalPreferences0);

  portal0->sigLoopAlways.setSignal([](Module*) {});
  portal0->sigLoopCyclic.setSignal([](Module*) {});
  portal0->sigLoopIdling.setSignal([](Module*) {});

  portal0->reqOutputManager.setSignal([outputManager0] {
    return outputManager0.get();
  });

  portal0->setup();
  FINALLY {
    portal0->close();
  };

  String string0;

  string0.append("output=");
  string0.append(toString(output0->uuid()));

  string0.append("&");

  string0.append("index=");
  string0.append("a");

  string0.append("&");

  string0.append("color=");
  string0.append("#0042AA");

  portalRequest(STRREF("/colour"), string0);

  Colors colors0 = output0->colors();
  ATEST_ASSERT_EQ(colors0.size(), 3);
  ATEST_ASSERT_EQ(colors0[0], Color(0x00, 0x42, 0xAA));
  ATEST_ASSERT_EQ(colors0[1], Color(0x00, 0x42, 0xAA));
  ATEST_ASSERT_EQ(colors0[2], Color(0x00, 0x42, 0xAA));
}

ATEST_CASE(Device) {
  Persistence::SharedPtr persistence0 = TestPersistence::constructShared();
  Preferences::SharedPtr preferences0 =     Preferences::constructShared(persistence0);

  auto deviceManager0 = DeviceManager::constructShared(preferences0);
  auto device0        = deviceManager0->devices().front();

  auto portalPreferences0 = PortalPreferences();
  auto portal0            = Portal::constructShared(preferences0, portalPreferences0);

  portal0->sigLoopAlways.setSignal([](Module*) {});
  portal0->sigLoopCyclic.setSignal([](Module*) {});
  portal0->sigLoopIdling.setSignal([](Module*) {});

  portal0->reqDeviceManager.setSignal([deviceManager0] {
    return deviceManager0.get();
  });

  portal0->setup();
  FINALLY {
    portal0->close();
  };

  String string0;

  string0.append("device=");
  string0.append(toString(device0->uuid()));

  string0.append("&");

  string0.append("name=");
  string0.append("Mera Luna");

  portalRequest(STRREF("/device"), string0);

  ATEST_ASSERT_EQ(device0->name(), Name::construct(STRREF("Mera Luna")));
}

ATEST_CASE(Output) {
  Persistence::SharedPtr persistence0 = TestPersistence::constructShared();
  Preferences::SharedPtr preferences0 =     Preferences::constructShared(persistence0);

  auto outputManager0 = OutputManager::constructShared(preferences0);
  auto output0        = outputManager0->outputs().front();

  auto portalPreferences0 = PortalPreferences();
  auto portal0            = Portal::constructShared(preferences0, portalPreferences0);

  portal0->sigLoopAlways.setSignal([](Module*) {});
  portal0->sigLoopCyclic.setSignal([](Module*) {});
  portal0->sigLoopIdling.setSignal([](Module*) {});

  portal0->reqOutputManager.setSignal([outputManager0] {
    return outputManager0.get();
  });

  portal0->setup();
  FINALLY {
    portal0->close();
  };

  String string0;

  string0.append("output=");
  string0.append(toString(output0->uuid()));

  string0.append("&");

  string0.append("name=");
  string0.append("Mera Luna");

  string0.append("&");

  string0.append("leds=");
  string0.append("42");

  portalRequest(STRREF("/output"), string0);

  ATEST_ASSERT_EQ(output0->name(), Name::construct(STRREF("Mera Luna")));
  ATEST_ASSERT_EQ(output0->leds(), 42);
}

ATEST_CASE(Portal) {
  Persistence::SharedPtr persistence0 = TestPersistence::constructShared();
  Preferences::SharedPtr preferences0 =     Preferences::constructShared(persistence0);

  auto portalPreferences0 = PortalPreferences();
  auto portal0            = Portal::constructShared(preferences0, portalPreferences0);

  portal0->sigLoopAlways.setSignal([](Module*) {});
  portal0->sigLoopCyclic.setSignal([](Module*) {});
  portal0->sigLoopIdling.setSignal([](Module*) {});

  portal0->setup();
  FINALLY {
    portal0->close();
  };

  String string0;

  string0.append("username=");
  string0.append("Mera");

  string0.append("&");

  string0.append("password=");
  string0.append("Luna");

  portalRequest(STRREF("/portal"), string0);

  PortalPreferencesManager portalPreferencesManager1(preferences0);
  PortalPreferences        portalPreferences1 = portalPreferencesManager1.load();

  ATEST_ASSERT_EQ(portalPreferences1.username(), STRREF("Mera"));
  ATEST_ASSERT_EQ(portalPreferences1.password(), STRREF("Luna"));
}

ATEST_CASE(System) {
  Persistence::SharedPtr persistence0 = TestPersistence::constructShared();
  Preferences::SharedPtr preferences0 =     Preferences::constructShared(persistence0);

  auto portalPreferences0 = PortalPreferences();
  auto portal0            = Portal::constructShared(preferences0, portalPreferences0);

  portal0->sigLoopAlways.setSignal([](Module*) {});
  portal0->sigLoopCyclic.setSignal([](Module*) {});
  portal0->sigLoopIdling.setSignal([](Module*) {});

  portal0->setup();
  FINALLY {
    portal0->close();
  };

  FINALLY {
    Logger::setLogLevel(Logger::LogLevel::D);
  };

  String string0;

  string0.append("logs=");
  string0.append("W");

  portalRequest(STRREF("/system"), string0);

  ATEST_ASSERT_EQ(Logger::logLevel(), Logger::LogLevel::W);

  String string1;

  string1.append("logs=");
  string1.append("I");

  portalRequest(STRREF("/system"), string1);

  ATEST_ASSERT_EQ(Logger::logLevel(), Logger::LogLevel::I);
}

ATEST_CASE(Update) {
  Persistence::SharedPtr persistence0 = TestPersistence::constructShared();
  Preferences::SharedPtr preferences0 =     Preferences::constructShared(persistence0);

  auto update0            = Update::constructShared();

  update0->sigLoopAlways.setSignal([](Module*) {});
  update0->sigLoopCyclic.setSignal([](Module*) {});
  update0->sigLoopIdling.setSignal([](Module*) {});

  update0->sigSuccess.setSignal([] {});
  update0->sigFailure.setSignal([] {});

  update0->setup();
  FINALLY {
    update0->close();
  };

  auto portalPreferences0 = PortalPreferences();
  auto portal0            = Portal::constructShared(preferences0, portalPreferences0);

  portal0->sigLoopAlways.setSignal([](Module*) {});
  portal0->sigLoopCyclic.setSignal([](Module*) {});
  portal0->sigLoopIdling.setSignal([](Module*) {});

  portal0->reqUpdate.setSignal([update0] {
    return update0.get();
  });

  portal0->setup();
  FINALLY {
    portal0->close();
  };

  FINALLY {
    Logger::setLogLevel(Logger::LogLevel::D);
  };

  String string0;

  string0.append("------WebKitFormBoundarycSHro14A0pHuAIPs");
  string0.append("\r\n");

  string0.append("Content-Disposition: form-data; name=\"file\"; filename=\"test\"");
  string0.append("\r\n");

  string0.append("Content-Type: application/octet-stream");
  string0.append("\r\n");

  string0.append("\r\n");

  string0.append("Mera Luna");
  string0.append("\r\n");

  string0.append("\r\n");

  string0.append("------WebKitFormBoundarycSHro14A0pHuAIPs--");
  string0.append("\r\n");

  portalRequest(STRREF("/update"), string0, STRREF("multipart/form-data; boundary=----WebKitFormBoundarycSHro14A0pHuAIPs"));

#ifdef NATIVE
  QFile qfile("update.dat");
  qfile.open(QFile::ReadOnly);

  ATEST_ASSERT_TR(qfile.isOpen());

  QByteArray qByteArray = qfile.readAll();
  ATEST_ASSERT_TR(qByteArray == "Mera Luna\r\n");
#else
  ATEST_ASSERT_TR(false);
#endif
}

ATEST_CASE(WifiAp) {
  Persistence::SharedPtr persistence0 = TestPersistence::constructShared();
  Preferences::SharedPtr preferences0 =     Preferences::constructShared(persistence0);

  auto wifiApPreferences0 = WifiApPreferences();
  auto wifiAp0            = WifiAp::constructShared(              wifiApPreferences0);

  wifiAp0->sigLoopAlways.setSignal([](Module*) {});
  wifiAp0->sigLoopCyclic.setSignal([](Module*) {});
  wifiAp0->sigLoopIdling.setSignal([](Module*) {});

  wifiAp0->setup();
  FINALLY {
    wifiAp0->close();
  };

  auto portalPreferences0 = PortalPreferences();
  auto portal0            = Portal::constructShared(preferences0, portalPreferences0);

  portal0->sigLoopAlways.setSignal([](Module*) {});
  portal0->sigLoopCyclic.setSignal([](Module*) {});
  portal0->sigLoopIdling.setSignal([](Module*) {});

  portal0->reqWifiAp.setSignal([wifiAp0] {
    return wifiAp0.get();
  });

  portal0->setup();
  FINALLY {
    portal0->close();
  };

  FINALLY {
    Logger::setLogLevel(Logger::LogLevel::D);
  };

  String string0;

  string0.append("ssid=");
  string0.append("Mera");

  string0.append("&");

  string0.append("password=");
  string0.append("Luna");

  portalRequest(STRREF("/wifiap"), string0);

  WifiApPreferencesManager wifiApPreferencesManager1(preferences0);
  WifiApPreferences        wifiApPreferences1 = wifiApPreferencesManager1.load();

  ATEST_ASSERT_EQ(wifiApPreferences1.clientSsid(), STRREF("Mera"));
  ATEST_ASSERT_EQ(wifiApPreferences1.clientPassword(), STRREF("Luna"));
}

ATEST_CASE(Rescan) {
  Persistence::SharedPtr persistence0 = TestPersistence::constructShared();
  Preferences::SharedPtr preferences0 =     Preferences::constructShared(persistence0);

  auto wifiApPreferences0 = WifiApPreferences();
  auto wifiAp0            = WifiAp::constructShared(              wifiApPreferences0);

  wifiAp0->sigLoopAlways.setSignal([](Module*) {});
  wifiAp0->sigLoopCyclic.setSignal([](Module*) {});
  wifiAp0->sigLoopIdling.setSignal([](Module*) {});

  wifiAp0->setup();
  FINALLY {
    wifiAp0->close();
  };

  auto portalPreferences0 = PortalPreferences();
  auto portal0            = Portal::constructShared(preferences0, portalPreferences0);

  portal0->sigLoopAlways.setSignal([](Module*) {});
  portal0->sigLoopCyclic.setSignal([](Module*) {});
  portal0->sigLoopIdling.setSignal([](Module*) {});

  portal0->reqWifiAp.setSignal([wifiAp0] {
    return wifiAp0.get();
  });

  portal0->setup();
  FINALLY {
    portal0->close();
  };

  FINALLY {
    Logger::setLogLevel(Logger::LogLevel::D);
  };

  String string0;

  portalRequest(STRREF("/wifiap/rescan"), string0);

  ATEST_ASSERT_TR(wifiAp0->scanning());
}

#endif

// ----------

ATEST_UNIT(Portal);
