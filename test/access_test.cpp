/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "access.hpp"
#include "string.hpp"

using namespace lvd::ambiramus;

// ----------

namespace {

class Clazz {};

}  // namespace

template <>
String lvd::ambiramus::toString(const Clazz&) {
  return STRREF("Clazz");
}

// ----------

ATEST_CASE(Constructor) {
  Clazz clazz0;
  Access<Clazz> access0(&clazz0 );

  ATEST_ASSERT_TR(access0);
  ATEST_ASSERT_EQ(access0.access(), &clazz0);
}

ATEST_CASE(Constructor2) {
//Clazz clazz0;
  Access<Clazz> access0(nullptr);

  ATEST_ASSERT_FA(access0);
  ATEST_ASSERT_EQ(access0.access(), nullptr);
}

ATEST_CASE(Invalidate) {
  Clazz clazz0;
  Access<Clazz> access0(&clazz0);

  ATEST_ASSERT_TR(access0);
  ATEST_ASSERT_EQ(access0.access(), &clazz0);

  access0.invalidate();

  ATEST_ASSERT_FA(access0);
  ATEST_ASSERT_EQ(access0.access(), nullptr);
}

// ----------

ATEST_UNIT(Access);
