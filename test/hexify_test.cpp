/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "hexify.hpp"

using namespace lvd::ambiramus;

// ----------

ATEST_CASE(ChrToHex) {
  ATEST_ASSERT_EQ(chrToHex(0x00, HexifyCase::UPPER_CASE), (((uint16_t)'0') << 8) | '0');
  ATEST_ASSERT_EQ(chrToHex(0x42, HexifyCase::UPPER_CASE), (((uint16_t)'4') << 8) | '2');
  ATEST_ASSERT_EQ(chrToHex(0xAA, HexifyCase::UPPER_CASE), (((uint16_t)'A') << 8) | 'A');
}

ATEST_CASE(ChrToHex2) {
  ATEST_ASSERT_EQ(chrToHex(0x00, HexifyCase::LOWER_CASE), (((uint16_t)'0') << 8) | '0');
  ATEST_ASSERT_EQ(chrToHex(0x42, HexifyCase::LOWER_CASE), (((uint16_t)'4') << 8) | '2');
  ATEST_ASSERT_EQ(chrToHex(0xaa, HexifyCase::LOWER_CASE), (((uint16_t)'a') << 8) | 'a');
}

ATEST_CASE(HexToChr) {
  ATEST_ASSERT_EQ(hexToChr('0', '0').value_or(0xFF), 0x00);
  ATEST_ASSERT_EQ(hexToChr('4', '2').value_or(0xFF), 0x42);
  ATEST_ASSERT_EQ(hexToChr('A', 'A').value_or(0xFF), 0xAA);
}

ATEST_CASE(HexToChr2) {
  ATEST_ASSERT_EQ(hexToChr('0', '0').value_or(0xff), 0x00);
  ATEST_ASSERT_EQ(hexToChr('4', '2').value_or(0xff), 0x42);
  ATEST_ASSERT_EQ(hexToChr('a', 'a').value_or(0xff), 0xaa);
}

ATEST_CASE(HexToChrInvalid) {
  ATEST_ASSERT_FA(hexToChr('0', '?'));
  ATEST_ASSERT_FA(hexToChr('?', '0'));
}

// ----------

ATEST_UNIT(Hexify);
