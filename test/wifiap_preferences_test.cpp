/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "config.hpp"
#include "string.hpp"
#include "test_persistence.hpp"
#include "wifiap_preferences.hpp"
#include "wifiap_preferences_manager.hpp"

using namespace lvd::ambiramus;

// ----------

ATEST_CASE(WifiApPreferences) {
  String clientSsid     = STRREF("Mera");
  String clientPassword = STRREF("Luna");

  String accessSsid     = STRREF("Star");
  String accessPassword = STRREF("Dust");

  WifiApPreferences wifiApPreferences0;

  wifiApPreferences0.setClientSsid    (clientSsid    );
  ATEST_ASSERT_EQ(wifiApPreferences0.clientSsid    (), clientSsid    );

  wifiApPreferences0.setClientPassword(clientPassword);
  ATEST_ASSERT_EQ(wifiApPreferences0.clientPassword(), clientPassword);

  wifiApPreferences0.setAccessSsid    (accessSsid    );
  ATEST_ASSERT_EQ(wifiApPreferences0.accessSsid    (), accessSsid    );

  wifiApPreferences0.setAccessPassword(accessPassword);
  ATEST_ASSERT_EQ(wifiApPreferences0.accessPassword(), accessPassword);

  Persistence::SharedPtr persistence0 = TestPersistence::constructShared();
  Preferences::SharedPtr preferences0 =     Preferences::constructShared(persistence0);

  WifiApPreferencesManager wifiApPreferencesManager0(preferences0);
  wifiApPreferencesManager0.save(wifiApPreferences0);

  WifiApPreferencesManager wifiApPreferencesManager1(preferences0);
  WifiApPreferences wifiApPreferences1 = wifiApPreferencesManager1.load();

  ATEST_ASSERT_EQ(wifiApPreferences1.clientSsid    (), clientSsid    );
  ATEST_ASSERT_EQ(wifiApPreferences1.clientPassword(), clientPassword);

  ATEST_ASSERT_EQ(wifiApPreferences1.accessSsid    (), accessSsid    );
  ATEST_ASSERT_EQ(wifiApPreferences1.accessPassword(), accessPassword);

  persistence0->erase ();
  preferences0->reload();

  WifiApPreferencesManager wifiApPreferencesManager2(preferences0);
  WifiApPreferences wifiApPreferences2 = wifiApPreferencesManager2.load();

  ATEST_ASSERT_EQ(wifiApPreferences2.clientSsid    (), String::construct(config::WIFIAP_CLIENT_SSID    ));
  ATEST_ASSERT_EQ(wifiApPreferences2.clientPassword(), String::construct(config::WIFIAP_CLIENT_PASSWORD));

  ATEST_ASSERT_EQ(wifiApPreferences2.accessSsid    (), String::construct(config::WIFIAP_ACCESS_SSID    ));
  ATEST_ASSERT_EQ(wifiApPreferences2.accessPassword(), String::construct(config::WIFIAP_ACCESS_PASSWORD));
}

// ----------

ATEST_UNIT(WifiApPreferences);
