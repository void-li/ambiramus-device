/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "test.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <cstring>

#include "bytes.hpp"
#include "persistence.hpp"
#include "preferences.hpp"
#include "test_persistence.hpp"

using namespace lvd::ambiramus;

// ----------

ATEST_CASE(SaveAndLoadSingleUnit) {
  Persistence::SharedPtr persistence0 = TestPersistence::constructShared();
  Preferences::SharedPtr preferences0 =     Preferences::constructShared(persistence0);

  Bytes bytes00;
  bytes00.resize(1);
  bytes00[0] = 0x42;

  preferences0->save(Preferences::Species::TEST0, 0x0000, bytes00);

  Bytes bytes10 = preferences0->load(Preferences::Species::TEST0, 0x0000);
  ATEST_ASSERT_EQ(bytes10.size(), 1);

  ATEST_ASSERT_EQ(bytes10[0], 0x42);

  Bytes bytes11 = preferences0->load(Preferences::Species::TEST0, 0x0001);
  ATEST_ASSERT_EQ(bytes11.size(), 0);

  Bytes bytes12 = preferences0->load(Preferences::Species::TEST1, 0x0000);
  ATEST_ASSERT_EQ(bytes12.size(), 0);
}

ATEST_CASE(SaveAndLoadSingleUnitSeveralTimes) {
  Persistence::SharedPtr persistence0 = TestPersistence::constructShared();
  Preferences::SharedPtr preferences0 =     Preferences::constructShared(persistence0);

  Bytes bytes00;
  bytes00.resize(1);
  bytes00[0] = 0x42;

  preferences0->save(Preferences::Species::TEST0, 0x0000, bytes00);

  Bytes bytes01;
  bytes01.resize(1);
  bytes01[0] = 0x23;

  preferences0->save(Preferences::Species::TEST0, 0x0000, bytes01);

  Bytes bytes10 = preferences0->load(Preferences::Species::TEST0, 0x0000);
  ATEST_ASSERT_EQ(bytes10.size(), 1);

  ATEST_ASSERT_EQ(bytes10[0], 0x23);
}

ATEST_CASE(SaveAndLoadMultipleUnits) {
  Persistence::SharedPtr persistence0 = TestPersistence::constructShared();
  Preferences::SharedPtr preferences0 =     Preferences::constructShared(persistence0);

  Bytes bytes00;
  bytes00.resize(1);
  bytes00[0] = 0x42;

  preferences0->save(Preferences::Species::TEST0, 0x0000, bytes00);

  Bytes bytes01;
  bytes01.resize(1);
  bytes01[0] = 0x23;

  preferences0->save(Preferences::Species::TEST0, 0x0001, bytes01);

  Bytes bytes02;
  bytes02.resize(1);
  bytes02[0] = 0x34;

  preferences0->save(Preferences::Species::TEST1, 0x0000, bytes02);

  Bytes bytes10 = preferences0->load(Preferences::Species::TEST0, 0x0000);
  ATEST_ASSERT_EQ(bytes10.size(), 1);

  ATEST_ASSERT_EQ(bytes10[0], 0x42);

  Bytes bytes11 = preferences0->load(Preferences::Species::TEST0, 0x0001);
  ATEST_ASSERT_EQ(bytes11.size(), 1);

  ATEST_ASSERT_EQ(bytes11[0], 0x23);

  Bytes bytes12 = preferences0->load(Preferences::Species::TEST1, 0x0000);
  ATEST_ASSERT_EQ(bytes12.size(), 1);

  ATEST_ASSERT_EQ(bytes12[0], 0x34);
}

ATEST_CASE(SaveAndLoadSingleUnitInPersistence) {
  Persistence::SharedPtr persistence0 = TestPersistence::constructShared();
  Preferences::SharedPtr preferences0 =     Preferences::constructShared(persistence0);

  Bytes bytes00;
  bytes00.resize(1);
  bytes00[0] = 0x42;

  preferences0->save(Preferences::Species::TEST0, 0x0000, bytes00);
  preferences0->commit();

  Preferences::SharedPtr preferences1 =     Preferences::constructShared(persistence0);

  Bytes bytes10 = preferences1->load(Preferences::Species::TEST0, 0x0000);
  ATEST_ASSERT_EQ(bytes10.size(), 1);

  ATEST_ASSERT_EQ(bytes10[0], 0x42);

  Bytes bytes11 = preferences1->load(Preferences::Species::TEST0, 0x0001);
  ATEST_ASSERT_EQ(bytes11.size(), 0);

  Bytes bytes12 = preferences1->load(Preferences::Species::TEST1, 0x0000);
  ATEST_ASSERT_EQ(bytes12.size(), 0);
}

ATEST_CASE(SaveAndLoadSingleUnitSeveralTimesInPersistence) {
  Persistence::SharedPtr persistence0 = TestPersistence::constructShared();
  Preferences::SharedPtr preferences0 =     Preferences::constructShared(persistence0);

  Bytes bytes00;
  bytes00.resize(1);
  bytes00[0] = 0x42;

  preferences0->save(Preferences::Species::TEST0, 0x0000, bytes00);
  preferences0->commit();

  Bytes bytes01;
  bytes01.resize(1);
  bytes01[0] = 0x23;

  preferences0->save(Preferences::Species::TEST0, 0x0000, bytes01);
  preferences0->commit();

  Preferences::SharedPtr preferences1 =     Preferences::constructShared(persistence0);

  Bytes bytes10 = preferences1->load(Preferences::Species::TEST0, 0x0000);
  ATEST_ASSERT_EQ(bytes10.size(), 1);

  ATEST_ASSERT_EQ(bytes10[0], 0x23);
}

ATEST_CASE(SaveAndLoadMultipleUnitsInPersistence) {
  Persistence::SharedPtr persistence0 = TestPersistence::constructShared();
  Preferences::SharedPtr preferences0 =     Preferences::constructShared(persistence0);

  Bytes bytes00;
  bytes00.resize(1);
  bytes00[0] = 0x42;

  preferences0->save(Preferences::Species::TEST0, 0x0000, bytes00);
  preferences0->commit();

  Bytes bytes01;
  bytes01.resize(1);
  bytes01[0] = 0x23;

  preferences0->save(Preferences::Species::TEST0, 0x0001, bytes01);
  preferences0->commit();

  Bytes bytes02;
  bytes02.resize(1);
  bytes02[0] = 0x34;

  preferences0->save(Preferences::Species::TEST1, 0x0000, bytes02);
  preferences0->commit();

  Preferences::SharedPtr preferences1 =     Preferences::constructShared(persistence0);

  Bytes bytes10 = preferences1->load(Preferences::Species::TEST0, 0x0000);
  ATEST_ASSERT_EQ(bytes10.size(), 1);

  ATEST_ASSERT_EQ(bytes10[0], 0x42);

  Bytes bytes11 = preferences1->load(Preferences::Species::TEST0, 0x0001);
  ATEST_ASSERT_EQ(bytes11.size(), 1);

  ATEST_ASSERT_EQ(bytes11[0], 0x23);

  Bytes bytes12 = preferences1->load(Preferences::Species::TEST1, 0x0000);
  ATEST_ASSERT_EQ(bytes12.size(), 1);

  ATEST_ASSERT_EQ(bytes12[0], 0x34);
}

ATEST_CASE(ReloadDiscardsUncomittetUnits) {
  Persistence::SharedPtr persistence0 = TestPersistence::constructShared();
  Preferences::SharedPtr preferences0 =     Preferences::constructShared(persistence0);

  Bytes bytes00;
  bytes00.resize(1);
  bytes00[0] = 0x42;

  preferences0->save(Preferences::Species::TEST0, 0x0000, bytes00);
  preferences0->commit();

  Bytes bytes01;
  bytes01.resize(1);
  bytes01[0] = 0x23;

  preferences0->save(Preferences::Species::TEST0, 0x0000, bytes01);
  preferences0->reload();

  Bytes bytes10 = preferences0->load(Preferences::Species::TEST0, 0x0000);
  ATEST_ASSERT_EQ(bytes10.size(), 1);

  ATEST_ASSERT_EQ(bytes10[0], 0x42);

  preferences0->save(Preferences::Species::TEST0, 0x0001, bytes01);
  preferences0->reload();

  Bytes bytes20 = preferences0->load(Preferences::Species::TEST0, 0x0001);
  ATEST_ASSERT_EQ(bytes20.size(), 0);

  preferences0->save(Preferences::Species::TEST1, 0x0000, bytes01);
  preferences0->reload();

  Bytes bytes30 = preferences0->load(Preferences::Species::TEST1, 0x0000);
  ATEST_ASSERT_EQ(bytes30.size(), 0);
}

ATEST_CASE(PersistenceOverflow) {
  Persistence::SharedPtr persistence0 = TestPersistence::constructShared();
  Preferences::SharedPtr preferences0 =     Preferences::constructShared(persistence0);

  for (int i = 0; i < 8; i++) {
    Bytes bytes00;
    bytes00.resize(persistence0->space() / 8);
    std::memset(bytes00.data(), i, bytes00.size());

    preferences0->save(Preferences::Species::TEST0, 0x0000, bytes00);
    preferences0->commit();
  }

  Preferences::SharedPtr preferences1 =     Preferences::constructShared(persistence0);

  Bytes bytes10 = preferences0->load(Preferences::Species::TEST0, 0x0000);
  ATEST_ASSERT_NE(bytes10.size(), 0);

  ATEST_ASSERT_EQ(bytes10[0], 0x07);
}

// ----------

ATEST_UNIT(Preferences);
