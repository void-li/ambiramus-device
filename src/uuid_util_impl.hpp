/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include "uuid.hpp"
#include "uuid_util.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class UuidUtil::Impl {
 public:
  static Uuid createRandom();
};

}  // namespace ambiramus
}  // namespace lvd
