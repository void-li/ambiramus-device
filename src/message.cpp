/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "message.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "config.hpp"
#include "crcsum.hpp"
#include "enum.hpp"
#include "logger.hpp"

// ----------

namespace {

//                         A u r o r a
const uint64_t MESSAGE = 0x4175726f72610000;

}  // namespace

// ----------

namespace lvd {
namespace ambiramus {

template <>
String toString<Message::Species>(const Message::Species& value) {
  switch (value) {
    case Message::Species::ANNOUNCE:
      return toString(STRREF("ANNOUNCE"         ));

    case Message::Species::ANNOUNCE_RESPONSE:
      return toString(STRREF("ANNOUNCE_RESPONSE"));

    case Message::Species::COLORIZE:
      return toString(STRREF("COLORIZE"         ));

    case Message::Species::COLORIZE_RESPONSE:
      return toString(STRREF("COLORIZE_RESPONSE"));
  }

  return toString(STRREF("~UNKNOWN"));
}

template <>
String toString<Message::Announce        ::Variant>(
    const Message::Announce        ::Variant& value
) {
  switch (value) {
    case Message::Announce        ::Variant::VVOID:
      return toString(STRREF("VVOID"));

    case Message::Announce        ::Variant::V0001:
      return toString(STRREF("V0001"));
  }

  return toString(STRREF("~UNKNOWN"));
}

template <>
String toString<Message::AnnounceResponse::Variant>(
    const Message::AnnounceResponse::Variant& value
) {
  switch (value) {
    case Message::AnnounceResponse::Variant::VVOID:
      return toString(STRREF("VVOID"));

    case Message::AnnounceResponse::Variant::V0001:
      return toString(STRREF("V0001"));
  }

  return toString(STRREF("~UNKNOWN"));
}

template <>
String toString<Message::Colorize        ::Variant>(
    const Message::Colorize        ::Variant& value
) {
  switch (value) {
    case Message::Colorize        ::Variant::VVOID:
      return toString(STRREF("VVOID"));

    case Message::Colorize        ::Variant::V0001:
      return toString(STRREF("V0001"));
  }

  return toString(STRREF("~UNKNOWN"));
}

template <>
String toString<Message::ColorizeResponse::Variant>(
    const Message::ColorizeResponse::Variant& value
) {
  switch (value) {
    case Message::ColorizeResponse::Variant::VVOID:
      return toString(STRREF("VVOID"));

    case Message::ColorizeResponse::Variant::V0001:
      return toString(STRREF("V0001"));
  }

  return toString(STRREF("~UNKNOWN"));
}

// ----------

template <>
auto Bytes::Decoder::decode() -> std::optional<Message::Species> {
  auto value = decode<std::underlying_type<Message::Species>::type>();
  if (!value.has_value()) {
    return std::nullopt;
  }

  return type2enum<Message::Species>(value.value());
}
template <>
void Bytes::Encoder::encode(const Message::Species& value) {
  encode(enum2type<Message::Species>(value       ));
}
template <>
auto Bytes::Encoder::length(const Message::Species& value) -> std::size_t {
  return
  length(enum2type<Message::Species>(value       ));
}

template <>
auto Bytes::Decoder::decode() -> std::optional<Message::Announce        ::Variant> {
  auto value = decode<std::underlying_type<Message::Announce        ::Variant>::type>();
  if (!value.has_value()) {
    return std::nullopt;
  }

  return type2enum<Message::Announce        ::Variant>(value.value());
}
template <>
void Bytes::Encoder::encode(const Message::Announce        ::Variant& value) {
  encode(enum2type<Message::Announce        ::Variant>(value       ));
}
template <>
auto Bytes::Encoder::length(const Message::Announce        ::Variant& value) -> std::size_t {
  return
  length(enum2type<Message::Announce        ::Variant>(value       ));
}

template <>
auto Bytes::Decoder::decode() -> std::optional<Message::AnnounceResponse::Variant> {
  auto value = decode<std::underlying_type<Message::AnnounceResponse::Variant>::type>();
  if (!value.has_value()) {
    return std::nullopt;
  }

  return type2enum<Message::AnnounceResponse::Variant>(value.value());
}
template <>
void Bytes::Encoder::encode(const Message::AnnounceResponse::Variant& value) {
  encode(enum2type<Message::AnnounceResponse::Variant>(value       ));
}
template <>
auto Bytes::Encoder::length(const Message::AnnounceResponse::Variant& value) -> std::size_t {
  return
  length(enum2type<Message::AnnounceResponse::Variant>(value       ));
}

template <>
auto Bytes::Decoder::decode() -> std::optional<Message::Colorize        ::Variant> {
  auto value = decode<std::underlying_type<Message::Colorize        ::Variant>::type>();
  if (!value.has_value()) {
    return std::nullopt;
  }

  return type2enum<Message::Colorize        ::Variant>(value.value());
}
template <>
void Bytes::Encoder::encode(const Message::Colorize        ::Variant& value) {
  encode(enum2type<Message::Colorize        ::Variant>(value       ));
}
template <>
auto Bytes::Encoder::length(const Message::Colorize        ::Variant& value) -> std::size_t {
  return
  length(enum2type<Message::Colorize        ::Variant>(value       ));
}

template <>
auto Bytes::Decoder::decode() -> std::optional<Message::ColorizeResponse::Variant> {
  auto value = decode<std::underlying_type<Message::ColorizeResponse::Variant>::type>();
  if (!value.has_value()) {
    return std::nullopt;
  }

  return type2enum<Message::ColorizeResponse::Variant>(value.value());
}
template <>
void Bytes::Encoder::encode(const Message::ColorizeResponse::Variant& value) {
  encode(enum2type<Message::ColorizeResponse::Variant>(value       ));
}
template <>
auto Bytes::Encoder::length(const Message::ColorizeResponse::Variant& value) -> std::size_t {
  return
  length(enum2type<Message::ColorizeResponse::Variant>(value       ));
}

// ----------

Message::Message(Species species,
                 Variant variant)
    : species_(species),
      variant_(variant) {}

Message::~Message() = default;

auto Message::decode(const Bytes& bytes) -> UniquePtr {
  Bytes::Decoder decoder(bytes);

  auto message = decoder.decode<uint64_t>();
  if (!message.has_value()) {
    LOG_W("Message")
        << STRREF("missing field")
        << STRREF("message");

    return nullptr;
  }

  if (message.value() != MESSAGE) {
    LOG_W("Message")
        << STRREF("invalid field")
        << STRREF("message")
        << STRREF("received")
        << message.value()
        << STRREF("expected")
        << MESSAGE;

    return nullptr;
  }

  auto metainf = decoder.decode<uint32_t>();
  if (!metainf.has_value()) {
    LOG_W("Message")
        << STRREF("missing field")
        << STRREF("metainf");

    return nullptr;
  }

  Species species = type2enum<Species>(metainf.value() >> 0x10);
  Variant variant =                    metainf.value() >> 0x00 ;

  auto instant = decoder.decode<std::chrono::system_clock::time_point>();
  if (!instant.has_value()) {
    LOG_W("Message")
        << STRREF("missing field")
        << STRREF("instant");

    return nullptr;
  }

  UniquePtr uniquePtr;

  switch (species) {
    case Species::ANNOUNCE:
      uniquePtr = Announce        ::decodeBytes(decoder, variant);
      break;

    case Species::ANNOUNCE_RESPONSE:
      uniquePtr = AnnounceResponse::decodeBytes(decoder, variant);
      break;

    case Species::COLORIZE:
      uniquePtr = Colorize        ::decodeBytes(decoder, variant);
      break;

    case Species::COLORIZE_RESPONSE:
      uniquePtr = ColorizeResponse::decodeBytes(decoder, variant);
      break;

    default:
      LOG_W("Message")
          << STRREF("invalid field")
          << STRREF("species")
          << enum2type(species);

      return nullptr;
  }

  if (uniquePtr) {
    uniquePtr->instant_ = instant.value();

    auto msgCrcSum = decoder.decode<CrcSum>();
    if (!msgCrcSum.has_value()) {
      LOG_W("Message")
          << STRREF("missing field")
          << STRREF("crcsum");

      return nullptr;
    }

    auto ourCrcSum = crcsum(decoder.bytes(), decoder.index() - sizeof(CrcSum));
    if ( ourCrcSum != msgCrcSum.value()) {
      LOG_W("Message")
          << STRREF("invalid field")
          << STRREF("crcsum")
          << STRREF("received")
          << msgCrcSum.value()
          << STRREF("expected")
          << ourCrcSum;

      return nullptr;
    }
  }

  return uniquePtr;
}

auto Message::encode() const -> Bytes {
  Bytes::Encoder encoder;

  encoder.bytes().reserve(
      256  // educated guess
  );

  auto msg = MESSAGE;
  encoder.encode(msg);

  encodeBytes(encoder);

  CrcSum crcSum = crcsum(encoder.bytes());
  encoder.encode(crcSum);

  return std::move(encoder.bytes());
}

// ----------

template <>
String toString<Message>(const Message& value) {
  return value.toString();
}

// ----------

Message::Announce::Announce(Construction)
    : Message(Species::ANNOUNCE, enum2type(Variant::VVOID)) {
  outputPreferencesList_.reserve(
      ARRAY_SIZE(config::OUTPUTS)
  );
}

Message::Announce::UniquePtr
Message::Announce::decodeBytes(Bytes::Decoder& decoder, Message::Variant variant) {
  switch (type2enum<Variant>(variant)) {
    case Variant::V0001:
      return decodeV0001(decoder);

    default:
      LOG_W("Message")
          << STRREF("invalid field")
          << STRREF("variant")
          << variant;

      return nullptr;
  }
}

Message::Announce::UniquePtr
Message::Announce::decodeV0001(Bytes::Decoder& decoder) {
  UniquePtr uniquePtr = Announce::constructUnique();

  auto name = decoder.decode<Name>();
  if (!name.has_value()) {
    LOG_W("Message")
        << STRREF("missing field")
        << STRREF("name");

    return nullptr;
  }
  uniquePtr->setName(name.value());

  auto vers = decoder.decode<Vers>();
  if (!vers.has_value()) {
    LOG_W("Message")
        << STRREF("missing field")
        << STRREF("vers");

    return nullptr;
  }
  uniquePtr->setVers(vers.value());

  {
    DevicePreferences devicePreferences;

    auto uuid = decoder.decode<Uuid>();
    if (!uuid.has_value()) {
      LOG_W("Message")
          << STRREF("missing field")
          << STRREF("uuid");

      return nullptr;
    }
    devicePreferences.setUuid(uuid.value());

    auto name = decoder.decode<Name>();
    if (!name.has_value()) {
      LOG_W("Message")
          << STRREF("missing field")
          << STRREF("name");

      return nullptr;
    }
    devicePreferences.setName(name.value());

    uniquePtr->setDevicePreferencesItem(
        devicePreferences
    );
  }

  auto outputPreferencesSize = decoder.decode<uint16_t>();
  if (!outputPreferencesSize.has_value()) {
    LOG_W("Message")
        << STRREF("missing field")
        << STRREF("outputPreferencesSize");

    return nullptr;
  }

  for (uint16_t i = 0; i < outputPreferencesSize; i++) {
    OutputPreferences outputPreferences;

    auto uuid = decoder.decode<Uuid>();
    if (!uuid.has_value()) {
      LOG_W("Message")
          << STRREF("missing field")
          << STRREF("uuid");

      return nullptr;
    }
    outputPreferences.setUuid(uuid.value());

    auto name = decoder.decode<Name>();
    if (!name.has_value()) {
      LOG_W("Message")
          << STRREF("missing field")
          << STRREF("name");

      return nullptr;
    }
    outputPreferences.setName(name.value());

    auto leds = decoder.decode<uint16_t>();
    if (!leds.has_value()) {
      LOG_W("Message")
          << STRREF("missing field")
          << STRREF("leds");

      return nullptr;
    }
    outputPreferences.setLeds(leds.value());

    uniquePtr->addOutputPreferencesItem(
        outputPreferences
    );
  }

  return uniquePtr;
}

void
Message::Announce::encodeBytes(Bytes::Encoder& encoder) const {
  uint32_t metainf = enum2type(Species::ANNOUNCE);
  metainf = (metainf << 0x10) | enum2type(Variant::V0001);

  encoder.encode(metainf );
  encoder.encode(instant_);

  encodeV0001(encoder);
}

void
Message::Announce::encodeV0001(Bytes::Encoder& encoder) const {
  encoder.encode(name_);
  encoder.encode(vers_);

  {
    encoder.encode(devicePreferencesItem_.uuid());
    encoder.encode(devicePreferencesItem_.name());
  }

  uint16_t outputPreferencesSize = outputPreferencesList_.size();
  encoder.encode(outputPreferencesSize);

  for (uint16_t i = 0; i < outputPreferencesSize; i++) {
    encoder.encode(outputPreferencesList_[i].uuid());
    encoder.encode(outputPreferencesList_[i].name());
    encoder.encode(outputPreferencesList_[i].leds());
  }
}

String Message::Announce::toString() const {
  String string = STRREF("Message::Announce");
  string.append("{");

  string.append("name='");
  string.append(lvd::ambiramus::toString(name_));
  string.append("',");

  string.append("name='");
  string.append(lvd::ambiramus::toString(vers_));
  string.append("'" );

  string.append("}");
  return string;
}

// ----------

Message::AnnounceResponse::AnnounceResponse(Construction)
    : Message(Species::ANNOUNCE_RESPONSE, enum2type(Variant::VVOID)) {}

Message::AnnounceResponse::UniquePtr
Message::AnnounceResponse::decodeBytes(Bytes::Decoder& decoder, Message::Variant variant) {
  switch (type2enum<Variant>(variant)) {
    case Variant::V0001:
      return decodeV0001(decoder);

    default:
      LOG_W("Message")
          << STRREF("invalid field")
          << STRREF("variant")
          << variant;

      return nullptr;
  }
}

Message::AnnounceResponse::UniquePtr
Message::AnnounceResponse::decodeV0001(Bytes::Decoder& decoder) {
  UniquePtr uniquePtr = AnnounceResponse::constructUnique();

  {
    MasterPreferences masterPreferences;

    auto uuid = decoder.decode<Uuid>();
    if (!uuid.has_value()) {
      LOG_W("Message")
          << STRREF("missing field")
          << STRREF("uuid");

      return nullptr;
    }
    masterPreferences.setUuid(uuid.value());

    auto name = decoder.decode<Name>();
    if (!name.has_value()) {
      LOG_W("Message")
          << STRREF("missing field")
          << STRREF("name");

      return nullptr;
    }
    masterPreferences.setName(name.value());

    uniquePtr->setMasterPreferencesItem(
        masterPreferences
    );
  }

  return uniquePtr;
}

void
Message::AnnounceResponse::encodeBytes(Bytes::Encoder& encoder) const {
  uint32_t metainf = enum2type(Species::ANNOUNCE_RESPONSE);
  metainf = (metainf << 0x10) | enum2type(Variant::V0001);

  encoder.encode(metainf );
  encoder.encode(instant_);

  encodeV0001(encoder);
}

void
Message::AnnounceResponse::encodeV0001(Bytes::Encoder& encoder) const {
  {
    encoder.encode(masterPreferencesItem_.uuid());
    encoder.encode(masterPreferencesItem_.name());
  }
}

String Message::AnnounceResponse::toString() const {
  String string = STRREF("Message::AnnounceResponse");
  return string;
}

// ----------

Message::Colorize::Colorize(Construction)
    : Message(Species::COLORIZE, enum2type(Variant::VVOID)) {}

Message::Colorize::UniquePtr
Message::Colorize::decodeBytes(Bytes::Decoder& decoder, Message::Variant variant) {
  switch (type2enum<Variant>(variant)) {
    case Variant::V0001:
      return decodeV0001(decoder);

    default:
      LOG_W("Message")
          << STRREF("invalid field")
          << STRREF("variant")
          << variant;

      return nullptr;
  }
}

Message::Colorize::UniquePtr
Message::Colorize::decodeV0001(Bytes::Decoder& decoder) {
  UniquePtr uniquePtr = Colorize::constructUnique();

//auto deviceUuid = decoder.decode<Uuid>();
//if (!deviceUuid.has_value()) {
//  LOG_W("Message")
//      << STRREF("missing field")
//      << STRREF("deviceUuid");

//  return nullptr;
//}
//uniquePtr->setDeviceUuid(std::move(deviceUuid.value()));

  auto outputUuid = decoder.decode<Uuid>();
  if (!outputUuid.has_value()) {
    LOG_W("Message")
        << STRREF("missing field")
        << STRREF("outputUuid");

    return nullptr;
  }
  uniquePtr->setOutputUuid(std::move(outputUuid.value()));

  auto colors = decoder.decode<Colors>();
  if (!colors.has_value()) {
    LOG_W("Message")
        << STRREF("missing field")
        << STRREF("colors");

    return nullptr;
  }
  uniquePtr->setColors    (std::move(colors    .value()));

  return uniquePtr;
}

void
Message::Colorize::encodeBytes(Bytes::Encoder& encoder) const {
  uint32_t metainf = enum2type(Species::COLORIZE);
  metainf = (metainf << 0x10) | enum2type(Variant::V0001);

  encoder.encode(metainf );
  encoder.encode(instant_);

  encodeV0001(encoder);
}

void
Message::Colorize::encodeV0001(Bytes::Encoder& encoder) const {
//encoder.encode(deviceUuid_);
  encoder.encode(outputUuid_);
  encoder.encode(colors_    );
}

String Message::Colorize::toString() const {
  String string = STRREF("Message::Colorize");
  string.append("{");

  string.append("outputUuid='");
  string.append(lvd::ambiramus::toString(outputUuid_));
  string.append("'" );

  string.append("}");
  return string;
}

// ----------

Message::ColorizeResponse::ColorizeResponse(Construction)
    : Message(Species::COLORIZE_RESPONSE, enum2type(Variant::VVOID)) {}

auto
Message::ColorizeResponse::create(const Message::Colorize* message) -> UniquePtr {
  UniquePtr uniquePtr = ColorizeResponse::constructUnique();

  uniquePtr->setOutputUuid(message->outputUuid());
  uniquePtr->setColors    (message->colors    ());

  return uniquePtr;
}

Message::ColorizeResponse::UniquePtr
Message::ColorizeResponse::decodeBytes(Bytes::Decoder& decoder, Message::Variant variant) {
  switch (type2enum<Variant>(variant)) {
    case Variant::V0001:
      return decodeV0001(decoder);

    default:
      LOG_W("Message")
          << STRREF("invalid field")
          << STRREF("variant")
          << variant;

      return nullptr;
  }
}

Message::ColorizeResponse::UniquePtr
Message::ColorizeResponse::decodeV0001(Bytes::Decoder& decoder) {
  UniquePtr uniquePtr = ColorizeResponse::constructUnique();

//auto deviceUuid = decoder.decode<Uuid>();
//if (!deviceUuid.has_value()) {
//  LOG_W("Message")
//      << STRREF("missing field")
//      << STRREF("deviceUuid");

//  return nullptr;
//}
//uniquePtr->setDeviceUuid(std::move(deviceUuid.value()));

  auto outputUuid = decoder.decode<Uuid>();
  if (!outputUuid.has_value()) {
    LOG_W("Message")
        << STRREF("missing field")
        << STRREF("outputUuid");

    return nullptr;
  }
  uniquePtr->setOutputUuid(std::move(outputUuid.value()));

  auto colors = decoder.decode<Colors>();
  if (!colors.has_value()) {
    LOG_W("Message")
        << STRREF("missing field")
        << STRREF("colors");

    return nullptr;
  }
  uniquePtr->setColors    (std::move(colors    .value()));

  return uniquePtr;
}

void
Message::ColorizeResponse::encodeBytes(Bytes::Encoder& encoder) const {
  uint32_t metainf = enum2type(Species::COLORIZE_RESPONSE);
  metainf = (metainf << 0x10) | enum2type(Variant::V0001);

  encoder.encode(metainf );
  encoder.encode(instant_);

  encodeV0001(encoder);
}

void
Message::ColorizeResponse::encodeV0001(Bytes::Encoder& encoder) const {
//encoder.encode(deviceUuid_);
  encoder.encode(outputUuid_);
  encoder.encode(colors_    );
}

String Message::ColorizeResponse::toString() const {
  String string = STRREF("Message::ColorizeResponse");
  string.append("{");

  string.append("outputUuid='");
  string.append(lvd::ambiramus::toString(outputUuid_));
  string.append("'" );

  string.append("}");
  return string;
}

}  // namespace ambiramus
}  // namespace lvd
