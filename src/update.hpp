/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <memory>

#include "bytes.hpp"
#include "module.hpp"
#include "object.hpp"
#include "signal.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class Update : public Object<Update>, public Module {
 public:
  Update(Construction);
  ~Update();

 public:
  virtual void setup() override;
  virtual void loop () override;
  virtual void close() override;

 public:
  bool updateSetup();
  bool updateData(const Bytes& bytes);
  bool updateClose();

  bool updateAbort();

 private:
  void updateSuccess();
  void updateFailure();

 public:
  Signal<void()> sigSuccess;
  Signal<void()> sigFailure;

 private:
  class           Impl;
  std::unique_ptr<Impl> impl_;
};

}  // namespace ambiramus
}  // namespace lvd
