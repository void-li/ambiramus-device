/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <memory>

#include "module.hpp"
#include "object.hpp"
#include "system.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class Reboot : public Object<Reboot>, public Module {
 public:
  Reboot(Construction, System::SharedPtr system);
  ~Reboot();

 public:
  virtual void setup() override;
  virtual void loop () override;
  virtual void close() override;

 public:
  void reboot();

 private:
  System::SharedPtr system_;
};

}  // namespace ambiramus
}  // namespace lvd
