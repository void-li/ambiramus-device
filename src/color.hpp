/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <cstdint>
#include <optional>

#include "string.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class Color {
 public:
  Color()
      : r_(0), g_(0), b_(0) {}

  Color(uint8_t r, uint8_t g, uint8_t b)
      : r_(r), g_(g), b_(b) {}

 public:
  static auto construct(const String& colorString) -> std::optional<Color>;

 public:
  auto toHex() const -> String;

 public:
  uint8_t r() const {
    return r_;
  }
  uint8_t g() const {
    return g_;
  }
  uint8_t b() const {
    return b_;
  }

 private:
  uint8_t r_ = 0x00;
  uint8_t g_ = 0x00;
  uint8_t b_ = 0x00;
};

// ----------

inline bool operator==(const Color& lhs,
                       const Color& rhs) {
  if (lhs.r() != rhs.r()) {
    return false;
  }

  if (lhs.g() != rhs.g()) {
    return false;
  }

  if (lhs.b() != rhs.b()) {
    return false;
  }

  return true;
}

inline bool operator!=(const Color& lhs,
                       const Color& rhs) {
  return !(lhs == rhs);
}

}  // namespace ambiramus
}  // namespace lvd
