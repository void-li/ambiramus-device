/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "output_impl.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <iomanip>
#include <iostream>

// ----------

namespace lvd {
namespace ambiramus {

class Output::Impl::Data {

};

// ----------

Output::Impl::Impl(Output* output)
    : data_(std::make_unique<Data>()),
      output_(output) {}

Output::Impl::~Impl() = default;

void Output::Impl::setup() {}

void Output::Impl::colorize(const Colors& colors) {
  std::cout << "Output::colorize "
            << toString(output_->uuid()).toStdString()
            << " "
            << toString(output_->name()).toStdString();

  for (const Color& color : colors) {
    std::cout << " "
              << "#"
              << std::setfill('0') << std::setw(2)
              << std::hex << (int)color.r()
              << std::setfill('0') << std::setw(2)
              << std::hex << (int)color.g()
              << std::setfill('0') << std::setw(2)
              << std::hex << (int)color.b();
  }

  std::cout << std::endl;
}

}  // namespace ambiramus
}  // namespace lvd
