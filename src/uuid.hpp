/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <optional>

#include "string.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class Uuid {
 public:
  Uuid()
      : valid_(false),
        part0_(0x000),
        part1_(0x000) {}

  Uuid(uint64_t part0, uint64_t part1)
      : valid_(true ),
        part0_(part0),
        part1_(part1) {}

 public:
  static auto construct(const String& uuidStr) -> std::optional<Uuid>;

 public:
  bool valid() const {
    return valid_;
  }

 public:
  auto part0() const -> uint64_t {
    return part0_;
  }
  auto part1() const -> uint64_t {
    return part1_;
  }

 private:
  template <class T>
  friend String toString(const T& value);

 private:
  mutable String uuid_;
  bool valid_ = false;

 private:
  uint64_t part0_ = 0x0000000000000000;
  uint64_t part1_ = 0x0000000000000000;
};

// ----------

inline bool operator==(const Uuid& lhs,
                       const Uuid& rhs) {
  if (lhs.part0() != rhs.part0()) {
    return false;
  }

  if (lhs.part1() != rhs.part1()) {
    return false;
  }

  return true;
}

inline bool operator!=(const Uuid& lhs,
                       const Uuid& rhs) {
  return !(lhs == rhs);
}

}  // namespace ambiramus
}  // namespace lvd
