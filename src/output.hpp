/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <memory>

#include "colors.hpp"
#include "name.hpp"
#include "object.hpp"
#include "output_id.hpp"
#include "output_preferences.hpp"
#include "uuid.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class Output : public Object<Output> {
 public:
  Output(Construction,
         const OutputId           outputId,
         const OutputPreferences& outputPreferences);
  ~Output();

 public:
  void colorize();
  void colorize(const Colors& colors);

  void colorize(const Color & color );
  void colorize(const Color & color , std::size_t index);

 public:
  auto outputId() const -> OutputId {
    return outputId_;
  }

  auto outputPreferences() const -> const OutputPreferences& {
    return outputPreferences_;
  }
  void setOutputPreferences(const OutputPreferences& outputPreferences) {
    outputPreferences_ = outputPreferences;
  }

  auto uuid() const -> const Uuid& {
    return outputPreferences_.uuid();
  }
  auto name() const -> const Name& {
    return outputPreferences_.name();
  }
  auto leds() const -> uint16_t {
    return outputPreferences_.leds();
  }

 public:
  auto colors() const -> const Colors& {
    return colors_;
  }

 private:
  class           Impl;
  std::unique_ptr<Impl> impl_;

 private:
  const OutputId          outputId_;
  /***/ OutputPreferences outputPreferences_;

 private:
  Colors colors_;
};

}  // namespace ambiramus
}  // namespace lvd
