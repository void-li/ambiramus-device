/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "device_preferences_manager.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "decoder_util.hpp"
#include "encoder_util.hpp"
#include "enum.hpp"
#include "logger.hpp"

// ----------

namespace lvd {
namespace ambiramus {

auto DevicePreferencesManager::load() -> DevicePreferences {
  LOG_D("DevicePreferencesManager")
      << STRREF("load")
      << deviceId_;

  const Bytes& bytes = preferences_->load(
      Preferences::Species::DEVICE_PREFERENCES, enum2type(deviceId_)
  );

  Bytes::Decoder decoder    ( bytes );
  DecoderUtil    decoderUtil(decoder);

  DevicePreferences devicePreferences;

  for (auto subject = decoderUtil.decode<DevicePreferences::Subject>();
            subject.has_value();
            subject = decoderUtil.decode<DevicePreferences::Subject>()) {
    switch (subject.value()) {
      case DevicePreferences::Subject::UUID: {
        auto uuid = decoderUtil.data<decltype(devicePreferences.uuid_)>();
        if (uuid.has_value()) {
          devicePreferences.setUuid(uuid.value());
        }
      } break;

      case DevicePreferences::Subject::NAME: {
        auto name = decoderUtil.data<decltype(devicePreferences.name_)>();
        if (name.has_value()) {
          devicePreferences.setName(name.value());
        }
      } break;

      default:
        LOG_W("DevicePreferencesManager")
            << STRREF("unknown subject")
            << enum2type(subject.value());
    }
  }

  return devicePreferences;
}

void DevicePreferencesManager::save(const DevicePreferences& devicePreferences) {
  LOG_D("DevicePreferencesManager")
      << STRREF("save")
      << deviceId_;

  Bytes::Encoder encoder;
  EncoderUtil    encoderUtil(encoder);

  encoder.bytes().reserve(
      + EncoderUtil::METASIZE + Bytes::Encoder::length(devicePreferences.uuid())
      + EncoderUtil::METASIZE + Bytes::Encoder::length(devicePreferences.name())
  );

  encoderUtil.encode(DevicePreferences::Subject::UUID,
                     devicePreferences.uuid());

  encoderUtil.encode(DevicePreferences::Subject::NAME,
                     devicePreferences.name());

  preferences_->save(
      Preferences::Species::DEVICE_PREFERENCES, enum2type(deviceId_), encoder.bytes()
  );

  preferences_->commit();
}

}  // namespace ambiramus
}  // namespace lvd
