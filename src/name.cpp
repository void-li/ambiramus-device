/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "name.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "bytes.hpp"
#include "logger.hpp"

// ----------

namespace lvd {
namespace ambiramus {

const std::size_t SIZE = 36;

auto Name::construct(const String& nameString) -> std::optional<Name> {
  if (nameString.size() > SIZE) {
    LOG_W("Name")
        << STRREF("invalid size");

    return std::nullopt;
  }

  Name name;
  name.name_  = nameString;
  name.valid_ = true;

  return name;
}

// ----------

template <>
void Bytes::Encoder::encode(const Name& value) {
  encode(toString(value));
}

template <>
auto Bytes::Decoder::decode() -> std::optional<Name> {
  auto value = decode<String>();
  if (!value.has_value()) {
    return std::nullopt;
  }

  return Name::construct(value.value());
}

template <>
auto Bytes::Encoder::length(const Name& value) -> std::size_t {
  return
  length(toString(value));
}

}  // namespace ambiramus
}  // namespace lvd
