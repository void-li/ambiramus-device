/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "bytes.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <chrono>
#include <cstring>
#include <string>

// ----------

namespace {
using namespace lvd::ambiramus;

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wshift-count-overflow"

template <class T>
T hton(T t) {
  T r;

  Byte* p = reinterpret_cast<Byte*>(&r) + sizeof(T);

  for (uint8_t i = 0; i < sizeof(T); i++) {
    *--p = t;
    t  >>= 8;
  }

  return r;
}

template <class T>
T ntoh(T t) {
  T r;

  Byte* p = reinterpret_cast<Byte*>(&r) + sizeof(T);

  for (uint8_t i = 0; i < sizeof(T); i++) {
    *--p = t;
    t  >>= 8;
  }

  return r;
}

#pragma clang diagnostic pop

template <class T>
auto decodeNumber(const Bytes& bytes, std::size_t& index) -> std::optional<T> {
  auto jndex = index + sizeof(T);
  if (jndex > bytes.size()) {
    return std::nullopt;
  }

  T            vdata;
  std::memcpy(&vdata, &bytes[index], sizeof(vdata));

  index = jndex;
  return ntoh(vdata);
}

template <class T>
void encodeNumber(      Bytes& bytes, const T    & value) {
  T vdata = hton(value);

  auto index = bytes.size();
  bytes.resize(bytes.size() + sizeof(vdata));

  std::memcpy(&bytes[index], &vdata, sizeof(vdata));
}

}  // namespace

// ----------

namespace lvd {
namespace ambiramus {

template <>
void Bytes::Encoder::encode(const uint8_t & value) {
  return encodeNumber<uint8_t >(bytes_, value );
}
template <>
auto Bytes::Decoder::decode() -> std::optional<uint8_t > {
  return decodeNumber<uint8_t >(bytes_, index_);
}
template <>
auto Bytes::Encoder::length(const uint8_t &      ) -> std::size_t {
  return sizeof(uint8_t );
}

template <>
void Bytes::Encoder::encode(const  int8_t & value) {
  return encodeNumber< int8_t >(bytes_, value );
}
template <>
auto Bytes::Decoder::decode() -> std::optional< int8_t > {
  return decodeNumber< int8_t >(bytes_, index_);
}
template <>
auto Bytes::Encoder::length(const  int8_t &      ) -> std::size_t {
  return sizeof( int8_t );
}

template <>
void Bytes::Encoder::encode(const uint16_t& value) {
  return encodeNumber<uint16_t>(bytes_, value );
}
template <>
auto Bytes::Decoder::decode() -> std::optional<uint16_t> {
  return decodeNumber<uint16_t>(bytes_, index_);
}
template <>
auto Bytes::Encoder::length(const uint16_t&      ) -> std::size_t {
  return sizeof(uint16_t);
}

template <>
void Bytes::Encoder::encode(const  int16_t& value) {
  return encodeNumber< int16_t>(bytes_, value );
}
template <>
auto Bytes::Decoder::decode() -> std::optional< int16_t> {
  return decodeNumber< int16_t>(bytes_, index_);
}
template <>
auto Bytes::Encoder::length(const  int16_t&      ) -> std::size_t {
  return sizeof( int16_t);
}

template <>
void Bytes::Encoder::encode(const uint32_t& value) {
  return encodeNumber<uint32_t>(bytes_, value );
}
template <>
auto Bytes::Decoder::decode() -> std::optional<uint32_t> {
  return decodeNumber<uint32_t>(bytes_, index_);
}
template <>
auto Bytes::Encoder::length(const uint32_t&      ) -> std::size_t {
  return sizeof(uint32_t);
}

template <>
void Bytes::Encoder::encode(const  int32_t& value) {
  return encodeNumber< int32_t>(bytes_, value );
}
template <>
auto Bytes::Decoder::decode() -> std::optional< int32_t> {
  return decodeNumber< int32_t>(bytes_, index_);
}
template <>
auto Bytes::Encoder::length(const  int32_t&      ) -> std::size_t {
  return sizeof( int32_t);
}

template <>
void Bytes::Encoder::encode(const uint64_t& value) {
  return encodeNumber<uint64_t>(bytes_, value );
}
template <>
auto Bytes::Decoder::decode() -> std::optional<uint64_t> {
  return decodeNumber<uint64_t>(bytes_, index_);
}
template <>
auto Bytes::Encoder::length(const uint64_t&      ) -> std::size_t {
  return sizeof(uint64_t);
}

template <>
void Bytes::Encoder::encode(const  int64_t& value) {
  return encodeNumber< int64_t>(bytes_, value );
}
template <>
auto Bytes::Decoder::decode() -> std::optional< int64_t> {
  return decodeNumber< int64_t>(bytes_, index_);
}
template <>
auto Bytes::Encoder::length(const  int64_t&      ) -> std::size_t {
  return sizeof( int64_t);
}

template <>
void Bytes::Encoder::encode(const float & value) {
  float  vdata = value;

  auto index = bytes_.size();
  bytes_.resize(bytes_.size() + sizeof(vdata));

  std::memcpy(&bytes_[index ], &vdata, sizeof(vdata));
}
template <>
auto Bytes::Decoder::decode() -> std::optional<float > {
  auto jndex = index_ + sizeof(float );
  if (jndex > bytes_.size()) {
    return std::nullopt;
  }

  float        vdata;
  std::memcpy(&vdata, &bytes_[index_], sizeof(vdata));

  index_ = jndex;
  return vdata;
}
template <>
auto Bytes::Encoder::length(const float &      ) -> std::size_t {
  return sizeof(float );
}

template <>
void Bytes::Encoder::encode(const double& value) {
  double vdata = value;

  auto index = bytes_.size();
  bytes_.resize(bytes_.size() + sizeof(vdata));

  std::memcpy(&bytes_[index ], &vdata, sizeof(vdata));
}
template <>
auto Bytes::Decoder::decode() -> std::optional<double> {
  auto jndex = index_ + sizeof(double);
  if (jndex > bytes_.size()) {
    return std::nullopt;
  }

  double       vdata;
  std::memcpy(&vdata, &bytes_[index_], sizeof(vdata));

  index_ = jndex;
  return vdata;
}
template <>
auto Bytes::Encoder::length(const double&      ) -> std::size_t {
  return sizeof(double);
}

// ----------

template <>
void Bytes::Encoder::encode(const bool& value) {
  return encode<uint8_t>(value ? 0xFF : 0x00);
}
template <>
auto Bytes::Decoder::decode() -> std::optional<bool> {
  auto value = decode<uint8_t>();
  if (!value.has_value()) {
    return std::nullopt;
  }

  return value.value() ? true : false;
}
template <>
auto Bytes::Encoder::length(const bool&      ) -> std::size_t {
  return sizeof(uint8_t);
}

template <>
void Bytes::Encoder::encode(const char& value) {
  return encode< int8_t>(value);
}
template <>
auto Bytes::Decoder::decode() -> std::optional<char> {
  auto value = decode< int8_t>();
  if (!value.has_value()) {
    return std::nullopt;
  }

  return value.value();
}
template <>
auto Bytes::Encoder::length(const char&      ) -> std::size_t {
  return sizeof( int8_t);
}

// ----------

template <>
void Bytes::Encoder::encode(
    const std::chrono::system_clock::time_point& value
) {
  uint64_t vdata = value.time_since_epoch().count();
  encode(vdata);
}
template <>
auto Bytes::Decoder::decode() -> std::optional<std::chrono::system_clock::time_point> {
  auto vdata = decode<uint64_t>();
  if (!vdata.has_value()) {
    return std::nullopt;
  }

  return std::chrono::system_clock::time_point(
      std::chrono::system_clock::duration(vdata.value())
  );
}
template <>
auto Bytes::Encoder::length(
    const std::chrono::system_clock::time_point&
) -> std::size_t {
  return sizeof(uint64_t);
};

}  // namespace ambiramus
}  // namespace lvd
