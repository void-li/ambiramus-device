/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "uuid_util_impl.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <QUuid>

// ----------

namespace lvd {
namespace ambiramus {

Uuid UuidUtil::Impl::createRandom() {
  return Uuid::construct(
      String::construct(
          QUuid::createUuid().toString(QUuid::WithoutBraces).toStdString().c_str()
      )
  ).value();
}

}  // namespace ambiramus
}  // namespace lvd
