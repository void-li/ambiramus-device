/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <optional>

#include "bytes.hpp"
#include "enum.hpp"
#include "logger.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class DecoderUtil {
 public:
  DecoderUtil(Bytes::Decoder& decoder)
      : decoder_(decoder) {}

 public:
  template <         class Subject>
  auto decode    () -> std::optional<Subject > {
    auto subject = decodeImpl();
    if (!subject.has_value()) {
      return std::nullopt;
    }

    return type2enum<Subject>(subject.value());
  }

 private:
//template <                      >
  auto decodeImpl() -> std::optional<uint16_t>;

 public:
  template <class T>
  auto data() -> std::optional<T>;

  void skip();

 private:
  Bytes::Decoder& decoder_;

 private:
  uint16_t subject_ = 0x0000;
  uint16_t footage_ = 0x0000;

  bool     allowed_ = false;
};

// ----------

//template <class T>
inline auto DecoderUtil::decodeImpl() -> std::optional<uint16_t> {
  auto subject = decoder_.decode<uint16_t>();
  if (!subject.has_value()) {
//  LOG_W("DecoderUtil")
//      << STRREF("missing field")
//      << STRREF("subject");

    return std::nullopt;
  }

  auto footage = decoder_.decode<uint16_t>();
  if (!footage.has_value()) {
    LOG_W("DecoderUtil")
        << STRREF("missing field")
        << STRREF("footage");

    return std::nullopt;
  }

  subject_ = subject.value();
  footage_ = footage.value();

  allowed_ = true;

  return subject_;
}

template <class T>
inline auto DecoderUtil::data() -> std::optional<T> {
  if (allowed_) {
    allowed_ = false;

    return decoder_.decode<T>();
  }

  return std::nullopt;
}

inline void DecoderUtil::skip() {
  if (allowed_) {
    allowed_ = false;

    decoder_.setIndex(
        decoder_.index() + footage_
    );
  }
}

}  // namespace ambiramus
}  // namespace lvd
