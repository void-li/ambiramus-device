/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include "name.hpp"
#include "uuid.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class DevicePreferences {
  friend class DevicePreferencesManager;

 public:
  enum class Subject : uint16_t {
    UUID = 0x0000,
    NAME = 0x0010,
  };

 public:
  auto uuid() const -> const Uuid& {
    return uuid_;
  }
  void setUuid(const Uuid& uuid) {
    uuid_ = uuid;
  }

  auto name() const -> const Name& {
    return name_;
  }
  void setName(const Name& name) {
    name_ = name;
  }

 private:
  Uuid uuid_;
  Name name_;
};

// ----------

inline bool operator==(const DevicePreferences& lhs,
                       const DevicePreferences& rhs) {
  if (lhs.uuid() != rhs.uuid()) {
    return false;
  }

  if (lhs.name() != rhs.name()) {
    return false;
  }

  return true;
}

inline bool operator!=(const DevicePreferences& lhs,
                       const DevicePreferences& rhs) {
  return !(lhs == rhs);
}

}  // namespace ambiramus
}  // namespace lvd
