/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "wifiap_impl.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <functional>

#include <ESP8266WiFi.h>

// ----------

namespace {
bool initialized = false;

void*                     callOnStationModeImpl         = nullptr;
std::function<void(void)> callOnStationModeConnected    = nullptr;
std::function<void(void)> callOnStationModeDisconnected = nullptr;

void onStationModeConnected   (const WiFiEventStationModeConnected   &) {
  if (callOnStationModeConnected   ) {
    callOnStationModeConnected   ();
  }
}

void onStationModeDisconnected(const WiFiEventStationModeDisconnected&) {
  if (callOnStationModeDisconnected) {
    callOnStationModeDisconnected();
  }
}

}  // namespace

// ----------

namespace lvd {
namespace ambiramus {

class WifiAp::Impl::Data {

};

// ----------

WifiAp::Impl::Impl(WifiAp* wifiAp)
    : data_(std::make_unique<Data>()),
      wifiAp_(wifiAp) {
  callOnStationModeImpl         =  this;
  callOnStationModeConnected    = [this] {
    wifiAp_->onConnected   ();
  };
  callOnStationModeDisconnected = [this] {
    wifiAp_->onDisconnected();
  };

  if (!initialized) {
    WiFi.persistent(false);

    WiFi.onStationModeConnected   (onStationModeConnected   );
    WiFi.onStationModeDisconnected(onStationModeDisconnected);

    initialized = true;
  }
}

WifiAp::Impl::~Impl() {
  if (callOnStationModeImpl == this) {
    callOnStationModeDisconnected = nullptr;
    callOnStationModeConnected    = nullptr;
    callOnStationModeImpl         = nullptr;
  }
}

void WifiAp::Impl::setup() {
  // void
}

void WifiAp::Impl::loop() {
  // void
}

void WifiAp::Impl::close() {
  // void
}

void WifiAp::Impl::scan() {
  WiFi.scanNetworks(true);
}

void WifiAp::Impl::clientSetup() {
  WiFi.begin(
      wifiAp_->wifiApPreferences_.clientSsid    ().data(),
      wifiAp_->wifiApPreferences_.clientPassword().data()
  );
}

void WifiAp::Impl::clientClose() {
  WiFi.disconnect();
}

bool WifiAp::Impl::clientAlive() const {
  return WiFi.isConnected();
}

bool WifiAp::Impl::clientScans() const {
  auto   scanSize = WiFi.scanComplete();
  return scanSize == WIFI_SCAN_RUNNING;
}

void WifiAp::Impl::accessSetup() {
  WiFi.softAPConfig(
      IPAddress( 10,  11,  12, 13),
      IPAddress( 10,  11,  12, 13),
      IPAddress(255, 255, 255,  0)
  );

  WiFi.softAP(
      wifiAp_->wifiApPreferences_.accessSsid    ().data(),
      wifiAp_->wifiApPreferences_.accessPassword().data()
  );
}

void WifiAp::Impl::accessClose() {
  WiFi.softAPdisconnect();
}

bool WifiAp::Impl::accessAlive() const {
  return WiFi.getMode() & WIFI_AP;
}

auto WifiAp::Impl::accessCount() const -> std::size_t {
  return WiFi.softAPgetStationNum();
}

auto WifiAp::Impl::networkSize() const -> std::size_t {
  auto   scanSize = WiFi.scanComplete();
  return scanSize >= 0 ? scanSize : 0;
}

auto WifiAp::Impl::networkSsid(std::size_t index) const -> String {
  return String::construct(WiFi.SSID(index).c_str());
}

auto WifiAp::Impl::ipAddress() const -> String {
  return String::construct(WiFi.localIP().toString().c_str());
}

}  // namespace ambiramus
}  // namespace lvd
