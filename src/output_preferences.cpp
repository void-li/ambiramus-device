/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "output_preferences.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "string.hpp"

// ----------

namespace lvd {
namespace ambiramus {

template <>
String toString<OutputPreferences>(const OutputPreferences& outputPreferences) {
  String string = STRREF("DevicePreferences");
  string.append("{");

  string.append("uuid='");
  string.append(toString(outputPreferences.uuid()));
  string.append("',");

  string.append("name='");
  string.append(toString(outputPreferences.name()));
  string.append("',");

  string.append("leds='");
  string.append(toString(outputPreferences.leds()));
  string.append("'" );

  string.append("}");
  return string;
}

}  // namespace ambiramus
}  // namespace lvd
