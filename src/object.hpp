/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <memory>

#include "core.hpp"

// ----------

namespace lvd {
namespace ambiramus {

template <class T>
class Object {
 protected:
  class Construction {};

 public:
  using ConstSharedPtr = std::shared_ptr<const T>;
  using      SharedPtr = std::shared_ptr<      T>;

  using ConstUniquePtr = std::unique_ptr<const T>;
  using      UniquePtr = std::unique_ptr<      T>;

 public:
  template <class... U>
  static SharedPtr constructShared(U&&... args) {
    auto self = std::make_shared<T>(Construction(), std::forward<U>(args)...);
    self->Object<T>::self_ = self;
    return self;
  }

  template <class... U>
  static UniquePtr constructUnique(U&&... args) {
    auto self = std::make_unique<T>(Construction(), std::forward<U>(args)...);
  //self->Object<T>::self_ = self; NO!
    return self;
  }

 public:
  std::weak_ptr<T> self() const {
    return self_;
  }

 protected:
  std::weak_ptr<T> self_;
};

}  // namespace ambiramus
}  // namespace lvd
