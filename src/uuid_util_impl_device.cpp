/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "uuid_util_impl.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <Esp.h>

// ----------

namespace lvd {
namespace ambiramus {

Uuid UuidUtil::Impl::createRandom() {
  uint64_t part0 = static_cast<uint64_t>(ESP.random()) << 32
                 | static_cast<uint64_t>(ESP.random());

  uint64_t part1 = static_cast<uint64_t>(ESP.random()) << 32
                 | static_cast<uint64_t>(ESP.random());

  return Uuid(part0, part1);
}

}  // namespace ambiramus
}  // namespace lvd
