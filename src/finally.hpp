/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <functional>

// ----------

namespace lvd {
namespace ambiramus {

#define UNIQUE_NAME                 UNIQUE_NAME_IMPL_A(__unique__, __COUNTER__, __)
#define UNIQUE_NAME_IMPL_A(A, B, C) UNIQUE_NAME_IMPL_B(A, B, C)
#define UNIQUE_NAME_IMPL_B(A, B, C)                    A##B##C

// ----------

template <class F>
class finally {
 public:
  finally& operator<<(F f) {
    f_ = f; return *this;
  }

  ~finally() noexcept {
    f_();
  }

 private:
  F f_;
};

#define FINALLY_HELPER(X) X; X
#define FINALLY lvd::ambiramus::finally<std::function<void(void)>> FINALLY_HELPER(UNIQUE_NAME) << [&]

}  // namespace ambiramus
}  // namespace lvd
