/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <cstddef>
#include <memory>
#include <optional>
#include <vector>

#include "byte.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class Bytes {
 public:
  using Value    =                 std::vector<Byte> ;
  using ValuePtr = std::shared_ptr<std::vector<Byte>>;

 public:
  class Decoder;
  class Encoder;

 public:
  Bytes()
      : value_(std::make_shared<Value>(     )) {}

  Bytes(Value::size_type vsize)
      : value_(std::make_shared<Value>(vsize)) {}

  Bytes(Value::size_type vsize, Value::value_type* value);

 private:
  Bytes          (const char* value);

 public:
  static
  Bytes construct(const char* value) {
    return value;
  }

 public:
  auto size() const -> Value::size_type {
    return value_->size();
  }

  bool empty() const {
    return size() == 0;
  }

  auto data() const -> const Value::value_type* {
    return value_->data();
  }

  auto data()       ->       Value::value_type* {
    detach();
    return value_->data();
  }

 public:
  auto front() const -> Value::const_reference {
    return value_->front();
  }

  auto front()       -> Value::      reference {
    detach();
    return value_->front();
  }

  auto back () const -> Value::const_reference {
    return value_->back ();
  }

  auto back ()       -> Value::      reference {
    detach();
    return value_->back ();
  }

  auto begin() const -> Value::const_iterator {
    return value_->begin();
  }

  auto begin()       -> Value::      iterator {
    detach();
    return value_->begin();
  }

  auto end  () const -> Value::const_iterator {
    return value_->end  ();
  }

  auto end  ()       -> Value::      iterator {
    detach();
    return value_->end  ();
  }

 public:
  void clear() {
    value_ = std::make_shared<Value>();
  }

  void resize (Value::size_type size) {
    detach();
    value_->resize (size);
  }

  void resize (Value::size_type size, Value::value_type item) {
    detach();
    value_->resize (size, item);
  }

  void reserve(Value::size_type size) {
  //detach();
    value_->reserve(size);
  }

  void push_back(Value::value_type item) {
    detach();
    value_->push_back(item);
  }

 public:
  auto operator[](Value::size_type index) const -> Value::const_reference {
    return (*value_)[index];
  }

  auto operator[](Value::size_type index)       -> Value::      reference {
    detach();
    return (*value_)[index];
  }

 private:
  void detach() {
    if (value_.use_count() > 1) {
      value_ = std::make_shared<Value>(*value_);
    }
  }

 private:
  friend bool operator==(const Bytes& lhs, const Bytes& rhs);
  friend bool operator!=(const Bytes& lhs, const Bytes& rhs);

 private:
  ValuePtr value_;
};

// ----------

inline bool operator==(const Bytes& lhs,
                       const Bytes& rhs) {
  return (*lhs.value_) == (*rhs.value_);
}

inline bool operator!=(const Bytes& lhs,
                       const Bytes& rhs) {
  return !(lhs == rhs);
}

// ----------

class Bytes::Encoder {
 public:
  template <class T>
  void encode(const T& value);

 public:
  template <class T>
  static
  auto length(const T& value) -> std::size_t;

 public:
  auto bytes() const -> const Bytes& {
    return bytes_;
  }
  auto bytes()       ->       Bytes& {
    return bytes_;
  }

  auto index() const -> std::size_t {
    return bytes_.size();
  }

 private:
  /***/ Bytes  bytes_;
//std::size_t  index_ = 0;
};

// ----------

class Bytes::Decoder {
 public:
  Decoder(const Bytes& bytes)
      : bytes_(bytes) {}

 public:
  template <class T>
  auto decode() -> std::optional<T>;

 public:
  auto bytes() const -> const Bytes& {
    return bytes_;
  }
  auto bytes()       -> const Bytes& {
    return bytes_;
  }

  auto index() const -> std::size_t {
    return index_;
  }

  void setIndex(std::size_t index) {
    index_ = index;
  }

 private:
  const Bytes& bytes_;
  std::size_t  index_ = 0;
};

}  // namespace ambiramus
}  // namespace lvd
