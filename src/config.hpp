/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <chrono>
#include <cstdio>

#include "device_id.hpp"
#include "output_id.hpp"
#include "string.hpp"

// ----------

namespace lvd {
namespace ambiramus {
namespace config {

using duration = std::chrono::system_clock::duration;

//! TODO description
__attribute__((unused))
static const char*    AMBIRA_NAME            =  "Ambiramus" ;

//! TODO description
__attribute__((unused))
static const char*    AMBIRA_VERS            = "~unknown";

//! TODO description
static const uint16_t CLIENT_PORT            = 8268;

//! TODO description
static const duration CLIENT_PERIOD          = std::chrono::milliseconds( 1000);

//! TODO description
static const uint16_t PORTAL_PORT            = 8266;

//! TODO description
static const duration PORTAL_ACTIVE          = std::chrono::milliseconds(60000);

//! TODO description
static const duration PORTAL_PERIOD          = std::chrono::milliseconds( 1000);

//! TODO description
static const uint16_t SERVER_PORT            = 8267;

//! TODO description
static const duration SYSTEM_PERIOD          = std::chrono::milliseconds(60000);

//! TODO description
static const duration UPDATE_ACTIVE          = std::chrono::milliseconds(10000);

//! TODO description
__attribute__((unused))
static const char*    WIFIAP_CLIENT_SSID     = "";

//! TODO description
__attribute__((unused))
static const char*    WIFIAP_CLIENT_PASSWORD = "";

//! TODO description
__attribute__((unused))
static const char*    WIFIAP_ACCESS_SSID     = "Ambiramus";

//! TODO description
__attribute__((unused))
static const char*    WIFIAP_ACCESS_PASSWORD = "Ambiramus";

//! TODO description
static const duration WIFIAP_HIATUS          = std::chrono::milliseconds(10000);

//! TODO description
static const duration WIFIAP_PERIOD          = std::chrono::milliseconds( 1000);

// ----------

//! TODO description
static const struct {
  DeviceId deviceId;
} DEVICES[] = {
  { DeviceId::DEVICE0 },
};

//! TODO description
static const struct {
  OutputId outputId;
} OUTPUTS[] = {
  { OutputId::OUTPUT0 },
  { OutputId::OUTPUT1 },
};

}  // namespace config
}  // namespace ambiramus
}  // namespace lvd
