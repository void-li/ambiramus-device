/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "wifiap.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "config.hpp"
#include "logger.hpp"
#include "wifiap_impl.hpp"

// ----------

namespace lvd {
namespace ambiramus {

WifiAp::WifiAp(Construction,
               const WifiApPreferences& wifiApPreferences)
    : Module(STRREF("WifiAp")),
      impl_(std::make_unique<Impl>(this)),
      wifiApPreferences_(wifiApPreferences) {}

WifiAp::~WifiAp() = default;

void WifiAp::setup() {
  LOG_D("WifiAp")
      << STRREF("setup");

  impl_->setup();

  activity(STRREF("WifiAp"));
  loopCyclic(STRREF("WifiAp"), config::WIFIAP_PERIOD);

  clientSetup();
//accessSetup(); NO!
}

void WifiAp::loop() {
  LOG_D("WifiAp")
      << STRREF("loop");

  impl_->loop();

  if (impl_->clientAlive()) {
    LOG_I("WifiAp")
        << "IP:"
        << impl_->ipAddress();

    if (!impl_->accessAlive()) {
      // void
    }
    else {
      if (impl_->accessCount() == 0) {
        accessClose();
      }
    }

    if (!impl_->accessAlive() && !impl_->clientScans()) {
      dormant (STRREF("WifiAp"));
      loopIdling(STRREF("WifiAp"));
    }
  }
  else {
    if (!impl_->accessAlive()) {
      if (activityFlag()) {
        auto now = std::chrono::system_clock::now();
        if (now > activityTime() + config::WIFIAP_HIATUS) {
          accessSetup();
        }
      }
    }
  }

  if (scanning_) {
    if (!impl_->clientScans()) {
      Networks networks;

      for (std::size_t i = 0; i < impl_->networkSize(); i++) {
        networks.emplace_back(
            impl_->networkSsid(i)
        );
      }

      scanning_ = false;
      networks_ = networks;
    }
  }
}

void WifiAp::close() {
  LOG_D("WifiAp")
      << STRREF("close");

  impl_->close();

  dormant (STRREF("WifiAp"));
  loopIdling(STRREF("WifiAp"));

  accessClose();
  clientClose();
}

void WifiAp::scan() {
  LOG_D("WifiAp")
      << STRREF("scan");

  impl_->scan();

  activity(STRREF("WifiAp"));
  loopCyclic(STRREF("WifiAp"), config::WIFIAP_PERIOD);

  scanning_ = true;
  networks_.clear();
}

bool WifiAp::scanning() {
  return scanning_;
}

auto WifiAp::networks() -> Networks {
  return networks_;
}

void WifiAp::onConnected   () {
  LOG_I("WifiAp")
      << STRREF("onConnected"   );

  activity(STRREF("WifiAp"));
  loopCyclic(STRREF("WifiAp"), config::WIFIAP_PERIOD);
}

void WifiAp::onDisconnected() {
  LOG_I("WifiAp")
      << STRREF("onDisconnected");

  activity(STRREF("WifiAp"));
  loopCyclic(STRREF("WifiAp"), config::WIFIAP_PERIOD);
}

void WifiAp::clientSetup() {
  LOG_D("WifiAp")
      << STRREF("clientSetup");

  impl_->clientSetup();
  clientAlive_ = true;
}

void WifiAp::clientClose() {
  LOG_D("WifiAp")
      << STRREF("clientClose");

  clientAlive_ = false;
  impl_->clientClose();
}

void WifiAp::accessSetup() {
  LOG_D("WifiAp")
      << STRREF("accessSetup");

  impl_->accessSetup();
  accessAlive_ = true;
}

void WifiAp::accessClose() {
  LOG_D("WifiAp")
      << STRREF("accessClose");

  accessAlive_ = false;
  impl_->accessClose();
}

void WifiAp::setWifiApPreferences(const WifiApPreferences& wifiApPreferences) {
  bool restartClient = false;
  bool restartAccess = false;

  if (clientAlive_) {
    if (   wifiApPreferences.clientSsid    () != wifiApPreferences_.clientSsid    ()
        || wifiApPreferences.clientPassword() != wifiApPreferences_.clientPassword()) {
      restartClient = true;
    }
  }

  if (accessAlive_) {
    if (   wifiApPreferences.accessSsid    () != wifiApPreferences_.accessSsid    ()
        || wifiApPreferences.accessPassword() != wifiApPreferences_.accessPassword()) {
      restartAccess = true;
    }
  }

  wifiApPreferences_ = wifiApPreferences;

  if (clientAlive_ && restartClient) {
    clientClose();
    clientSetup();
  }

  if (accessAlive_ && restartAccess) {
    accessClose();
    accessSetup();
  }
}

}  // namespace ambiramus
}  // namespace lvd
