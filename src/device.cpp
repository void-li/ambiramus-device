/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "device.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "device_impl.hpp"
#include "logger.hpp"

// ----------

namespace lvd {
namespace ambiramus {

Device::Device(Construction,
               const DeviceId           deviceId,
               const DevicePreferences& devicePreferences)
    : impl_(std::make_unique<Impl>(this)),
      deviceId_         (deviceId         ),
      devicePreferences_(devicePreferences) {}

Device::~Device() = default;

}  // namespace ambiramus
}  // namespace lvd
