/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <optional>
#include <vector>

#include "bytes.hpp"
#include "object.hpp"
#include "persistence.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class Preferences : public Object<Preferences> {
 public:
  enum class Species : uint16_t {
    INVALID            = 0x0000,

    DEVICE_PREFERENCES = 0x0001,
    OUTPUT_PREFERENCES = 0x0002,
    PORTAL_PREFERENCES = 0x0003,
    WIFIAP_PREFERENCES = 0x0004,

#ifdef UNIT_TEST
    TEST0              = 0xFFFF,
    TEST1              = 0xFFFE,
#endif
  };
  /**/ using Variant = uint16_t;
  /**/ using Subject = uint16_t;

 private:
  class Unit;

 public:
  Preferences(Construction,
              const Persistence::SharedPtr& persistence);
  ~Preferences();

 public:
  auto load(Species species, Variant variant) -> Bytes;
  void save(Species species, Variant variant, const Bytes& bytes);

  void commit();
  void reload();

 private:
  void encodePage(Bytes::Encoder& encoder);
  bool decodePage(Bytes::Decoder& decoder);

  void encodeUnit(Bytes::Encoder& encoder, const Unit& unit);
  auto decodeUnit(Bytes::Decoder& decoder) -> std::optional<Unit>;

 private:
  Persistence::SharedPtr persistence_;

 private:
  std::vector<Unit> units_;

 private:
  bool        erase_ = false;
  std::size_t index_ = false;
};

}  // namespace ambiramus
}  // namespace lvd
