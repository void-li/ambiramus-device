/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <cstdint>
#include <memory>
#include <vector>

#include "bytes.hpp"
#include "device_manager.hpp"
#include "module.hpp"
#include "object.hpp"
#include "output_manager.hpp"
#include "signal.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class Client : public Object<Client>, public Module {
 public:
  Client(Construction);
  ~Client();

 public:
  virtual void setup() override;
  virtual void loop () override;
  virtual void close() override;

 public:
  void airmailMessage(uint32_t addr, const Bytes& bytes);

 public:
  Signal<DeviceManager*()> reqDeviceManager;
  Signal<OutputManager*()> reqOutputManager;

 private:
  class           Impl;
  std::unique_ptr<Impl> impl_;
};

}  // namespace ambiramus
}  // namespace lvd
