/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <chrono>

#include "signal.hpp"
#include "string.hpp"
#include "timer.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class Module {
 protected:
  using TimePoint = std::chrono::system_clock::time_point;

 protected:
  Module(const String& moduleString);

 public:
  virtual void setup() = 0;
  virtual void loop () = 0;
  virtual void close() = 0;

 protected:
  void loopAlways();
  void loopAlways(const String& module);

  void loopCyclic(                      Timer::Timeout timeout);
  void loopCyclic(const String& module, Timer::Timeout timeout);

  void loopIdling();
  void loopIdling(const String& module);

 protected:
  void activity  ();
  void activity  (const String& module);

  void dormant   ();
  void dormant   (const String& module);

 public:
  auto moduleString() const -> const String& {
    return moduleString_;
  }

 public:
  auto timer() const -> const Timer& {
    return timer_;
  }
  auto timer()       ->       Timer& {
    return timer_;
  }

 protected:
  auto activityTime() -> TimePoint {
    return activityTime_;
  }
  bool activityFlag() {
    return activityFlag_;
  }

 public:
  Signal<void(Module*)> sigLoopAlways;
  Signal<void(Module*)> sigLoopCyclic;
  Signal<void(Module*)> sigLoopIdling;

 private:
  const String moduleString_;

 private:
  enum class State {
    INVALID, ALWAYS, CYCLIC, IDLING
  } state_ = State::INVALID;

 private:
  Timer timer_;

  TimePoint activityTime_;
  bool      activityFlag_ = false;
};

}  // namespace ambiramus
}  // namespace lvd
