/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "system_impl.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <Esp.h>

// ----------

namespace lvd {
namespace ambiramus {

class System::Impl::Data {

};

// ----------

System::Impl::Impl(System* system)
    : data_(std::make_unique<Data>()),
      system_(system) {}

System::Impl::~Impl() = default;

void System::Impl::setup() {
  // void
}

void System::Impl::loop() {
  // void
}

void System::Impl::close() {
  // void
}

auto System::Impl::bootCause() -> String {
  return String::construct(
      ESP.getResetReason().c_str()
  );
}

auto System::Impl::heapFragm() -> uint8_t {
  return ESP.getHeapFragmentation();
}

void System::Impl::reboot() {
  ESP.restart();
}

}  // namespace ambiramus
}  // namespace lvd
