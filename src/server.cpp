/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "server.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "logger.hpp"
#include "server_impl.hpp"

// ----------

namespace lvd {
namespace ambiramus {

Server::Server(Construction)
    : Module(STRREF("Server")),
      impl_(std::make_unique<Impl>(this)) {}

Server::~Server() = default;

void Server::setup() {
  LOG_D("Server")
      << STRREF("setup");

  impl_->setup();

  activity(STRREF("Server"));
  loopAlways(STRREF("Server"));
}

void Server::loop() {
//LOG_D("Server")
//    << STRREF("loop");

  impl_->loop();
}

void Server::close() {
  LOG_D("Server")
      << STRREF("close");

  impl_->close();

  dormant (STRREF("Server"));
  loopIdling(STRREF("Server"));
}

}  // namespace ambiramus
}  // namespace lvd
