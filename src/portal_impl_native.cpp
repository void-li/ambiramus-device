/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "portal_impl.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <cstring>

#include <QByteArray>
#include <QHostAddress>
#include <QHttpServer>
#include <QHttpServerRequest>
#include <QRegularExpression>
#include <QRegularExpressionMatch>

#include "config.hpp"
#include "logger.hpp"
#include "string.hpp"

// ----------

namespace {

const char HEADER  [] =
#include "../portal/header.html.hppd"
;

const char FOOTER  [] =
#include "../portal/footer.html.hppd"
;

const char SYNC_SVG[] =
#include "../portal/sync.svg.hppd"
;

const char WIFI_SVG[] =
#include "../portal/wifi.svg.hppd"
;

}  // namespace

// ----------

namespace {
using namespace lvd::ambiramus;

auto requiredArgument(const QMap<QByteArray, QByteArray>& arguments,
                      const char*                         name) ->              lvd::ambiramus::String  {
  if (!arguments.contains(name)) {
    return lvd::ambiramus::String();
  }

  return lvd::ambiramus::String::construct(
      arguments.value(name).toStdString().c_str()
  );
}

auto optionalArgument(const QMap<QByteArray, QByteArray>& arguments,
                      const char*                         name) -> std::optional<lvd::ambiramus::String> {
  if (!arguments.contains(name)) {
    return std::nullopt  ;
  }

  return lvd::ambiramus::String::construct(
      arguments.value(name).toStdString().c_str()
  );
}

QMap<QByteArray, QByteArray> parseArguments(const QByteArray& qByteArray) {
  QMap<QByteArray, QByteArray> arguments;

  for (int i = 0; i < qByteArray.size();) {
    QByteArray key;
    QByteArray val;

    for (QByteArray* itm : {&key, &val}) {
      for (; i < qByteArray.size() && qByteArray[i] != '=' && qByteArray[i] != '&'; i++) {
        if (qByteArray[i] == '%') {
          if (i + 2 < qByteArray.size()) {
            *itm += QByteArray::fromHex(qByteArray.mid(i + 1, 2));
          }

          i += 2;
          continue;
        }

        if (qByteArray[i] == '+') {
          itm->append(' ');
          continue;
        }

        itm->append(qByteArray[i]);
      }

      i++;
    }

    if (!key.isEmpty()) {
      arguments[key] = val;
    }
  }

  return arguments;
}

}  // namespace

// ----------

namespace lvd {
namespace ambiramus {

class Portal::Impl::Data {
 public:
  QHttpServer qHttpServer;
};

// ----------

Portal::Impl::Impl(Portal* portal)
    : data_(std::make_unique<Data>()), portal_(portal) {
  data_->qHttpServer.route("/",
                           [this](const QHttpServerRequest& qHttpServerRequest) {
    Q_UNUSED(qHttpServerRequest);

    String overview = portal_->overview();

    QHttpServerResponse qHttpServerResponse(
        "text/html", QByteArray::fromStdString(overview.toStdString()),
        QHttpServerResponse::StatusCode::Ok);

    if (portal_->refresh_) {
      qHttpServerResponse.addHeader("Refresh", "1");
    }

    return qHttpServerResponse;
  });

  data_->qHttpServer.route("/colour",
                           [this](const QHttpServerRequest& qHttpServerRequest) {
    auto arguments = parseArguments(qHttpServerRequest.body());

    portal_->onColour(
        requiredArgument(arguments, "output"),
        requiredArgument(arguments, "index" ),
        requiredArgument(arguments, "color" )
    );

    QHttpServerResponse qHttpServerResponse(
        QHttpServerResponse::StatusCode::Ok);
//  qHttpServerResponse.setHeader("Location", "/");

    return qHttpServerResponse;
  });

  data_->qHttpServer.route("/device",
                           [this](const QHttpServerRequest& qHttpServerRequest) {
    auto arguments = parseArguments(qHttpServerRequest.body());

    portal_->onDevice(
        requiredArgument(arguments, "device"),
        optionalArgument(arguments,  "uuid" ),
        optionalArgument(arguments,  "name" )
    );

    QHttpServerResponse qHttpServerResponse(
        QHttpServerResponse::StatusCode::SeeOther);
    qHttpServerResponse.setHeader("Location", "/");

    return qHttpServerResponse;
  });

  data_->qHttpServer.route("/output",
                           [this](const QHttpServerRequest& qHttpServerRequest) {
    auto arguments = parseArguments(qHttpServerRequest.body());

    portal_->onOutput(
        requiredArgument(arguments, "output"),
        optionalArgument(arguments,  "uuid" ),
        optionalArgument(arguments,  "name" ),
        optionalArgument(arguments,  "leds" )
    );

    QHttpServerResponse qHttpServerResponse(
        QHttpServerResponse::StatusCode::SeeOther);
    qHttpServerResponse.setHeader("Location", "/");

    return qHttpServerResponse;
  });

  data_->qHttpServer.route("/portal",
                           [this](const QHttpServerRequest& qHttpServerRequest) {
    auto arguments = parseArguments(qHttpServerRequest.body());

    portal_->onPortal(
        requiredArgument(arguments, "username"),
        requiredArgument(arguments, "password")
    );

    QHttpServerResponse qHttpServerResponse(
        QHttpServerResponse::StatusCode::SeeOther);
    qHttpServerResponse.setHeader("Location", "/");

    return qHttpServerResponse;
  });

  data_->qHttpServer.route("/reboot",
                           [this](const QHttpServerRequest& qHttpServerRequest) {
    portal_->onReboot();

    QHttpServerResponse qHttpServerResponse(
        QHttpServerResponse::StatusCode::SeeOther);
    qHttpServerResponse.setHeader("Location", "/");

    return qHttpServerResponse;
  });

  data_->qHttpServer.route("/system",
                           [this](const QHttpServerRequest& qHttpServerRequest) {
    auto arguments = parseArguments(qHttpServerRequest.body());

    portal_->onSystem(
        requiredArgument(arguments, "logs")
    );

    QHttpServerResponse qHttpServerResponse(
        QHttpServerResponse::StatusCode::SeeOther);
    qHttpServerResponse.setHeader("Location", "/");

    return qHttpServerResponse;
  });

  data_->qHttpServer.route("/update",
                           [this](const QHttpServerRequest& qHttpServerRequest) {
    [this, &qHttpServerRequest] {
      static const QRegularExpression contentTypeExpression(
          "^multipart/form-data; boundary=(?<boundary>.+)$"
      );

      QByteArray contentType = qHttpServerRequest.value("Content-Type");

      auto match = contentTypeExpression.match(contentType);
      if (!match.hasMatch()) {
        return;
      }

      QString boundary = match.captured("boundary");
      QByteArray body = qHttpServerRequest.body();

      int beg;
      beg = body.indexOf(boundary.toLocal8Bit());
      if (beg < 0) {
        return;
      }

      beg = body.indexOf("\r\n\r\n", beg);
      if (beg < 0) {
        return;
      }

      beg += strlen("\r\n\r\n");

      int end;
      end = body.lastIndexOf(boundary.toLocal8Bit());
      if (end < 0) {
        return;
      }

      end = body.lastIndexOf("\r\n", end);
      if (end < 0) {
        return;
      }

      body = body.mid(beg, end - beg);

      QList<QByteArray> parts;

      for (int i = 0; i < body.size(); i += 256) {
        parts.append(body.mid(i, 256));
      }

      bool ok;

      ok = portal_->onUpdateSetup();
      if (!ok) {
        return;
      }

      for (const QByteArray& part : parts) {
        Bytes bytes(part.size());

        std::memcpy(bytes.data(), part.data(), bytes.size());

        ok = portal_->onUpdateData(bytes);
        if (!ok) {
          return;
        }
      }

      ok = portal_->onUpdateClose();
      if (!ok) {
        return;
      }
    }();

    QHttpServerResponse qHttpServerResponse(
        QHttpServerResponse::StatusCode::SeeOther);
    qHttpServerResponse.setHeader("Location", "/");

    return qHttpServerResponse;
  });

  data_->qHttpServer.route("/wifiap",
                           [this](const QHttpServerRequest& qHttpServerRequest) {
    auto arguments = parseArguments(qHttpServerRequest.body());

    portal_->onWifiAp(
        requiredArgument(arguments, "ssid"    ),
        requiredArgument(arguments, "password")
    );

    QHttpServerResponse qHttpServerResponse(
        QHttpServerResponse::StatusCode::SeeOther);
    qHttpServerResponse.setHeader("Location", "/");

    return qHttpServerResponse;
  });

  data_->qHttpServer.route("/wifiap/rescan",
                           [this](const QHttpServerRequest& qHttpServerRequest) {
    Q_UNUSED(qHttpServerRequest);

    portal_->onRescan();

    QHttpServerResponse qHttpServerResponse(
        QHttpServerResponse::StatusCode::SeeOther);
    qHttpServerResponse.setHeader("Location", "/");

    return qHttpServerResponse;
  });

  data_->qHttpServer.route("/sync.svg",
                           [    ](const QHttpServerRequest& qHttpServerRequest) {
    Q_UNUSED(qHttpServerRequest);

    QHttpServerResponse qHttpServerResponse(
        "image/svg+xml", SYNC_SVG, QHttpServerResponse::StatusCode::Ok);

    return qHttpServerResponse;
  });

  data_->qHttpServer.route("/wifi.svg",
                           [    ](const QHttpServerRequest& qHttpServerRequest) {
    Q_UNUSED(qHttpServerRequest);

    QHttpServerResponse qHttpServerResponse(
        "image/svg+xml", WIFI_SVG, QHttpServerResponse::StatusCode::Ok);

    return qHttpServerResponse;
  });

  data_->qHttpServer.listen(
      QHostAddress::Any, config::PORTAL_PORT
  );
}

Portal::Impl::~Impl() = default;

void Portal::Impl::setup() {
  // void
}

void Portal::Impl::loop() {
  // void
}

void Portal::Impl::close() {
  // void
}

String Portal::Impl::header() const {
  return String::construct(HEADER);
}

String Portal::Impl::footer() const {
  return String::construct(FOOTER);
}

}  // namespace ambiramus
}  // namespace lvd
