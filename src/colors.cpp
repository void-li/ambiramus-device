/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "colors.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <cstring>

#include "bytes.hpp"

// ----------

namespace lvd {
namespace ambiramus {

template <>
String toString<Colors>(const Colors& value) {
  String string = STRREF("Colors");
  string.append("[");

  for (std::size_t i = 0; i < value.size(); i++) {
    if (i > 0) {
      string.append(", ");
    }

    string.append(toString(value[i]));
  }

  string.append("]");
  return string;
}

// ----------

template <>
void Bytes::Encoder::encode(const Colors& value) {
  uint16_t vsize = value.size();
  encode(vsize);

  auto index = bytes_.size();
  bytes_.resize(bytes_.size() + vsize * sizeof(Color));

  std::memcpy(&bytes_[index ], value.data(), value.size() * sizeof(Color));
}

template <>
auto Bytes::Decoder::decode() -> std::optional<Colors> {
  auto vsize = decode<uint16_t>();
  if (!vsize.has_value()) {
    return std::nullopt;
  }

  if ( vsize.    value() * sizeof(Color) + index_ > bytes_.size()) {
    return std::nullopt;
  }

  auto value = Colors(vsize.value());
  std::memcpy(value.data(), &bytes_[index_], value.size() * sizeof(Color));

  index_ += vsize.value() * sizeof(Color);
  return value;
}

template <>
auto Bytes::Encoder::length(const Colors& value) -> std::size_t {
  return sizeof(uint16_t) + value.size() * sizeof(Color);
}

}  // namespace ambiramus
}  // namespace lvd
