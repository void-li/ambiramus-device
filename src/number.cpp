/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "number.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <cstdlib>

// ----------

namespace lvd {
namespace ambiramus {

auto Number::construct(const String& numberString) -> std::optional<Number> {
  auto value = atoll(numberString.data());
  if (value == 0) {
    if (numberString.empty()) {
      return std::nullopt;
    }

    if (numberString[0] != '0') {
      return std::nullopt;
    }
  }

  return Number(value);
}

// ----------

template <>
String toString<Number>(const Number& value) {
  return toString(static_cast<uint64_t>(value));
}

}  // namespace ambiramus
}  // namespace lvd
