/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <vector>

#include "device.hpp"
#include "device_preferences.hpp"
#include "object.hpp"
#include "preferences.hpp"
#include "uuid.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class DeviceManager : public Object<DeviceManager> {
 public:
  DeviceManager(Construction,
                const Preferences::SharedPtr& preferences);

 public:
  auto search(const Uuid& uuid) -> Device::SharedPtr;
  bool update(const Uuid& uuid, const DevicePreferences& devicePreferences);

 public:
  auto devices() const -> const std::vector<Device::SharedPtr>& {
    return devices_;
  }

 private:
  Preferences::SharedPtr preferences_;

 private:
  std::vector<Device::SharedPtr> devices_;
};

}  // namespace ambiramus
}  // namespace lvd
