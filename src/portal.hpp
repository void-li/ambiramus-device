/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <chrono>
#include <cstdint>
#include <memory>
#include <optional>
#include <vector>

#include "bytes.hpp"
#include "device.hpp"
#include "device_manager.hpp"
#include "device_preferences.hpp"
#include "module.hpp"
#include "object.hpp"
#include "output.hpp"
#include "output_manager.hpp"
#include "output_preferences.hpp"
#include "portal_preferences.hpp"
#include "preferences.hpp"
#include "reboot.hpp"
#include "signal.hpp"
#include "string.hpp"
#include "system.hpp"
#include "update.hpp"
#include "uuid.hpp"
#include "wifiap.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class Portal : public Object<Portal>, public Module {
 public:
  Portal(Construction,
         const Preferences::SharedPtr& preferences,
         const PortalPreferences& portalPreferences);
  ~Portal();

 public:
  virtual void setup() override;
  virtual void loop () override;
  virtual void close() override;

 private:
  auto overview() -> String;

  void onColour(const               String & s_output,
                const               String & s_index,
                const               String & s_color);

  void onDevice(const               String & s_device,
                const std::optional<String>& s_uuid,
                const std::optional<String>& s_name);

  void onOutput(const               String & s_output,
                const std::optional<String>& s_uuid,
                const std::optional<String>& s_name,
                const std::optional<String>& s_leds);

  void onPortal(const               String & s_username,
                const               String & s_password);

  void onReboot();

  void onSystem(const               String & s_logs);

  bool onUpdateSetup();
  bool onUpdateData(const Bytes& bytes);
  bool onUpdateClose();

  void onUpdateAbort();

  void onWifiAp(const               String & s_ssid,
                const               String & s_password);

  void onRescan();

 private:
  void overviewNotify(String& string);
  void overviewWifiAp(String& string);
  void overviewUpdate(String& string);
  void overviewPortal(String& string);
  void overviewDevice(String& string);
  void overviewOutput(String& string);
  void overviewSystem(String& string);

 private:
  void reset();

 public:
  auto portalPreferences() const -> const PortalPreferences& {
    return portalPreferences_;
  }
  void setPortalPreferences(const PortalPreferences& portalPreferences) {
    portalPreferences_ = portalPreferences;
  }

 public:
  Signal<DeviceManager*()> reqDeviceManager;
  Signal<OutputManager*()> reqOutputManager;

  Signal<System       *()> reqSystem;
  Signal<Reboot       *()> reqReboot;
  Signal<WifiAp       *()> reqWifiAp;
  Signal<Update       *()> reqUpdate;

 private:
  class           Impl;
  std::unique_ptr<Impl> impl_;

 private:
  Preferences::SharedPtr preferences_;

 private:
  PortalPreferences portalPreferences_;

 private:
  String successMessage_;
  String failureMessage_;

 private:
  bool refresh_ = false;
};

}  // namespace ambiramus
}  // namespace lvd
