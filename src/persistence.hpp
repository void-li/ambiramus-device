/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <memory>

#include "bytes.hpp"
#include "object.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class Persistence : public Object<Persistence> {
 public:
  Persistence(Construction);
  ~Persistence();

 public:
  bool read (      Byte* bytes, std::size_t index, std::size_t count);
  bool write(const Byte* bytes, std::size_t index, std::size_t count);
  bool erase();

 public:
  static auto space() -> std::size_t;

 public:
  static Persistence::SharedPtr instance();
  static void                   instance_release();

 private:
  static Persistence::SharedPtr instance_;

#ifdef UNIT_TEST
  friend class TestPersistence;
  friend class TestPersistenceImpl;
#endif

 private:
  class           Impl;
  std::unique_ptr<Impl> impl_;
};

}  // namespace ambiramus
}  // namespace lvd
