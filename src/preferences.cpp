/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "preferences.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <algorithm>

#include "crcsum.hpp"
#include "enum.hpp"
#include "logger.hpp"
#include "string.hpp"

// ----------

namespace lvd {
namespace ambiramus {

//                           P A G E
const uint32_t PAGE_FLAG = 0x50414745;

//                           U N I T
const uint32_t UNIT_FLAG = 0x554e4954;

// ----------

class Preferences::Unit {
 public:
  Unit(Species       species,
       Variant       variant,
       const Bytes&  bytes,
       bool          saved = false)
      : species_(         (species)),
        variant_(         (variant)),
        bytes_  (         ( bytes )),
        saved_  (           saved  ) {}

  Unit(Species       species,
       Variant       variant,
             Bytes&& bytes,
       bool          saved = false)
      : species_(std::move(species)),
        variant_(std::move(variant)),
        bytes_  (std::move( bytes )),
        saved_  (std::move( saved )) {}

 public:
  auto species() const -> Species {
    return species_;
  }
  auto variant() const -> Variant {
    return variant_;
  }

  auto bytes() const -> const Bytes& {
    return bytes_;
  }

  auto saved() const -> bool {
    return saved_;
  }
  void setSaved(bool saved) {
    saved_ = saved;
  }

 private:
  Species species_ = Species::INVALID;
  Variant variant_ =          0x00000;

  Bytes bytes_ = Bytes();
  bool  saved_ = false;
};

// ----------

template <>
String toString<Preferences::Species>(const Preferences::Species& value) {
  switch (value) {
    case Preferences::Species::INVALID:
      return toString(STRREF("INVALID"           ));

    case Preferences::Species::DEVICE_PREFERENCES:
      return toString(STRREF("DEVICE_PREFERENCES"));

    case Preferences::Species::OUTPUT_PREFERENCES:
      return toString(STRREF("OUTPUT_PREFERENCES"));

    case Preferences::Species::PORTAL_PREFERENCES:
      return toString(STRREF("PORTAL_PREFERENCES"));

    case Preferences::Species::WIFIAP_PREFERENCES:
      return toString(STRREF("WIFIAP_PREFERENCES"));

#ifdef UNIT_TEST
    case Preferences::Species::TEST0:
      return toString(STRREF("TEST0"             ));

    case Preferences::Species::TEST1:
      return toString(STRREF("TEST1"             ));
#endif
  }

  return toString(STRREF("~UNKNOWN"));
}

// ----------

template <>
void Bytes::Encoder::encode(const Preferences::Species& value) {
  encode(enum2type<Preferences::Species>(value));
}

template <>
auto Bytes::Decoder::decode() -> std::optional<Preferences::Species> {
  auto value = decode<std::underlying_type<Preferences::Species>::type>();
  if (!value.has_value()) {
    return std::nullopt;
  }

  return type2enum<Preferences::Species>(value.value());
}

template <>
auto Bytes::Encoder::length(const Preferences::Species& value) -> std::size_t {
  return
  length(enum2type<Preferences::Species>(value));
}

// ----------

Preferences::Preferences(Construction,
                         const Persistence::SharedPtr& persistence)
    : persistence_(persistence) {
  reload();
}

Preferences::~Preferences() = default;

auto Preferences::load(Species species, Variant variant) -> Bytes {
  LOG_D("Preferences")
      << STRREF("load")
      << species
      << variant;

  auto it = std::  find_if(units_.rbegin(),
                           units_.rend  (),
                           [species, variant](const Unit& unit) {
    return unit.species() == species
        && unit.variant() == variant;
  });

  if (it != units_.rend()) {
    LOG_D("Preferences")
        << STRREF("unit found")
        << species
        << variant
        << it->bytes().size();

    return it->bytes();
  }

  LOG_D("Preferences")
      << STRREF("unit not found")
      << species
      << variant;

  return Bytes();
}

void Preferences::save(Species species, Variant variant, const Bytes& bytes) {
  LOG_D("Preferences")
      << STRREF("save")
      << species
      << variant;

  auto it = std::remove_if(units_. begin(),
                           units_. end  (),
                           [species, variant](const Unit& unit) {
    return unit.species() == species
        && unit.variant() == variant;
  });

  if (it != units_. end()) {
    LOG_D("Preferences")
        << STRREF("unit found")
        << species
        << variant;

    units_.erase(it, units_. end());
  }

  units_.emplace_back(
      species,
      variant,
       bytes
  );
}

void Preferences::commit() {
  LOG_D("Preferences")
      << STRREF("commit");

  bool success = true;

  if (!erase_) {
    for (Unit& unit : units_) {
      if (unit.saved()) {
        continue;
      }

      LOG_D("Preferences")
          << STRREF("unit write")
          << unit.species()
          << unit.variant()
          << unit.bytes().size();

      Bytes::Encoder encoder;
      encodeUnit    (encoder, unit);

      const Bytes& bytes = encoder.bytes();

      bool ok = persistence_->write(bytes.data(), index_, bytes.size());
      if (!ok) {
        LOG_D("Preferences")
            << STRREF("unit write")
            << STRREF("failure")
            << unit.species()
            << unit.variant()
            << unit.bytes().size();

        erase_ = true;
        index_ = 0x00;

        success = false;
        break;
      }

      index_ += bytes.size();
      unit.setSaved(true);
    }

    if (success) {
      LOG_D("Preferences")
          << STRREF("commit")
          << STRREF("success");

      return;
    }
  }

  for (Unit& unit : units_) {
    unit.setSaved(false);
  }

  {
    bool ok = persistence_->erase();
    if (!ok) {
      LOG_C("Preferences")
          << STRREF("page erase")
          << STRREF("failure");

      return;
    }

    erase_ = false;
    index_ = 0x000;
  }

  success = true;

  {
    LOG_D("Preferences")
        << STRREF("page write");

    Bytes::Encoder encoder;
    encodePage    (encoder);

    const Bytes& bytes = encoder.bytes();

    bool ok = persistence_->write(bytes.data(), index_, bytes.size());
    if (!ok) {
      LOG_C("Preferences")
          << STRREF("page write")
          << STRREF("failure");

      erase_ = true;
      index_ = 0x00;

      success = false;
      return;
    }

    index_ += bytes.size();
  //page.setSaved(true);
  }

  for (Unit& unit : units_) {
    LOG_D("Preferences")
        << STRREF("unit write")
        << unit.species()
        << unit.variant()
        << unit.bytes().size();

    Bytes::Encoder encoder;
    encodeUnit    (encoder, unit);

    const Bytes& bytes = encoder.bytes();

    bool ok = persistence_->write(bytes.data(), index_, bytes.size());
    if (!ok) {
      LOG_C("Preferences")
          << STRREF("unit write")
          << STRREF("failure")
          << unit.species()
          << unit.variant()
          << unit.bytes().size();

      erase_ = true;
      index_ = 0x00;

      success = false;
      break;
    }

    index_ += bytes.size();
    unit.setSaved(true);
  }

  if (success) {
    for (Unit& unit : units_) {
      unit.setSaved(true);
    }
  }

  if (success) {
    LOG_D("Preferences")
        << STRREF("commit")
        << STRREF("success");
  } else {
    LOG_D("Preferences")
        << STRREF("commit")
        << STRREF("failure");
  }
}

void Preferences::reload() {
  LOG_D("Preferences")
      << STRREF("reload");

  units_.clear();

  Bytes bytes;
  bytes.resize(persistence_->space());

  {
    bool ok = persistence_->read(bytes.data(), 0, bytes.size());
    if (!ok) {
      LOG_W("Preferences")
          << STRREF("page read")
          << STRREF("failure");

      erase_ = true;
      index_ = 0x00;

      return;
    }
  }

  Bytes::Decoder decoder(bytes);

  {
    bool ok = decodePage(decoder);
    if (!ok) {
      LOG_W("Preferences")
          << STRREF("page data")
          << STRREF("failure");

      erase_ = true;
      index_ = 0x00;

      return;
    }
  }

  std::size_t index = decoder.index();

  while (index < bytes.size()) {
    decoder.setIndex(index);

    auto unit = decodeUnit(decoder);
    if (!unit) {
      index++;
      continue;
    }

    Species species = unit->species();
    Variant variant = unit->variant();

    auto it = std::remove_if(units_. begin(),
                             units_. end  (),
                             [species, variant](const Unit& unit) {
      return unit.species() == species
          && unit.variant() == variant;
    });

    if (it != units_. end()) {
      LOG_D("Preferences")
          << STRREF("unit found")
          << species
          << variant;

      units_.erase(it, units_. end());
    }

    units_.push_back(
        std::move(unit.value())
    );

    index = decoder.index();
    continue;
  }

  index = 0;

  for (std::size_t i = 0; i < bytes.size(); i++) {
    index = bytes.size() -i -1;

    if (bytes[index] != 0xFF) {
      break;
    }
  }

  do {
    index++;
  } while (index % 4 != 0);

  LOG_D("Preferences")
      << STRREF("reload")
      << STRREF("success");

  erase_ = false;
  index_ = index;
}

void Preferences::encodePage(Bytes::Encoder& encoder) {
  encoder.bytes().reserve(
    + Bytes::Encoder::length(PAGE_FLAG     )
  );

  encoder.encode(PAGE_FLAG);
}

bool Preferences::decodePage(Bytes::Decoder& decoder) {
  auto pageFlag = decoder.decode<std::remove_const<decltype(PAGE_FLAG)>::type>();
  if (!pageFlag.has_value()) {
    return false;
  }

  if ( pageFlag.    value() != PAGE_FLAG) {
    return false;
  }

  return true;
}

void Preferences::encodeUnit(Bytes::Encoder& encoder, const Unit& unit) {
  encoder.bytes().reserve(
    + Bytes::Encoder::length(UNIT_FLAG     )
    + Bytes::Encoder::length(unit.species())
    + Bytes::Encoder::length(unit.variant())
    + Bytes::Encoder::length(unit. bytes ())
    + 4  // alignment
    + Bytes::Encoder::length(CrcSum()      )
  );

  encoder.encode(UNIT_FLAG);

  encoder.encode(unit.species());
  encoder.encode(unit.variant());
  encoder.encode(unit. bytes ());

  for (std::size_t i = encoder.bytes().size();
                   i % 4 != 0;
                   i++) {
    encoder.encode('\0');
  }

  CrcSum crcSum = crcsum(encoder.bytes());
  encoder.encode(crcSum);
}

auto Preferences::decodeUnit(Bytes::Decoder& decoder) -> std::optional<Unit> {
  std::size_t indexA = decoder.index();

  auto unitFlag = decoder.decode<std::remove_const<decltype(UNIT_FLAG)>::type>();
  if (!unitFlag.has_value()) {
    return std::nullopt;
  }

  if ( unitFlag.    value() != UNIT_FLAG) {
    return std::nullopt;
  }

  auto species = decoder.decode<Species>();
  if (!species.has_value()) {
    LOG_W("Preferences")
        << STRREF("unit data")
        << STRREF("failure")
        << STRREF("missing field")
        << STRREF("species");

    return std::nullopt;
  }

  auto variant = decoder.decode<Variant>();
  if (!variant.has_value()) {
    LOG_W("Preferences")
        << STRREF("unit data")
        << STRREF("failure")
        << STRREF("missing field")
        << STRREF("variant");

    return std::nullopt;
  }

  auto  bytes  = decoder.decode< Bytes >();
  if (! bytes .has_value()) {
    LOG_W("Preferences")
        << STRREF("unit data")
        << STRREF("failure")
        << STRREF("missing field")
        << STRREF("bytes");

    return std::nullopt;
  }

  for (std::size_t i = decoder.index();
                   i % 4 != 0;
                   i++) {
    decoder.decode<Byte>();
  }

  std::size_t indexB = decoder.index();

  auto datCrcSum = decoder.decode<CrcSum>();
  if (!datCrcSum.has_value()) {
    LOG_W("Preferences")
        << STRREF("unit data")
        << STRREF("failure")
        << STRREF("missing field")
        << STRREF("crcsum");

    return std::nullopt;
  }

  auto ourCrcSum = crcsum(decoder.bytes().data() + indexA, indexB - indexA);
  if ( ourCrcSum != datCrcSum.value()) {
    LOG_W("Preferences")
        << STRREF("unit data")
        << STRREF("failure")
        << STRREF("invalid field")
        << STRREF("crcsum")
        << STRREF("received")
        << datCrcSum.value()
        << STRREF("expected")
        << ourCrcSum;

    return std::nullopt;
  }

  return Unit(
      std::move(species.value()),
      std::move(variant.value()),
      std::move( bytes .value()),
      true
  );
}

}  // namespace ambiramus
}  // namespace lvd
