/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "output_id.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "string.hpp"

// ----------

namespace lvd {
namespace ambiramus {

template <>
String toString<OutputId>(const OutputId& value) {
  switch (value) {
    case OutputId::OUTPUT0:
      return toString(STRREF("OUTPUT0"));

    case OutputId::OUTPUT1:
      return toString(STRREF("OUTPUT1"));

#ifdef UNIT_TEST
    case OutputId::TEST0:
      return toString(STRREF("TEST0"));

    case OutputId::TEST1:
      return toString(STRREF("TEST1"));
#endif
  }

  return toString(STRREF("~UNKNOWN"));
}

}  // namespace ambiramus
}  // namespace lvd
