/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "device_id.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "string.hpp"

// ----------

namespace lvd {
namespace ambiramus {

template <>
String toString<DeviceId>(const DeviceId& value) {
  switch (value) {
    case DeviceId::DEVICE0:
      return toString(STRREF("DEVICE0"));

#ifdef UNIT_TEST
    case DeviceId::TEST0:
      return toString(STRREF("TEST0"));

    case DeviceId::TEST1:
      return toString(STRREF("TEST1"));
#endif
  }

  return toString(STRREF("~UNKNOWN"));
}

}  // namespace ambiramus
}  // namespace lvd
