/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "bytes.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <cstring>

#include "hexify.hpp"
#include "string.hpp"

// ----------

namespace lvd {
namespace ambiramus {

Bytes::Bytes(Value::size_type vsize, Value::value_type* value)
    : value_(std::make_shared<Value>(vsize)) {
  std::memcpy(value_->data(), value, value_->size());
}

Bytes::Bytes(const char* bytes)
    : value_(std::make_shared<Value>(std::strlen(bytes))) {
  std::memcpy(value_->data(), bytes, value_->size());
}

// ----------

template <>
String toString<Bytes>(const Bytes& value) {
  String string;

  for (std::size_t i = 0; i < value.size(); i++) {
    uint16_t hexed = chrToHex(value[i]);

    string.push_back(hexed >> 0x08);
    string.push_back(hexed >> 0x00);
  }

  return string;
}

// ----------

template <>
void Bytes::Encoder::encode(const Bytes& value) {
  uint16_t vsize = value.size();
  encode(vsize);

  auto index = bytes_.size();
  bytes_.resize(bytes_.size() + vsize);

  std::memcpy(&bytes_[index], value.data(), value.size());
}

template <>
auto Bytes::Decoder::decode() -> std::optional<Bytes> {
  auto vsize = decode<uint16_t>();
  if (!vsize.has_value()) {
    return std::nullopt;
  }

  if ( vsize.    value() + index_ > bytes_.size()) {
    return std::nullopt;
  }

  auto value = Bytes      (vsize.value());
  std::memcpy(value.data(), &bytes_[index_], value.size());

  index_ += vsize.value();
  return value;
}

template <>
auto Bytes::Encoder::length(const Bytes& value) -> std::size_t {
  return sizeof(uint16_t) + value.size();
}

}  // namespace ambiramus
}  // namespace lvd
