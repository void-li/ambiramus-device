/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "client_impl.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <WiFiUdp.h>

#include "config.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class Client::Impl::Data {
 public:
  WiFiUDP wiFiUDP;
};

// ----------

Client::Impl::Impl(Client* client)
    : data_(std::make_unique<Data>()),
      client_(client) {}

Client::Impl::~Impl() = default;

void Client::Impl::setup() {
  // void
}

void Client::Impl::loop() {
  // void
}

void Client::Impl::close() {
  // void
}

void Client::Impl::airmailMessage(uint32_t addr, const Bytes& bytes) {
  data_->wiFiUDP.beginPacket(addr, config::CLIENT_PORT);
  data_->wiFiUDP.write(bytes.data(), bytes.size());
  data_->wiFiUDP.endPacket();
}

}  // namespace ambiramus
}  // namespace lvd
