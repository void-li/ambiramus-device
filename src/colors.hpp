/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <memory>
#include <vector>

#include "color.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class Colors {
 public:
  using Value    =                 std::vector<Color> ;
  using ValuePtr = std::shared_ptr<std::vector<Color>>;

 public:
  Colors()
      : value_(std::make_shared<Value>(            )) {}

  Colors(Value::size_type vsize)
      : value_(std::make_shared<Value>(vsize       )) {}

 public:
  Colors(Value::size_type vsize, const Color& value)
      : value_(std::make_shared<Value>(vsize, value)) {}

 public:
  auto size() const -> Value::size_type {
    return value_->size();
  }

  bool empty() const {
    return size() == 0;
  }

  auto data() const -> const Value::value_type* {
    return value_->data();
  }

  auto data()       ->       Value::value_type* {
    detach();
    return value_->data();
  }

 public:
  auto front() const -> Value::const_reference {
    return value_->front();
  }

  auto front()       -> Value::      reference {
    detach();
    return value_->front();
  }

  auto back () const -> Value::const_reference {
    return value_->back ();
  }

  auto back ()       -> Value::      reference {
    detach();
    return value_->back ();
  }

  auto begin() const -> Value::const_iterator {
    return value_->begin();
  }

  auto begin()       -> Value::      iterator {
    detach();
    return value_->begin();
  }

  auto end  () const -> Value::const_iterator {
    return value_->end  ();
  }

  auto end  ()       -> Value::      iterator {
    detach();
    return value_->end  ();
  }

 public:
  void clear() {
    value_ = std::make_shared<Value>();
  }

  void resize (Value::size_type size) {
    detach();
    value_->resize (size);
  }

  void resize (Value::size_type size, const Value::value_type& item) {
    detach();
    value_->resize (size, item);
  }

  void reserve(Value::size_type size) {
  //detach();
    value_->reserve(size);
  }

  void push_back(const Value::value_type& item) {
    detach();
    value_->push_back(item);
  }

 public:
  auto operator[](Value::size_type index) const -> Value::const_reference {
    return (*value_)[index];
  }

  auto operator[](Value::size_type index)       -> Value::      reference {
    detach();
    return (*value_)[index];
  }

 private:
  void detach() {
    if (value_.use_count() > 1) {
      value_ = std::make_shared<Value>(*value_);
    }
  }

 private:
  friend bool operator==(const Colors& lhs, const Colors& rhs);
  friend bool operator!=(const Colors& lhs, const Colors& rhs);

 private:
  ValuePtr value_;
};

// ----------

inline bool operator==(const Colors& lhs,
                       const Colors& rhs) {
  return (*lhs.value_) == (*rhs.value_);
}

inline bool operator!=(const Colors& lhs,
                       const Colors& rhs) {
  return !(lhs == rhs);
}

}  // namespace ambiramus
}  // namespace lvd
