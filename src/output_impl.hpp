/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <memory>

#include "color.hpp"
#include "output.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class Output::Impl {
 public:
  Impl(Output* output);
  ~Impl();

 public:
  void setup();

 public:
  void colorize(const Colors& colors);

 private:
  class           Data;
  std::unique_ptr<Data> data_;

  Output* const output_;
};

}  // namespace ambiramus
}  // namespace lvd
