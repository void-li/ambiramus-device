/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "portal_impl.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <optional>

#include <ESP8266WebServer.h>
#include <detail/RequestHandler.h>
#include <detail/RequestHandlersImpl.h>

#include "config.hpp"
#include "string.hpp"

// ----------

namespace {

const char HEADER  [] PROGMEM =
#include "../portal/header.html.hppd"
;

const char FOOTER  [] PROGMEM =
#include "../portal/footer.html.hppd"
;

const char SYNC_SVG[] PROGMEM =
#include "../portal/sync.svg.hppd"
;

const char WIFI_SVG[] PROGMEM =
#include "../portal/wifi.svg.hppd"
;

}  // namespace

// ----------

namespace {
using namespace lvd::ambiramus;

class AuthorizationHandler : public esp8266webserver::RequestHandler<WiFiServer> {
 public:
  using WebServerType = esp8266webserver::ESP8266WebServerTemplate<WiFiServer>;

 public:
  AuthorizationHandler(const Portal* portal, ESP8266WebServer* eSP8266WebServer)
      : portal_(portal), eSP8266WebServer_(eSP8266WebServer) {}

 public:
  virtual bool canHandle(HTTPMethod, const ::String&) override {
    if (portal_) {
      if (   !portal_->portalPreferences().username().empty()
          && !portal_->portalPreferences().password().empty()) {
        return true;
      }
    }

    return false;
  }

 public:
  virtual bool handle(WebServerType&, HTTPMethod, const ::String&) override {
    if (portal_) {
      if (   !portal_->portalPreferences().username().empty()
          && !portal_->portalPreferences().password().empty()) {
        if (!eSP8266WebServer_->authenticate(
              portal_->portalPreferences().username().data(),
              portal_->portalPreferences().password().data())
        ) {
          eSP8266WebServer_->requestAuthentication();
          return true;
        }
      }
    }

    return false;
  }

 private:
  const Portal* const portal_;

 private:
  ESP8266WebServer* eSP8266WebServer_;
};

// ----------

auto requiredArgument(const ESP8266WebServer& eSP8266WebServer,
                      const char*             name) ->              lvd::ambiramus::String  {
  if (!eSP8266WebServer.hasArg(name)) {
    return lvd::ambiramus::String();
  }

  return lvd::ambiramus::String::construct(
      eSP8266WebServer.arg(name).c_str()
  );
}

auto optionalArgument(const ESP8266WebServer& eSP8266WebServer,
                      const char*             name) -> std::optional<lvd::ambiramus::String> {
  if (!eSP8266WebServer.hasArg(name)) {
    return std::nullopt;
  }

  return lvd::ambiramus::String::construct(
      eSP8266WebServer.arg(name).c_str()
  );
}

}  // namespace

// ----------

namespace lvd {
namespace ambiramus {

class Portal::Impl::Data {
 public:
  ESP8266WebServer eSP8266WebServer;
};

// ----------

Portal::Impl::Impl(Portal* portal)
    : data_(std::make_unique<Data>()), portal_(portal) {
  data_->eSP8266WebServer.addHandler(
      new AuthorizationHandler(portal_, &data_->eSP8266WebServer)
  );

  data_->eSP8266WebServer.on(F("/"),
                             [this] {
    String overview = portal_->overview();

    if (portal_->refresh_) {
      data_->eSP8266WebServer.sendHeader("Refresh", "1");
    }

    data_->eSP8266WebServer.send(200, "text/html", overview.data(), overview.size());
  });

  data_->eSP8266WebServer.on(F("/colour"),
                             [this] {
    portal_->onColour(
        requiredArgument(data_->eSP8266WebServer, "output"),
        requiredArgument(data_->eSP8266WebServer, "index" ),
        requiredArgument(data_->eSP8266WebServer, "color" )
    );

//  data_->eSP8266WebServer.sendHeader("Location", "/");
    data_->eSP8266WebServer.send(200);
  });

  data_->eSP8266WebServer.on(F("/device"),
                             [this] {
    portal_->onDevice(
        requiredArgument(data_->eSP8266WebServer, "device"),
        optionalArgument(data_->eSP8266WebServer,  "uuid" ),
        optionalArgument(data_->eSP8266WebServer,  "name" )
    );

    data_->eSP8266WebServer.sendHeader("Location", "/");
    data_->eSP8266WebServer.send(303);
  });

  data_->eSP8266WebServer.on(F("/output"),
                             [this] {
    portal_->onOutput(
        requiredArgument(data_->eSP8266WebServer, "output"),
        optionalArgument(data_->eSP8266WebServer,  "uuid" ),
        optionalArgument(data_->eSP8266WebServer,  "name" ),
        optionalArgument(data_->eSP8266WebServer,  "leds" )
    );

    data_->eSP8266WebServer.sendHeader("Location", "/");
    data_->eSP8266WebServer.send(303);
  });

  data_->eSP8266WebServer.on(F("/portal"),
                             [this] {
    portal_->onPortal(
        requiredArgument(data_->eSP8266WebServer, "username"),
        requiredArgument(data_->eSP8266WebServer, "password")
    );

    data_->eSP8266WebServer.sendHeader("Location", "/");
    data_->eSP8266WebServer.send(303);
  });

  data_->eSP8266WebServer.on(F("/reboot"),
                             [this] {
    portal_->onReboot();

    data_->eSP8266WebServer.sendHeader("Location", "/");
    data_->eSP8266WebServer.send(303);
  });

  data_->eSP8266WebServer.on(F("/system"),
                             [this] {
    portal_->onSystem(
        requiredArgument(data_->eSP8266WebServer, "logs")
    );

    data_->eSP8266WebServer.sendHeader("Location", "/");
    data_->eSP8266WebServer.send(303);
  });

  data_->eSP8266WebServer.on(F("/update"),
                             HTTPMethod::HTTP_POST,
                             [this] {
    HTTPUpload& upload = data_->eSP8266WebServer.upload();

    switch (upload.status) {
      case UPLOAD_FILE_END:
        portal_->onUpdateClose();
        break;

      default:
        portal_->onUpdateAbort();
        break;
    }

    data_->eSP8266WebServer.sendHeader("Location", "/");
    data_->eSP8266WebServer.send(303);
  },                         [this] {
    HTTPUpload& upload = data_->eSP8266WebServer.upload();

    switch (upload.status) {
      case UPLOAD_FILE_START:
        portal_->onUpdateSetup();
        break;

      case UPLOAD_FILE_WRITE:
        portal_->onUpdateData({ upload.currentSize, upload.buf });
        break;

      case UPLOAD_FILE_END:
      //portal_->onUpdateClose();
        break;

      case UPLOAD_FILE_ABORTED:
        portal_->onUpdateAbort();
        break;

      default:
        portal_->onUpdateAbort();
        break;
    }
  });

  data_->eSP8266WebServer.on(F("/wifiap"),
                             [this] {
    portal_->onWifiAp(
        requiredArgument(data_->eSP8266WebServer, "ssid"    ),
        requiredArgument(data_->eSP8266WebServer, "password")
    );

    data_->eSP8266WebServer.sendHeader("Location", "/");
    data_->eSP8266WebServer.send(303);
  });

  data_->eSP8266WebServer.on(F("/wifiap/rescan"),
                             [this] {
    portal_->onRescan();

    data_->eSP8266WebServer.sendHeader("Location", "/");
    data_->eSP8266WebServer.send(303);
  });

  data_->eSP8266WebServer.on(F("/sync.svg"),
                             [this] {
    data_->eSP8266WebServer.send_P(200, PSTR("image/svg+xml"), SYNC_SVG);
  });

  data_->eSP8266WebServer.on(F("/wifi.svg"),
                             [this] {
    data_->eSP8266WebServer.send_P(200, PSTR("image/svg+xml"), WIFI_SVG);
  });
}

Portal::Impl::~Impl() = default;

void Portal::Impl::setup() {
  data_->eSP8266WebServer.begin(config::PORTAL_PORT);
}

void Portal::Impl::loop() {
  data_->eSP8266WebServer.handleClient();
}

void Portal::Impl::close() {
  data_->eSP8266WebServer.close();
}

String Portal::Impl::header() const {
  String string;
  string.resize(strlen_P(HEADER));

  memcpy_P(const_cast<char*>(string.data()), HEADER, string.size());
  return string;
}

String Portal::Impl::footer() const {
  String string;
  string.resize(strlen_P(FOOTER));

  memcpy_P(const_cast<char*>(string.data()), FOOTER, string.size());
  return string;
}

}  // namespace ambiramus
}  // namespace lvd
