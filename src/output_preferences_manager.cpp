/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "output_preferences_manager.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "decoder_util.hpp"
#include "encoder_util.hpp"
#include "enum.hpp"
#include "logger.hpp"

// ----------

namespace lvd {
namespace ambiramus {

auto OutputPreferencesManager::load() -> OutputPreferences {
  LOG_D("OutputPreferencesManager")
      << STRREF("load")
      << outputId_;

  const Bytes& bytes = preferences_->load(
      Preferences::Species::OUTPUT_PREFERENCES, enum2type(outputId_)
  );

  Bytes::Decoder decoder    ( bytes );
  DecoderUtil    decoderUtil(decoder);

  OutputPreferences outputPreferences;

  for (auto subject = decoderUtil.decode<OutputPreferences::Subject>();
            subject.has_value();
            subject = decoderUtil.decode<OutputPreferences::Subject>()) {
    switch (subject.value()) {
      case OutputPreferences::Subject::UUID: {
        auto uuid = decoderUtil.data<decltype(outputPreferences.uuid_)>();
        if (uuid.has_value()) {
          outputPreferences.setUuid(uuid.value());
        }
      } break;

      case OutputPreferences::Subject::NAME: {
        auto name = decoderUtil.data<decltype(outputPreferences.name_)>();
        if (name.has_value()) {
          outputPreferences.setName(name.value());
        }
      } break;

      case OutputPreferences::Subject::LEDS: {
        auto leds = decoderUtil.data<decltype(outputPreferences.leds_)>();
        if (leds.has_value()) {
          outputPreferences.setLeds(leds.value());
        }
      } break;

      default:
        LOG_W("OutputPreferencesManager")
            << STRREF("unknown subject")
            << enum2type(subject.value());
    }
  }

  return outputPreferences;
}

void OutputPreferencesManager::save(const OutputPreferences& outputPreferences) {
  LOG_D("OutputPreferencesManager")
      << STRREF("save")
      << outputId_;

  Bytes::Encoder encoder;
  EncoderUtil    encoderUtil(encoder);

  encoder.bytes().reserve(
      + EncoderUtil::METASIZE + Bytes::Encoder::length(outputPreferences.uuid())
      + EncoderUtil::METASIZE + Bytes::Encoder::length(outputPreferences.name())
      + EncoderUtil::METASIZE + Bytes::Encoder::length(outputPreferences.leds())
  );

  encoderUtil.encode(OutputPreferences::Subject::UUID,
                     outputPreferences.uuid());

  encoderUtil.encode(OutputPreferences::Subject::NAME,
                     outputPreferences.name());

  encoderUtil.encode(OutputPreferences::Subject::LEDS,
                     outputPreferences.leds());

  preferences_->save(
      Preferences::Species::OUTPUT_PREFERENCES, enum2type(outputId_), encoder.bytes()
  );

  preferences_->commit();
}

}  // namespace ambiramus
}  // namespace lvd
