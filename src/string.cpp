/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "string.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <chrono>
#include <cstddef>
#include <cstdio>
#include <cstring>

#include <string.h>

#include "bytes.hpp"
#include "hexify.hpp"

// ----------

namespace lvd {
namespace ambiramus {

auto String::size () const -> Value::size_type {
  if (varef_ && !value_) {
#ifdef DEVICE
    auto valuePtr = reinterpret_cast<      PGM_P>(varef_);
    return strlen_P(valuePtr);
#endif
#ifdef NATIVE
    auto valuePtr = reinterpret_cast<const char*>(varef_);
    return strlen  (valuePtr);
#endif
  }

  if (value_) {
    return value_->size ();
  }

  return 0x00;
}

void String::assure() const {
  if (varef_ && !value_) {
#ifdef DEVICE
    auto valuePtr = reinterpret_cast<      PGM_P>(varef_);

    std::size_t /****/ size = strlen_P(valuePtr);
    value_ = std::make_shared<Value>(size, '\0');

    memcpy_P(const_cast<char*>(value_->data() + value_->size() - size), valuePtr, size);
#endif
#ifdef NATIVE
    auto valuePtr = reinterpret_cast<const char*>(varef_);

    std::size_t /****/ size = strlen  (valuePtr);
    value_ = std::make_shared<Value>(size, '\0');

    memcpy  (const_cast<char*>(value_->data() + value_->size() - size), valuePtr, size);
#endif
  }

  if (!value_) {
    value_ = std::make_shared<Value>();
  }
}

void String::detach() const {
  assure();

  if (value_.use_count() > 1) {
    value_ = std::make_shared<Value>(*value_);
  }
}

const std::string::size_type String::npos = std::string::npos;

// ----------

template <>
String toString<String     >(const String     & value) {
  return value;
}

template <>
String toString<String::Ref>(const String::Ref& value) {
  return&value;
}

// ----------

template <>
void Bytes::Encoder::encode(const String& value) {
  uint16_t vsize = value.size();
  encode(vsize);

  auto index = bytes_.size();
  bytes_.resize(bytes_.size() + vsize);

  std::memcpy(&bytes_[index], value.data(), value.size());
}

template <>
auto Bytes::Decoder::decode() -> std::optional<String> {
  auto vsize = decode<uint16_t>();
  if (!vsize.has_value()) {
    return std::nullopt;
  }

  if ( vsize.    value() + index_ > bytes_.size()) {
    return std::nullopt;
  }

  auto value = String(vsize.value(), '\0');
  std::memcpy((void*)value.data(), &bytes_[index_], value.size());

  index_ += vsize.value();
  return value;
}

template <>
auto Bytes::Encoder::length(const String& value) -> std::size_t {
  return sizeof(uint16_t) + value.size();
}

}  // namespace ambiramus
}  // namespace lvd
