/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "persistence.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "logger.hpp"
#include "persistence_impl.hpp"

// ----------

namespace lvd {
namespace ambiramus {

Persistence::Persistence(Construction)
    : impl_(std::make_unique<Impl>(this)) {}

Persistence::~Persistence() = default;

bool Persistence::read (      Byte* bytes, std::size_t index, std::size_t count) {
  LOG_D("Persistence")
      << STRREF("read" )
      << index
      << count;

  if (index % 4 != 0 || count % 4 != 0) {
    return false;
  }

  auto bound = index + count;
  if (bound > space()) {
    return false;
  }

  return impl_->read (bytes, index, count);
}

bool Persistence::write(const Byte* bytes, std::size_t index, std::size_t count) {
  LOG_D("Persistence")
      << STRREF("write")
      << index
      << count;

  if (index % 4 != 0 || count % 4 != 0) {
    return false;
  }

  auto bound = index + count;
  if (bound > space()) {
    return false;
  }

  return impl_->write(bytes, index, count);
}

bool Persistence::erase() {
  LOG_D("Persistence")
      << STRREF("erase");

  return impl_->erase();
}

auto Persistence::space() -> std::size_t {
  return Impl::space();
}

Persistence::SharedPtr
Persistence::instance_;

Persistence::SharedPtr
Persistence::instance() {
  if (!instance_) {
    instance_ = Persistence::constructShared();
  }

  return instance_;
}

void
Persistence::instance_release() {
  instance_.reset();
}

}  // namespace ambiramus
}  // namespace lvd
