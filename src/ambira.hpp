/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <chrono>
#include <memory>
#include <vector>

#include "access.hpp"
#include "bytes.hpp"
#include "client.hpp"
#include "device_manager.hpp"
#include "message.hpp"
#include "module.hpp"
#include "object.hpp"
#include "output_manager.hpp"
#include "portal.hpp"
#include "reboot.hpp"
#include "server.hpp"
#include "system.hpp"
#include "update.hpp"
#include "wifiap.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class Ambira : public Object<Ambira> {
 public:
  using Access = lvd::ambiramus::Access<Ambira>;

 public:
  Ambira(Construction,
         Preferences::SharedPtr preferences);
  ~Ambira();

 public:
  void setup();
  void loop();
  void close();

 private:
  template <class T>
  void initializeModule(std::shared_ptr<T>& module);

  void onLoopAlways(Module* module);
  void onLoopCyclic(Module* module);
  void onLoopIdling(Module* module);

 private:
  void handleServerMessage(uint32_t addr, const Bytes& bytes);

  void handleServerMessage(uint32_t addr, const Message::Announce        ::UniquePtr& message);
  void handleServerMessage(uint32_t addr, const Message::AnnounceResponse::UniquePtr& message);

  void handleServerMessage(uint32_t addr, const Message::Colorize        ::UniquePtr& message);
  void handleServerMessage(uint32_t addr, const Message::ColorizeResponse::UniquePtr& message);

 private:
  void clearLoopAlways();
  void clearLoopCyclic();

 private:
  Access::SharedPtr access_;

 private:
  DeviceManager::SharedPtr deviceManager_;
  OutputManager::SharedPtr outputManager_;

 private:
  System::SharedPtr system_;
  Reboot::SharedPtr reboot_;

  WifiAp::SharedPtr wifiAp_;
  Portal::SharedPtr portal_;

  Server::SharedPtr server_;
  Client::SharedPtr client_;

  Update::SharedPtr update_;

 private:
  std::vector<Module*> loopAlways_;
  std::vector<Module*> loopCyclic_;
};

}  // namespace ambiramus
}  // namespace lvd
