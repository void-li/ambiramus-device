/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <vector>

#include "object.hpp"
#include "output.hpp"
#include "output_preferences.hpp"
#include "preferences.hpp"
#include "uuid.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class OutputManager : public Object<OutputManager> {
 public:
  OutputManager(Construction,
                const Preferences::SharedPtr& preferences);

 public:
  auto search(const Uuid& uuid) -> Output::SharedPtr;
  bool update(const Uuid& uuid, const OutputPreferences& outputPreferences);

 public:
  auto outputs() const -> const std::vector<Output::SharedPtr>& {
    return outputs_;
  }

 private:
  Preferences::SharedPtr preferences_;

 private:
  std::vector<Output::SharedPtr> outputs_;
};

}  // namespace ambiramus
}  // namespace lvd
