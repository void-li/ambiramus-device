/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <memory>

#include "object.hpp"
#include "string.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class Logger : public Object<Logger> {
 public:
  enum class LogLevel {
    A, F, C, W, I, D
  };
  /**/ class LogProxy;

 public:
  enum       Modifier {
    DOQUOTE,
    NOQUOTE,
    DOSPACE,
    NOSPACE,
    OUTPUTS,
  };

 public:
  Logger(Construction);

 public:
  static LogProxy logA(const String& naming, int lineno);
  static LogProxy logF(const String& naming, int lineno);
  static LogProxy logC(const String& naming, int lineno);
  static LogProxy logW(const String& naming, int lineno);
  static LogProxy logI(const String& naming, int lineno);
  static LogProxy logD(const String& naming, int lineno);

 public:
  static auto logLevel() -> LogLevel {
    return logLevel_;
  }
  static void setLogLevel(LogLevel logLevel) {
    logLevel_ = logLevel;
  }

 public:
  static Logger::SharedPtr instance();
 private:
  static Logger::SharedPtr instance_;

 private:
  class           Impl;
  std::unique_ptr<Impl> impl_;

 private:
  static LogLevel logLevel_;
};

// ----------

class Logger::LogProxy {
 public:
  LogProxy(LogLevel      logLevel,
           LogLevel      ourLevel,
           const String&  naming ,
           int            lineno )
      : logLevel_(logLevel),
        ourLevel_(ourLevel),
        naming_  ( naming ),
        lineno_  ( lineno ) {}

  ~LogProxy();

 public:
  template <class T>
  LogProxy& operator<<(const T& value) {
    if (ourLevel_ <= logLevel_ || outputs_) {
      if (doquote_ || !noquote_) {
        string_.push_back('\'');
      }

      string_.append(toString(value));

      if (doquote_ || !noquote_) {
        string_.push_back('\'');
      }

      if (dospace_ || !nospace_) {
        string_.push_back( ' ');
      }
    }

    reset();
    return *this;
  }

  template <std::size_t N>
  LogProxy& operator<<(const char (&array)[N]) {
    const char* value = array;
    return (*this) << value;
  }

  operator String() const {
    return string_;
  }

 private:
  void reset();

 private:
  const LogLevel logLevel_;
  const LogLevel ourLevel_;

  const String   naming_;
  const int      lineno_;

 private:
  String string_;

 private:
  bool doquote_ = false;
  bool noquote_ = false;

  bool dospace_ = false;
  bool nospace_ = false;

  bool outputs_ = false;
};

// ----------

template <>
inline
Logger::LogProxy& Logger::LogProxy::operator<<(const char*        const& value) {
  noquote_ = true;
  return *this << toString<const char*>(value);
}

template <>
inline
Logger::LogProxy& Logger::LogProxy::operator<<(const String::Ref* const& value) {
  noquote_ = true;
  return *this << toString             (value);
}

template <>
inline
Logger::LogProxy& Logger::LogProxy::operator<<(const Modifier& modifier) {
  switch (modifier) {
    case DOQUOTE:
      doquote_ = true;
      break;

    case NOQUOTE:
      noquote_ = true;
      break;

    case DOSPACE:
      dospace_ = true;
      break;

    case NOSPACE:
      nospace_ = true;
      break;

    case OUTPUTS:
      outputs_ = true;
      break;
  }

  return *this;
}

// ----------

inline Logger::LogProxy Logger::logA(const String& naming, int lineno) {
  return LogProxy(Logger::logLevel(), LogLevel::A, naming, lineno);
}
#define LOG_A(N) ::lvd::ambiramus::Logger::logA(STRREF(N), __LINE__)

inline Logger::LogProxy Logger::logF(const String& naming, int lineno) {
  return LogProxy(Logger::logLevel(), LogLevel::F, naming, lineno);
}
#define LOG_F(N) ::lvd::ambiramus::Logger::logF(STRREF(N), __LINE__)

inline Logger::LogProxy Logger::logC(const String& naming, int lineno) {
  return LogProxy(Logger::logLevel(), LogLevel::C, naming, lineno);
}
#define LOG_C(N) ::lvd::ambiramus::Logger::logC(STRREF(N), __LINE__)

inline Logger::LogProxy Logger::logW(const String& naming, int lineno) {
  return LogProxy(Logger::logLevel(), LogLevel::W, naming, lineno);
}
#define LOG_W(N) ::lvd::ambiramus::Logger::logW(STRREF(N), __LINE__)

inline Logger::LogProxy Logger::logI(const String& naming, int lineno) {
  return LogProxy(Logger::logLevel(), LogLevel::I, naming, lineno);
}
#define LOG_I(N) ::lvd::ambiramus::Logger::logI(STRREF(N), __LINE__)

inline Logger::LogProxy Logger::logD(const String& naming, int lineno) {
  return LogProxy(Logger::logLevel(), LogLevel::D, naming, lineno);
}
#define LOG_D(N) ::lvd::ambiramus::Logger::logD(STRREF(N), __LINE__)

}  // namespace ambiramus
}  // namespace lvd
