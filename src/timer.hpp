/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <chrono>
#include <optional>

// ----------

namespace lvd {
namespace ambiramus {

class Timer {
 public:
  using Instant = std::chrono::system_clock::time_point;
  using Timeout = std::chrono::system_clock:: duration ;

 public:
  void acquire();
  void release();

 public:
  auto instant() const -> std::optional<Instant> {
    return instant_;
  }
  void setInstant(const Instant& instant) {
    instant_ = instant;
  }

  auto timeout() const -> std::optional<Timeout> {
    return timeout_;
  }
  void setTimeout(const Timeout& timeout) {
    timeout_ = timeout;
  }

 private:
  std::optional<Instant> instant_;
  std::optional<Timeout> timeout_;
};

}  // namespace ambiramus
}  // namespace lvd
