/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <memory>

#include "wifiap.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class WifiAp::Impl {
 public:
  Impl(WifiAp* wifiAp);
  ~Impl();

 public:
  void setup();
  void loop();
  void close();

 public:
  void scan();

 public:
  void clientSetup();
  void clientClose();

  bool clientAlive() const;
  bool clientScans() const;

  void accessSetup();
  void accessClose();

  bool accessAlive() const;
  auto accessCount() const -> std::size_t;

  auto networkSize() const -> std::size_t;

  auto networkSsid(std::size_t index) const -> String;

  auto ipAddress() const -> String;

 private:
  class           Data;
  std::unique_ptr<Data> data_;

  WifiAp* const wifiAp_;
};

}  // namespace ambiramus
}  // namespace lvd
