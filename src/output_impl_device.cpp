/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "output_impl.hpp"
#include "core.hpp"  // IWYU pragma: keep

#define FASTLED_ESP8266_NODEMCU_PIN_ORDER
#define FASTLED_INTERNAL
#include <FastLED.h>

#include "enum.hpp"
#include "logger.hpp"

// ----------

namespace {
using namespace lvd::ambiramus;

CLEDController* cLEDController(OutputId outputId) {
  switch (outputId) {
    case OutputId::OUTPUT0: { // NodeMCU D1
      static CLEDController& cLEDController = FastLED.addLeds<WS2812B, 1, GRB>(nullptr, 0);
      return &cLEDController;
    } break;

    case OutputId::OUTPUT1: { // NodeMCU D2
      static CLEDController& cLEDController = FastLED.addLeds<WS2812B, 2, GRB>(nullptr, 0);
      return &cLEDController;
    } break;
  }

  for (std::size_t i = 0; i < 42; i++) {
    LOG_F("Output")
        << STRREF("invalid output")
        << enum2type(outputId);
  }

  std::abort();
}

}  // namespace

// ----------

namespace lvd {
namespace ambiramus {

class Output::Impl::Data {
 public:
  CLEDController* cLEDController = nullptr;
};

// ----------

Output::Impl::Impl(Output* output)
    : data_(std::make_unique<Data>()),
      output_(output) {}

Output::Impl::~Impl() = default;

void Output::Impl::setup() {
  data_->cLEDController = cLEDController(output_->outputId_);
}

void Output::Impl::colorize(const Colors& colors) {
  data_->cLEDController->show(
      reinterpret_cast<const CRGB*>(colors.data()), colors.size(), 255
  );
}

}  // namespace ambiramus
}  // namespace lvd
