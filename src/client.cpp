/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "client.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "client_impl.hpp"
#include "config.hpp"
#include "logger.hpp"
#include "message.hpp"

// ----------

namespace lvd {
namespace ambiramus {

Client::Client(Construction)
    : Module(STRREF("Client")),
      impl_(std::make_unique<Impl>(this)) {}

Client::~Client() = default;

void Client::setup() {
  LOG_D("Client")
      << STRREF("setup");

  impl_->setup();

  activity(STRREF("Client"));
  loopCyclic(STRREF("Client"), config::CLIENT_PERIOD);
}

void Client::loop() {
//LOG_D("Client")
//    << STRREF("loop");

  Message::Announce::UniquePtr message = Message::Announce::constructUnique();
  message->setName(Name::construct(String::construct(config::AMBIRA_NAME)).value());
  message->setVers(Vers::construct(String::construct(config::AMBIRA_VERS)).value());

  auto deviceManager = reqDeviceManager();
  if (!deviceManager) {
    LOG_C("Client")
        << STRREF("device manager unavailable");

    return;
  }

  for (const Device::SharedPtr& device : deviceManager->devices()) {
    if (device) {
      message->setDevicePreferencesItem(
          device->devicePreferences()
      );
    }
  }

  auto outputManager = reqOutputManager();
  if (!outputManager) {
    LOG_C("Client")
        << STRREF("output manager unavailable");

    return;
  }

  for (const Output::SharedPtr& output : outputManager->outputs()) {
    if (output) {
      message->addOutputPreferencesItem(
          output->outputPreferences()
      );
    }
  }

  impl_->airmailMessage(
      ~0x0, message->encode()
  );
}

void Client::close() {
  LOG_D("Client")
      << STRREF("close");

  impl_->close();

  dormant (STRREF("Client"));
  loopIdling(STRREF("Client"));
}

void Client::airmailMessage(uint32_t addr, const Bytes& bytes) {
  impl_->airmailMessage(
      addr, bytes
  );
}

}  // namespace ambiramus
}  // namespace lvd
