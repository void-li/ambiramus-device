/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <memory>

#include "system.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class System::Impl {
 public:
  Impl(System* client);
  ~Impl();

 public:
  void setup();
  void loop ();
  void close();

 public:
  auto bootCause() -> String;
  auto heapFragm() -> uint8_t;

 public:
  void reboot();

 private:
  class           Data;
  std::unique_ptr<Data> data_;

  System* const system_;
};

}  // namespace ambiramus
}  // namespace lvd
