/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <cstdint>
#include <optional>

#include "byte.hpp"

// ----------

namespace lvd {
namespace ambiramus {

enum class HexifyCase {
  UPPER_CASE,
  LOWER_CASE
};

auto chrToHex(Byte     value, HexifyCase hexifyCase = HexifyCase::UPPER_CASE) -> uint16_t;

auto hexToChr(uint16_t value) -> std::optional<Byte>;
auto hexToChr(char h, char l) -> std::optional<Byte>;

}  // namespace ambiramus
}  // namespace lvd
