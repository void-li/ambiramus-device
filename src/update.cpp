/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "update.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "config.hpp"
#include "logger.hpp"
#include "update_impl.hpp"

// ----------

namespace lvd {
namespace ambiramus {

Update::Update(Construction)
    : Module(STRREF("Update")),
      impl_(std::make_unique<Impl>(this)) {}

Update::~Update() = default;

void Update::setup() {
  LOG_D("Update")
      << STRREF("setup");

  impl_->setup();

  dormant (STRREF("Update"));
  loopIdling(STRREF("Update"));

  bool ok = impl_->updateAbort();
  if (!ok) {
    updateFailure();
  }
}

void Update::loop() {
  LOG_D("Update")
      << STRREF("loop");

  impl_->loop();

  if (activityFlag()) {
    auto now = std::chrono::system_clock::now();
    if (now > activityTime() + config::UPDATE_ACTIVE) {
      dormant (STRREF("Update"));
      loopIdling(STRREF("Update"));

      bool ok = impl_->updateAbort();
      if (!ok) {
        updateFailure();
      }
    }
  }
}

void Update::close() {
  LOG_D("Update")
      << STRREF("close");

  impl_->close();

  dormant (STRREF("Update"));
  loopIdling(STRREF("Update"));
}

bool Update::updateSetup() {
  LOG_D("Update")
      << STRREF("updateSetup");

  activity(STRREF("Update"));
  loopCyclic(STRREF("Update"), config::UPDATE_ACTIVE);

  bool ok = impl_->updateSetup();
  if (!ok) {
    updateFailure();
  }

  return ok;
}

bool Update::updateData(const Bytes& bytes) {
  LOG_D("Update")
      << STRREF("updateData")
      << bytes.size();

  activity(STRREF("Update"));
  loopCyclic(STRREF("Update"), config::UPDATE_ACTIVE);

  bool ok = impl_->updateData(bytes);
  if (!ok) {
    updateFailure();
  }

  return ok;
}

bool Update::updateClose() {
  LOG_D("Update")
      << STRREF("updateClose");

  activity(STRREF("Update"));
  loopCyclic(STRREF("Update"), config::UPDATE_ACTIVE);

  bool ok = impl_->updateClose();
  if (!ok) {
    updateFailure();
  } else {
    updateSuccess();
  }

  return ok;
}

bool Update::updateAbort() {
  LOG_W("Update")
      << STRREF("updateAbort");

  activity(STRREF("Update"));
  loopCyclic(STRREF("Update"), config::UPDATE_ACTIVE);

  bool ok = impl_->updateClose();
  if (!ok) {
    updateFailure();
  }

  return ok;
}

void Update::updateSuccess() {
  LOG_I("Update")
      << STRREF("updateSuccess");

  sigSuccess();

  auto successMessage = impl_->successMessage();
  if (!successMessage.empty()) {
    LOG_I("Update")
        << STRREF("updateSuccess")
        << successMessage;
  }
}

void Update::updateFailure() {
  LOG_W("Update")
      << STRREF("updateFailure");

  sigFailure();

  auto failureMessage = impl_->failureMessage();
  if (!failureMessage.empty()) {
    LOG_W("Update")
        << STRREF("updateFailure")
        << failureMessage;
  }
}

}  // namespace ambiramus
}  // namespace lvd
