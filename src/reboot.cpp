/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "reboot.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "logger.hpp"

// ----------

namespace lvd {
namespace ambiramus {

Reboot::Reboot(Construction, System::SharedPtr system)
    : Module(STRREF("Reboot")),
      system_(system) {}

Reboot::~Reboot() = default;

void Reboot::setup() {
  LOG_D("Reboot")
      << STRREF("setup");

  dormant (STRREF("Reboot"));
  loopIdling(STRREF("Reboot"));
}

void Reboot::loop() {
  LOG_D("Reboot")
      << STRREF("loop");

  system_->reboot();
}

void Reboot::close() {
  LOG_D("Reboot")
      << STRREF("close");

  dormant (STRREF("Reboot"));
  loopIdling(STRREF("Reboot"));
}

void Reboot::reboot() {
  LOG_D("Reboot")
      << STRREF("reboot");

  activity(STRREF("Reboot"));
  loopAlways(STRREF("Reboot"));
}

}  // namespace ambiramus
}  // namespace lvd
