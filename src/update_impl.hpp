/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <memory>

#include "bytes.hpp"
#include "string.hpp"
#include "update.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class Update::Impl {
 public:
  Impl(Update* update);
  ~Impl();

 public:
  void setup();
  void loop ();
  void close();

 public:
  bool updateSetup();
  bool updateData(const Bytes& bytes);
  bool updateClose();

  bool updateAbort();

 public:
  auto successMessage() -> String;
  auto failureMessage() -> String;

 private:
  class           Data;
  std::unique_ptr<Data> data_;

  Update* const update_;
};

}  // namespace ambiramus
}  // namespace lvd
