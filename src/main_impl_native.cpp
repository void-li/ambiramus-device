/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "main.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <chrono>

#include <signal.h>

#include <QCoreApplication>
#include <QTimer>

// ----------

bool quitFlag = false;
void quit(int) {
  quitFlag = true;
}

int main(int argc, char* argv[]) {
  QCoreApplication qCoreApplication(argc, argv);

  QTimer qtimers; qtimers.setSingleShot(true);
  QTimer::connect(&qtimers, &QTimer::timeout, [] { setup(); });
  qtimers.start(std::chrono::milliseconds(  0));

  QTimer qtimerl;
  QTimer::connect(&qtimerl, &QTimer::timeout, [] { loop (); });
  qtimerl.start(std::chrono::milliseconds(100));

  signal(SIGINT , &quit);
  signal(SIGTERM, &quit);

  QTimer::connect(&qtimerl, &QTimer::timeout, [] {
    if (quitFlag) {
      QCoreApplication::quit();
    }
  });

  QCoreApplication::connect(
      &qCoreApplication,
      &QCoreApplication::aboutToQuit,
      [] { close(); }
  );

  return qCoreApplication.exec();
}
