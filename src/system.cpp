/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "system.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "config.hpp"
#include "logger.hpp"
#include "system_impl.hpp"

// ----------

namespace lvd {
namespace ambiramus {

System::System(Construction)
    : Module(STRREF("System")),
      impl_(std::make_unique<Impl>(this)) {}

System::~System() = default;

void System::setup() {
  LOG_D("System")
      << STRREF("setup");

  impl_->setup();

  activity(STRREF("System"));
  loopCyclic(STRREF("System"), config::SYSTEM_PERIOD);
}

void System::loop() {
//LOG_D("System")
//    << STRREF("loop");

  impl_->loop();
}

void System::close() {
  LOG_D("System")
      << STRREF("close");

  impl_->close();

  dormant (STRREF("System"));
  loopIdling(STRREF("System"));
}

auto System::bootCause() -> String {
  return impl_->bootCause();
}

auto System::heapFragm() -> uint8_t {
  return impl_->heapFragm();
}

void System::reboot() {
  LOG_I("System")
      << STRREF("reboot");

  impl_->reboot();
}

}  // namespace ambiramus
}  // namespace lvd
