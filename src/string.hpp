/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <cstddef>
#include <memory>
#include <optional>
#include <string>

// ----------

namespace lvd {
namespace ambiramus {

class String {
 public:
  using Value    =                 std::string ;
  using ValuePtr = std::shared_ptr<std::string>;

 public:
  class Ref;

 public:
  String()
      : value_(std::make_shared<Value>(     )) {}

  String(Value::size_type vsize)
      : value_(std::make_shared<Value>(vsize, ' '  )) {}

 public:
  String(Value::size_type vsize, char value)
      : value_(std::make_shared<Value>(vsize, value)) {}

  String(const String::Ref* varef)
      : varef_(                        varef ) {}

 private:
  String          (const char       * value)
      : value_(std::make_shared<Value>(       value)) {}

  String          (const std::string& value)
      : value_(std::make_shared<Value>(       value)) {}

 public:
  static
  String construct(const char       * value) {
    return value;
  }

 public:
  auto size() const -> Value::size_type;

  bool empty() const {
    return size() == 0;
  }

  auto data() const -> const Value::value_type* {
    assure();
    return value_->data();
  }

//auto data()       ->       Value::value_type* {
//  detach();
//  return value_->data();
//}

 public:
  auto front() const -> Value::const_reference {
    assure();
    return value_->front();
  }

  auto front()       -> Value::      reference {
    detach();
    return value_->front();
  }

  auto back () const -> Value::const_reference {
    assure();
    return value_->back ();
  }

  auto back ()       -> Value::      reference {
    detach();
    return value_->back ();
  }

  auto begin() const -> Value::const_iterator {
    assure();
    return value_->begin();
  }

  auto begin()       -> Value::      iterator {
    detach();
    return value_->begin();
  }

  auto end  () const -> Value::const_iterator {
    assure();
    return value_->end  ();
  }

  auto end  ()       -> Value::      iterator {
    detach();
    return value_->end  ();
  }

 public:
  void clear() {
    value_ = std::make_shared<Value>();
    varef_ =                 nullptr  ;
  }

  void resize (Value::size_type size) {
    detach();
    value_->resize (size, ' ' );
  }

  void resize (Value::size_type size, Value::value_type item) {
    detach();
    value_->resize (size, item);
  }

  void reserve(Value::size_type size) {
    assure();
    value_->reserve(size     );
  }

  void push_back(Value::value_type item) {
    detach();
    value_->push_back(item);
  }

  void append(const String& string) {
    detach(); string.assure();
    value_->append(*string.value_);
  }

  void append(const char  * string) {
    detach();
    value_->append( string       );
  }

  void insert (Value:: iterator   index,
               Value:: size_type  count,
               Value::value_type  value) {
    detach();
    value_->insert (index, count, value);
  }

  void replace(Value:: size_type  index,
               Value:: size_type  count,
               Value::value_type* value) {
    detach();
    value_->replace(index, count, value);
  }

 public:
  auto  find(const char* string, Value::size_type index = +0) const -> Value::size_type {
    assure();
    return value_-> find(string, index);
  }

  auto rfind(const char* string, Value::size_type index = -1) const -> Value::size_type {
    assure();
    return value_->rfind(string, index);
  }

 public:
  auto operator[](Value::size_type index) const -> Value::const_reference {
    assure();
    return (*value_)[index];
  }

  auto operator[](Value::size_type index)       -> Value::      reference {
    detach();
    return (*value_)[index];
  }

  auto operator+ (const String& value) -> String  {
    assure(); value.assure();
    return String((*value_) + *value.value_);
  }

  auto operator+ (const char*   value) -> String  {
    assure();
    return String((*value_) +  value);
  }

  auto operator+=(const String& value) -> String& {
    detach(); value.assure();
    (*value_) += (*value.value_);
    return *this;
  }

  auto operator+=(const char*   value) -> String& {
    detach();
    (*value_) += ( value       );
    return *this;
  }

 public:
  const std::string& toStdString() const {
    assure();
    return *value_;
  }

 private:
  void assure() const;
  void detach() const;

 private:
  friend bool operator==(const String& lhs, const String& rhs);
  friend bool operator!=(const String& lhs, const String& rhs);

 public:
  static const std::string::size_type npos;

 private:
  mutable ValuePtr   value_ = {     };
  /*****/ const Ref* varef_ = nullptr;
};

// ----------

template <class T>
String toString(const T& value);

template <class T>
String toString(const T* value) {
  return (value != nullptr) ? toString(*value) : toString(nullptr);
}

template <class T>
String toString(      T* value) {
  return (value != nullptr) ? toString(*value) : toString(nullptr);
}

template <std::size_t N>
String toString(const char (&value)[N]) {
  return String        (value);
}

template <std::size_t N>
String toString(      char (&value)[N]) {
  return String        (value);
}

template <class T>
String toString(const std::shared_ptr<T>& value) {
  return (value) ? toString(*value) : toString(nullptr);
}

template <class T>
String toString(const std::unique_ptr<T>& value) {
  return (value) ? toString(*value) : toString(nullptr);
}

template <class T>
String toString(const std::optional   <T>& value) {
  return (value) ? toString(*value) : toString(nullptr);
}

template <>
inline
String toString(const char* const &value) {
  return String::construct(value);
}

// ----------

inline bool operator==(const String& lhs,
                       const String& rhs) {
  lhs.assure();
  rhs.assure();

  return (*lhs.value_) == (*rhs.value_);
}

inline bool operator!=(const String& lhs,
                       const String& rhs) {
  return !(lhs == rhs);
}

}  // namespace ambiramus
}  // namespace lvd

#ifdef DEVICE
#define STRREF(S) (const lvd::ambiramus::String::Ref*)(PSTR(S))
#endif

#ifdef NATIVE
#define STRREF(S) (const lvd::ambiramus::String::Ref*)(    (S))
#endif
