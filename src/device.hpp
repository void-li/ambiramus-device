/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <memory>

#include "device_id.hpp"
#include "device_preferences.hpp"
#include "name.hpp"
#include "object.hpp"
#include "uuid.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class Device : public Object<Device> {
 public:
  Device(Construction,
         const DeviceId           deviceId,
         const DevicePreferences& devicePreferences);
  ~Device();

 public:
  auto deviceId() const -> DeviceId {
    return deviceId_;
  }

  auto devicePreferences() const -> const DevicePreferences& {
    return devicePreferences_;
  }
  void setDevicePreferences(const DevicePreferences& devicePreferences) {
    devicePreferences_ = devicePreferences;
  }

  auto uuid() const -> const Uuid& {
    return devicePreferences_.uuid();
  }
  auto name() const -> const Name& {
    return devicePreferences_.name();
  }

 private:
  class           Impl;
  std::unique_ptr<Impl> impl_;

 private:
  const DeviceId          deviceId_;
  /***/ DevicePreferences devicePreferences_;
};

}  // namespace ambiramus
}  // namespace lvd
