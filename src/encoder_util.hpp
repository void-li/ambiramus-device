/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <cstdlib>

#include "bytes.hpp"
#include "enum.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class EncoderUtil {
 public:
  EncoderUtil(Bytes::Encoder& encoder)
      : encoder_(encoder) {}

 public:
  template <class T, class Subject>
  void encode    (Subject  subject, const T& value) {
    return encodeImpl(enum2type(subject), value);
  }

 private:
  template <class T               >
  void encodeImpl(uint16_t subject, const T& value);

 public:
  static const std::size_t METASIZE = sizeof(uint16_t) + sizeof(uint16_t);

 private:
  Bytes::Encoder& encoder_;
};

// ----------

template <class T>
inline void EncoderUtil::encodeImpl(uint16_t subject, const T& value) {
  encoder_.encode<uint16_t>(subject);
  uint16_t footage = 0x0000;
  encoder_.encode<uint16_t>(footage);

  std::size_t indexA = encoder_.index();
  encoder_.encode(value);
  std::size_t indexB = encoder_.index();

  std::size_t length = indexB - indexA;

  encoder_.bytes()[indexA - 2] = length >> 0x08;
  encoder_.bytes()[indexA - 1] = length &  0xFF;
}

}  // namespace ambiramus
}  // namespace lvd
