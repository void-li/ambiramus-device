/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <optional>

#include "string.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class Name {
 public:
  static auto construct(const String& nameString) -> std::optional<Name>;

 public:
  bool valid() const {
    return valid_;
  }

 private:
  template <class T>
  friend String toString(const T& value);

 private:
  String name_;
  bool valid_ = false;
};

// ----------

template <>
inline String toString<Name>(const Name& value) {
  return value.name_;
}

// ----------

inline bool operator==(const Name& lhs,
                       const Name& rhs) {
  return toString(lhs) == toString(rhs);
}

inline bool operator!=(const Name& lhs,
                       const Name& rhs) {
  return !(lhs == rhs);
}

}  // namespace ambiramus
}  // namespace lvd
