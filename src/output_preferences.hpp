/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <cstdint>

#include "name.hpp"
#include "uuid.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class OutputPreferences {
  friend class OutputPreferencesManager;

 public:
  enum class Subject : uint16_t {
    UUID = 0x0000,
    NAME = 0x0010,
    LEDS = 0x0020,
  };

 public:
  auto uuid() const -> const Uuid& {
    return uuid_;
  }
  void setUuid(const Uuid& uuid) {
    uuid_ = uuid;
  }

  auto name() const -> const Name& {
    return name_;
  }
  void setName(const Name& name) {
    name_ = name;
  }

  auto leds() const -> uint16_t {
    return leds_;
  }
  void setLeds(uint16_t leds) {
    leds_ = leds;
  }

 private:
  Uuid uuid_;
  Name name_;

 private:
  uint16_t leds_ = 0;
};

// ----------

inline bool operator==(const OutputPreferences& lhs,
                       const OutputPreferences& rhs) {
  if (lhs.uuid() != rhs.uuid()) {
    return false;
  }

  if (lhs.name() != rhs.name()) {
    return false;
  }

  if (lhs.leds() != rhs.leds()) {
    return false;
  }

  return true;
}

inline bool operator!=(const OutputPreferences& lhs,
                       const OutputPreferences& rhs) {
  return !(lhs == rhs);
}

}  // namespace ambiramus
}  // namespace lvd
