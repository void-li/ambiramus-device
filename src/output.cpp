/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "output.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "logger.hpp"
#include "output_impl.hpp"

// ----------

namespace lvd {
namespace ambiramus {

Output::Output(Construction,
               const OutputId           outputId,
               const OutputPreferences& outputPreferences)
    : impl_(std::make_unique<Impl>(this)),
      outputId_         (outputId         ),
      outputPreferences_(outputPreferences) {
  impl_->setup();
}

Output::~Output() = default;

void Output::colorize() {
  impl_->colorize(colors_);
}

void Output::colorize(const Colors& colors) {
  colors_ = colors;
  impl_->colorize(colors_);
}

void Output::colorize(const Color & color ) {
  for (std::size_t index = 0; index < colors_.size(); index++) {
    colors_[index] = color;
  }

  if (colors_.size() < outputPreferences_.leds()) {
    colors_.resize(outputPreferences_.leds(), color);
  }

  colorize();
}

void Output::colorize(const Color & color , std::size_t index) {
  if  (                       index < colors_.size()         ) {
    colors_[index] = color;
  }

  if (colors_.size() < outputPreferences_.leds()) {
    colors_.resize(outputPreferences_.leds(), color);
  }

  colorize();
}

}  // namespace ambiramus
}  // namespace lvd
