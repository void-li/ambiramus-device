/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "portal.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <string.h>

#include "config.hpp"
#include "logger.hpp"
#include "number.hpp"
#include "portal_impl.hpp"
#include "portal_preferences_manager.hpp"
#include "string.hpp"
#include "wifiap_preferences_manager.hpp"

// ----------

namespace {
using namespace lvd::ambiramus;

String escape(String value) {
  auto pos = String::npos;

  for (pos = value.rfind("\"", pos);
       pos != String::npos;
       pos = value.rfind("\"", pos)) {
    value.replace(pos, 1, (char*) "&quot;");
  }

  return value;
}

template <class T>
String escape(const T& value) {
  return escape(toString(value));
}

}  // namespace

// ----------

namespace lvd {
namespace ambiramus {

Portal::Portal(Construction,
               const Preferences::SharedPtr& preferences,
               const PortalPreferences& portalPreferences)
    : Module(STRREF("Portal")),
      impl_(std::make_unique<Impl>(this)),
      preferences_(preferences),
      portalPreferences_(portalPreferences) {}

Portal::~Portal() = default;

void Portal::setup() {
  LOG_D("Portal")
      << STRREF("setup");

  reset();
  impl_->setup();

  activity(STRREF("Portal"));
  loopCyclic(STRREF("Portal"), config::PORTAL_PERIOD);
}

void Portal::loop() {
//LOG_D("Portal")
//    << STRREF("loop");

  impl_->loop();

  if (activityFlag()) {
    auto now = std::chrono::system_clock::now();
    if (now > activityTime() + config::PORTAL_ACTIVE) {
      dormant (STRREF("Portal"));
      loopCyclic(STRREF("Portal"), config::PORTAL_PERIOD);
    }
  }
}

void Portal::close() {
  LOG_D("Portal")
      << STRREF("setup");

  reset();
  impl_->close();

  dormant (STRREF("Portal"));
  loopIdling(STRREF("Portal"));

  reset();
}

auto Portal::overview() -> String {
  LOG_D("Portal")
      << STRREF("overview");

  activity(STRREF("Portal"));
  loopAlways(STRREF("Portal"));

  String string;
  refresh_ = false;

  {
    String header = impl_->header();
    string.append(header);
  }

  overviewNotify(string);
  overviewWifiAp(string);
  overviewUpdate(string);
  overviewPortal(string);
  overviewDevice(string);
  overviewOutput(string);
  overviewSystem(string);

  {
    String footer = impl_->footer();
    string.append(footer);
  }

  reset();
  return string;
}

void Portal::onColour(const String& s_output,
                      const String& s_index,
                      const String& s_color) {
  LOG_D("Portal")
      << STRREF("onColour")
      << s_output
      << s_index
      << s_color;

  activity(STRREF("Portal"));
  loopAlways(STRREF("Portal"));

  auto outputUuid = Uuid::construct(s_output);
  if (!outputUuid.has_value()) {
    failureMessage_ =
    LOG_W("Portal")
        << Logger::OUTPUTS
        << STRREF("invalid output uuid")
        << s_output;

    return;
  }

  auto outputManager = reqOutputManager();
  if (!outputManager) {
    failureMessage_ =
    LOG_C("Portal")
        << Logger::OUTPUTS
        << STRREF("output manager unavailable");

    return;
  }

  auto output = outputManager->search(outputUuid.value());
  if (!output) {
    failureMessage_ =
    LOG_W("Portal")
        << Logger::OUTPUTS
        << STRREF("output unavailable")
        << outputUuid.value();

    return;
  }

  auto index = Number::construct(s_index);
//if (!index.has_value()) {
//  failureMessage_ =
//  LOG_W("Portal")
//      << Logger::OUTPUTS
//      << STRREF("invalid index")
//      << s_index;

//  return;
//}

  auto color = Color ::construct(s_color);
  if (!color.has_value()) {
    failureMessage_ =
    LOG_W("Portal")
        << Logger::OUTPUTS
        << STRREF("invalid color")
        << s_color;

    return;
  }

  if (!index.has_value()) {
    output->colorize(
        color.value()
    );
  } else {
    output->colorize(
        color.value(), index.value()
    );
  }
}

void Portal::onDevice(const               String & s_device,
                      const std::optional<String>& s_uuid,
                      const std::optional<String>& s_name) {
  LOG_D("Portal")
      << STRREF("onDevice")
      << s_device
      << s_uuid
      << s_name;

  activity(STRREF("Portal"));
  loopAlways(STRREF("Portal"));

  auto deviceUuid = Uuid::construct(s_device);
  if (!deviceUuid.has_value()) {
    failureMessage_ =
    LOG_W("Portal")
        << Logger::OUTPUTS
        << STRREF("invalid device uuid")
        << s_device;

    return;
  }

  auto deviceManager = reqDeviceManager();
  if (!deviceManager) {
    failureMessage_ =
    LOG_C("Portal")
        << Logger::OUTPUTS
        << STRREF("device manager unavailable");

    return;
  }

  auto device = deviceManager->search(deviceUuid.value());
  if (!device) {
    failureMessage_ =
    LOG_W("Portal")
        << Logger::OUTPUTS
        << STRREF("device unavailable")
        << deviceUuid.value();

    return;
  }

  auto devicePreferences = device->devicePreferences();

  if (s_uuid.has_value()) {
    auto uuid = Uuid::construct(s_uuid.value());
    if (!uuid.has_value()) {
      failureMessage_ =
      LOG_W("Portal")
          << Logger::OUTPUTS
          << STRREF("invalid uuid")
          << s_uuid.value();

      return;
    }
    devicePreferences.setUuid(uuid.value());
  }

  if (s_name.has_value()) {
    auto name = Name::construct(s_name.value());
    if (!name.has_value()) {
      failureMessage_ =
      LOG_W("Portal")
          << Logger::OUTPUTS
          << STRREF("invalid name")
          << s_name.value();

      return;
    }
    devicePreferences.setName(name.value());
  }

  if (devicePreferences != device->devicePreferences()) {
    bool ok = deviceManager->update(deviceUuid.value(), devicePreferences);
    if (!ok) {
      failureMessage_ =
      LOG_W("Portal")
          << Logger::OUTPUTS
          << STRREF("unknown error");

      return;
    }

    successMessage_ =
    LOG_I("Portal")
        << Logger::OUTPUTS
        << STRREF("updated device");
  }
}

void Portal::onOutput(const               String & s_output,
                      const std::optional<String>& s_uuid,
                      const std::optional<String>& s_name,
                      const std::optional<String>& s_leds) {
  LOG_D("Portal")
      << STRREF("onOutput")
      << s_output
      << s_uuid
      << s_name
      << s_leds;

  activity(STRREF("Portal"));
  loopAlways(STRREF("Portal"));

  auto outputUuid = Uuid::construct(s_output);
  if (!outputUuid.has_value()) {
    failureMessage_ =
    LOG_W("Portal")
        << Logger::OUTPUTS
        << STRREF("invalid output uuid")
        << s_output;

    return;
  }

  auto outputManager = reqOutputManager();
  if (!outputManager) {
    failureMessage_ =
    LOG_C("Portal")
        << Logger::OUTPUTS
        << STRREF("output manager unavailable");

    return;
  }

  auto output = outputManager->search(outputUuid.value());
  if (!output) {
    failureMessage_ =
    LOG_W("Portal")
        << Logger::OUTPUTS
        << STRREF("output unavailable")
        << outputUuid.value();

    return;
  }

  auto outputPreferences = output->outputPreferences();

  if (s_uuid.has_value()) {
    auto uuid = Uuid::construct(s_uuid.value());
    if (!uuid.has_value()) {
      failureMessage_ =
      LOG_W("Portal")
          << Logger::OUTPUTS
          << STRREF("invalid uuid")
          << s_uuid.value();

      return;
    }
    outputPreferences.setUuid(uuid.value());
  }

  if (s_name.has_value()) {
    auto name = Name::construct(s_name.value());
    if (!name.has_value()) {
      failureMessage_ =
      LOG_W("Portal")
          << Logger::OUTPUTS
          << STRREF("invalid name")
          << s_name.value();

      return;
    }
    outputPreferences.setName(name.value());
  }

  if (s_leds.has_value()) {
    auto leds = static_cast<uint8_t>(
        std::strtol(s_leds.value().data(), nullptr, 10)
    );

    outputPreferences.setLeds(leds);
  }

  if (outputPreferences != output->outputPreferences()) {
    bool ok = outputManager->update(outputUuid.value(), outputPreferences);
    if (!ok) {
      failureMessage_ =
      LOG_W("Portal")
          << Logger::OUTPUTS
          << STRREF("unknown error");

      return;
    }

    successMessage_ =
    LOG_I("Portal")
        << Logger::OUTPUTS
        << STRREF("updated output")
        << output->outputId();
  }
}

void Portal::onPortal(const String& s_username,
                      const String& s_password) {
  LOG_D("Portal")
      << STRREF("onPortal")
      << s_username
      << s_password;

  activity(STRREF("Portal"));
  loopAlways(STRREF("Portal"));

  auto portalPreferences = portalPreferences_;

  const String& username = s_username;
  portalPreferences.setUsername(username);

  const String& password = s_password;
  portalPreferences.setPassword(password);

  if (portalPreferences != portalPreferences_) {
    setPortalPreferences(portalPreferences);

    PortalPreferencesManager portalPreferencesManager(preferences_);
    portalPreferencesManager.save(portalPreferences);

    successMessage_ =
    LOG_I("Portal")
        << Logger::OUTPUTS
        << STRREF("updated portal");
  }
}

void Portal::onReboot() {
  LOG_D("Portal")
      << STRREF("onReboot");

  auto reboot = reqReboot();
  if (!reboot) {
    failureMessage_ =
    LOG_C("Portal")
        << Logger::OUTPUTS
        << STRREF("reboot unavailable");

    return;
  }

  reboot->reboot();
}

void Portal::onSystem(const String& s_logs) {
  LOG_D("Portal")
      << STRREF("onSystem")
      << s_logs;

  activity(STRREF("Portal"));
  loopAlways(STRREF("Portal"));

  if (s_logs.empty()) {
    failureMessage_ =
    LOG_W("Portal")
        << Logger::OUTPUTS
        << STRREF("missing logs");
  }

  switch (s_logs.front()) {
    case 'A':
      Logger::setLogLevel(Logger::LogLevel::A);
      break;

    case 'F':
      Logger::setLogLevel(Logger::LogLevel::F);
      break;

    case 'C':
      Logger::setLogLevel(Logger::LogLevel::C);
      break;

    case 'W':
      Logger::setLogLevel(Logger::LogLevel::W);
      break;

    case 'I':
      Logger::setLogLevel(Logger::LogLevel::I);
      break;

    case 'D':
      Logger::setLogLevel(Logger::LogLevel::D);
      break;

    default:
      LOG_W("Portal")
          << STRREF("invalid logs")
          << s_logs;
  }

  successMessage_ =
  LOG_I("Portal")
      << Logger::OUTPUTS
      << STRREF("updated system");
}

bool Portal::onUpdateSetup() {
  LOG_D("Portal")
      << STRREF("onUpdateSetup");

  activity(STRREF("Portal"));
  loopAlways(STRREF("Portal"));

  auto update = reqUpdate();
  if (!update) {
    failureMessage_ =
    LOG_C("Portal")
        << Logger::OUTPUTS
        << STRREF("update unavailable");

    return false;
  }

  bool ok = update->updateSetup();
  if (!ok) {
    failureMessage_ =
    LOG_W("Portal")
        << Logger::OUTPUTS
        << STRREF("unknown error");
  }

  return ok;
}

bool Portal::onUpdateData(const Bytes& bytes) {
  LOG_D("Portal")
      << STRREF("onUpdateData")
      << bytes.size();

  activity(STRREF("Portal"));
  loopAlways(STRREF("Portal"));

  auto update = reqUpdate();
  if (!update) {
    failureMessage_ =
    LOG_C("Portal")
        << Logger::OUTPUTS
        << STRREF("update unavailable");

    return false;
  }

  bool ok = update->updateData(bytes);
  if (!ok) {
    failureMessage_ =
    LOG_W("Portal")
        << Logger::OUTPUTS
        << STRREF("unknown error");
  }

  return ok;
}

bool Portal::onUpdateClose() {
  LOG_D("Portal")
      << STRREF("onUpdateClose");

  activity(STRREF("Portal"));
  loopAlways(STRREF("Portal"));

  auto update = reqUpdate();
  if (!update) {
    failureMessage_ =
    LOG_C("Portal")
        << Logger::OUTPUTS
        << STRREF("update unavailable");

    return false;
  }

  bool ok = update->updateClose();
  if (!ok) {
    failureMessage_ =
    LOG_W("Portal")
        << Logger::OUTPUTS
        << STRREF("unknown error");
  }

  return ok;
}

void Portal::onUpdateAbort() {
  LOG_D("Portal")
      << STRREF("onUpdateAbort");

  activity(STRREF("Portal"));
  loopAlways(STRREF("Portal"));

  auto update = reqUpdate();
  if (!update) {
    failureMessage_ =
    LOG_C("Portal")
        << Logger::OUTPUTS
        << STRREF("update unavailable");

    return;
  }

  bool ok = update->updateAbort();
  if (!ok) {
    failureMessage_ =
    LOG_W("Portal")
        << Logger::OUTPUTS
        << STRREF("unknown error");
  }
}

void Portal::onWifiAp(const String& s_ssid,
                      const String& s_password) {
  LOG_D("Portal")
      << STRREF("onWifiAp")
      << s_ssid
      << s_password;

  activity(STRREF("Portal"));
  loopAlways(STRREF("Portal"));

  auto wifiAp = reqWifiAp();
  if (!wifiAp) {
    failureMessage_ =
    LOG_C("Portal")
        << Logger::OUTPUTS
        << STRREF("wifiAp unavailable");

    return;
  }

  auto wifiApPreferences = wifiAp->wifiApPreferences();

  const String& ssid     = s_ssid    ;
  wifiApPreferences.setClientSsid    (ssid    );

  const String& password = s_password;
  wifiApPreferences.setClientPassword(password);

  if (wifiApPreferences != wifiAp->wifiApPreferences()) {
    wifiAp->setWifiApPreferences(wifiApPreferences);

    WifiApPreferencesManager wifiApPreferencesManager(preferences_);
    wifiApPreferencesManager.save(wifiApPreferences);

    successMessage_ =
    LOG_I("Portal")
        << Logger::OUTPUTS
        << STRREF("updated wifiAp");
  }
}

void Portal::onRescan() {
  LOG_D("Portal")
      << STRREF("onRescan");

  activity(STRREF("Portal"));
  loopAlways(STRREF("Portal"));

  auto wifiAp = reqWifiAp();
  if (!wifiAp) {
    failureMessage_ =
    LOG_C("Portal")
        << Logger::OUTPUTS
        << STRREF("wifiAp unavailable");

    return;
  }

  wifiAp->scan();
}

void Portal::overviewNotify(String& string) {
  if (!successMessage_.empty()) {
    string.append(STRREF("<p id=\"s\">"));
    string.append(       successMessage_);
    string.append(STRREF("</p>"));
  }

  if (!failureMessage_.empty()) {
    string.append(STRREF("<p id=\"f\">"));
    string.append(       failureMessage_);
    string.append(STRREF("</p>"));
  }
}

void Portal::overviewWifiAp(String& string) {
  string.append(STRREF("<div id=\"w\">"));
  string.append(STRREF(  "<div>"));
  string.append(STRREF(    "<h3>WiFi</h3>"));
  string.append(STRREF(    "<a href=\"/wifiap/rescan\">"));
  string.append(STRREF(      "<img src=\"/sync.svg\" />"));
  string.append(STRREF(    "</a>"));
  string.append(STRREF(  "</div>"));

  auto wifiAp = reqWifiAp();
  if (!wifiAp) {
    failureMessage_ =
    LOG_C("Portal")
        << Logger::OUTPUTS
        << STRREF("wifiAp unavailable");

    return;
  }

  if (wifiAp->scanning()) {
    string.append(STRREF("<div class=\"c\">Scanning networks.</div>"));
    refresh_ = true;
  }
  else {
    if (wifiAp->networks().empty()) {
      string.append(STRREF("<div class=\"c\">No networks found.</div>"));
    }
    else {
      string.append(STRREF(  "<table>"));

      for (auto& network : wifiAp->networks()) {
        string.append(STRREF(    "<tr>"));
        string.append(STRREF(      "<td></td>"));
        string.append(STRREF(      "<td>"));
        string.append(STRREF(        "<a onclick=\"w(this)\">"));
        string.append(STRREF(          "<img src=\"/wifi.svg\" />"));
        string.append(STRREF(          "<div>"));
        string.append(                 network.ssid());
        string.append(STRREF(          "</div>"));
        string.append(STRREF(        "</a>"));
        string.append(STRREF(      "</td>"));
        string.append(STRREF(    "</tr>"));
      }

      string.append(STRREF(  "</table>"));
    }
  }

  string.append(STRREF(  "<form action=\"/wifiap\" method=\"POST\">"));
  string.append(STRREF(    "<table>"));
  string.append(STRREF(      "<tr>"));
  string.append(STRREF(        "<td>SSID:</td>"));
  string.append(STRREF(        "<td>"));
  string.append(STRREF(          "<input type=\"text\""  " name=\"ssid\""  " id=\"ws\" placeholder=\"SSID\""  " value=\""));
  string.append(                   escape(wifiAp->wifiApPreferences().clientSsid    ()));
  string.append(STRREF(              "\" />"));
  string.append(STRREF(        "</td>"));
  string.append(STRREF(      "</tr>"));
  string.append(STRREF(      "<tr>"));
  string.append(STRREF(        "<td>Pass:</td>"));
  string.append(STRREF(        "<td>"));
  string.append(STRREF(          "<input type=\"password\" name=\"password\" id=\"wp\" placeholder=\"Password\" value=\""));
  string.append(                   escape(wifiAp->wifiApPreferences().clientPassword()));
  string.append(STRREF(              "\" />"));
  string.append(STRREF(        "</td>"));
  string.append(STRREF(      "</tr>"));
  string.append(STRREF(      "<tr>"));
  string.append(STRREF(        "<td></td>"));
  string.append(STRREF(        "<td>"));
  string.append(STRREF(          "<input type=\"submit\" value=\"Update\" />"));
  string.append(STRREF(        "</td>"));
  string.append(STRREF(      "</tr>"));
  string.append(STRREF(    "</table>"));
  string.append(STRREF(  "</form>"));
  string.append(STRREF("</div>"));
}

void Portal::overviewUpdate(String& string) {
  string.append(STRREF("<div>"));
  string.append(STRREF(  "<h3>OTA Update</h3>"));
  string.append(STRREF(  "<form action=\"/update\" method=\"POST\" enctype=\"multipart/form-data\">"));
  string.append(STRREF(    "<table>"));
  string.append(STRREF(      "<tr>"));
  string.append(STRREF(        "<td>File:</td>"));
  string.append(STRREF(        "<td>"));
  string.append(STRREF(          "<input type=\"file\" name=\"file\" />"));
  string.append(STRREF(        "</td>"));
  string.append(STRREF(      "</tr>"));
  string.append(STRREF(      "<tr>"));
  string.append(STRREF(        "<td></td>"));
  string.append(STRREF(        "<td>"));
  string.append(STRREF(          "<input type=\"submit\" value=\"Update\" />"));
  string.append(STRREF(        "</td>"));
  string.append(STRREF(      "</tr>"));
  string.append(STRREF(    "</table>"));
  string.append(STRREF(  "</form>"));
  string.append(STRREF("</div>"));
}

void Portal::overviewPortal(String& string) {
  string.append(STRREF("<div>"));
  string.append(STRREF(  "<h3>Portal</h3>"));
  string.append(STRREF(  "<form action=\"/portal\" method=\"POST\">"));
  string.append(STRREF(    "<table>"));
  string.append(STRREF(      "<tr>"));
  string.append(STRREF(        "<td>User:</td>"));
  string.append(STRREF(        "<td>"));
  string.append(STRREF(          "<input type=\"text\""  " name=\"username\" placeholder=\"Username\" value=\""));
  string.append(                   escape(portalPreferences_.username()));
  string.append(STRREF(              "\" />"));
  string.append(STRREF(        "</td>"));
  string.append(STRREF(      "</tr>"));
  string.append(STRREF(      "<tr>"));
  string.append(STRREF(        "<td>Pass:</td>"));
  string.append(STRREF(        "<td>"));
  string.append(STRREF(          "<input type=\"password\" name=\"password\" placeholder=\"Password\" value=\""));
  string.append(                   escape(portalPreferences_.password()));
  string.append(STRREF(              "\" />"));
  string.append(STRREF(        "</td>"));
  string.append(STRREF(      "</tr>"));
  string.append(STRREF(      "<tr>"));
  string.append(STRREF(        "<td></td>"));
  string.append(STRREF(        "<td>"));
  string.append(STRREF(          "<input type=\"submit\" value=\"Update\" />"));
  string.append(STRREF(        "</td>"));
  string.append(STRREF(      "</tr>"));
  string.append(STRREF(    "</table>"));
  string.append(STRREF(  "</form>"));
  string.append(STRREF("</div>"));
}

void Portal::overviewDevice(String& string) {
  auto deviceManager = reqDeviceManager();
  if (!deviceManager) {
    failureMessage_ =
    LOG_C("Portal")
        << Logger::OUTPUTS
        << STRREF("device manager unavailable");

    return;
  }

  for (auto& device : deviceManager->devices()) {
    string.append(STRREF("<div>"));
    string.append(STRREF(  "<h3>Device</h3>"));
    string.append(STRREF(  "<form action=\"/device\" method=\"POST\">"));
    string.append(STRREF(    "<input type=\"hidden\" name=\"device\" value=\""));
    String uuid = escape(device->uuid());
    string.append(             uuid);
    string.append(STRREF(        "\" />"));
    string.append(STRREF(    "<table>"));
    string.append(STRREF(      "<tr>"));
    string.append(STRREF(        "<td>Uuid:</td>"));
    string.append(STRREF(        "<td>"));
    string.append(STRREF(          "<input type=\"text\" name=\"uuid\" value=\""));
    string.append(                   uuid);
    string.append(STRREF(              "\" placeholder=\"Uuid\" disabled />"));
    string.append(STRREF(        "</td>"));
    string.append(STRREF(      "</tr>"));
    string.append(STRREF(      "<tr>"));
    string.append(STRREF(        "<td>Name:</td>"));
    string.append(STRREF(        "<td>"));
    string.append(STRREF(          "<input type=\"text\" name=\"name\" value=\""));
    String name = escape(device->name());
    string.append(                   name);
    string.append(STRREF(              "\" placeholder=\"Name\" />"));
    string.append(STRREF(        "</td>"));
    string.append(STRREF(      "</tr>"));
    string.append(STRREF(      "<tr>"));
    string.append(STRREF(        "<td></td>"));
    string.append(STRREF(        "<td>"));
    string.append(STRREF(          "<input type=\"submit\" value=\"Update\" />"));
    string.append(STRREF(        "</td>"));
    string.append(STRREF(      "</tr>"));
    string.append(STRREF(    "</table>"));
    string.append(STRREF(  "</form>"));
    string.append(STRREF("</div>"));
  }
}

void Portal::overviewOutput(String& string) {
  auto outputManager = reqOutputManager();
  if (!outputManager) {
    failureMessage_ =
    LOG_C("Portal")
        << Logger::OUTPUTS
        << STRREF("output manager unavailable");

    return;
  }

  for (auto& output : outputManager->outputs()) {
    string.append(STRREF("<div>"));
    string.append(STRREF(  "<h3>"));
    string.append(STRREF(    "Output" " "));
    string.append(             toString(static_cast<std::underlying_type<OutputId>::type>(output->outputId()) + 1));
    string.append(STRREF(  "</h3>"));
    string.append(STRREF(  "<form action=\"/output\" method=\"POST\">"));
    string.append(STRREF(    "<input type=\"hidden\" name=\"output\" value=\""));
    String uuid = escape(output->outputPreferences().uuid());
    string.append(             uuid);
    string.append(STRREF(        "\" />"));
    string.append(STRREF(    "<table>"));
    string.append(STRREF(      "<tr>"));
    string.append(STRREF(        "<td>Uuid:</td>"));
    string.append(STRREF(        "<td>"));
    string.append(STRREF(          "<input type=\"text\""" name=\"uuid\" value=\""));
    string.append(                   uuid);
    string.append(STRREF(              "\" placeholder=\"Uuid\" disabled />"));
    string.append(STRREF(        "</td>"));
    string.append(STRREF(      "</tr>"));
    string.append(STRREF(      "<tr>"));
    string.append(STRREF(        "<td>Name:</td>"));
    string.append(STRREF(        "<td>"));
    string.append(STRREF(          "<input type=\"text\""" name=\"name\" value=\""));
    String name = escape(output->outputPreferences().name());
    string.append(                   name);
    string.append(STRREF(              "\" placeholder=\"Name\" />"));
    string.append(STRREF(        "</td>"));
    string.append(STRREF(      "</tr>"));
    string.append(STRREF(      "<tr>"));
    string.append(STRREF(        "<td>LEDs:</td>"));
    string.append(STRREF(        "<td>"));
    string.append(STRREF(          "<input type=\"number\" name=\"leds\" value=\""));
    String leds = escape(output->outputPreferences().leds());
    string.append(                   leds);
    string.append(STRREF(              "\" placeholder=\"LEDs\" min=\"0\" max=\"256\" />"));
    string.append(STRREF(        "</td>"));
    string.append(STRREF(      "</tr>"));
    string.append(STRREF(      "<tr>"));
    string.append(STRREF(        "<td></td>"));
    string.append(STRREF(        "<td>"));
    string.append(STRREF(          "<input type=\"submit\" value=\"Update\" />"));
    string.append(STRREF(        "</td>"));
    string.append(STRREF(      "</tr>"));
    string.append(STRREF(      "<tr>"));
    string.append(STRREF(        "<td colspan=\"2\"><hr /></td>"));
    string.append(STRREF(      "</tr>"));
    string.append(STRREF(      "<tr>"));
    string.append(STRREF(        "<td>"));
    string.append(STRREF(          "<input type=\"color\" onchange=\"c('"));
    string.append(                   uuid);
    string.append(STRREF(              "',this,"));
    string.append(STRREF(              "'a'"));
    string.append(STRREF(              ")\" /> "));
    string.append(STRREF(        "</td>"));
    string.append(STRREF(        "<td>"));

    const Colors& colors = output->colors();
    for (std::size_t i = 0; i < output->leds(); i++) {
      string.append(STRREF(          "<input type=\"color\" onchange=\"c('"));
      string.append(                   uuid);
      string.append(STRREF(              "',this,"));
      string.append(                   toString(i));
      string.append(STRREF(              ")\""));

      if (i < colors.size()) {
        string.append(STRREF(              " value=\""));
        String color = escape(colors[i].toHex());
        string.append(                     color);
        string.append(STRREF(              "\""));
      }

      string.append(STRREF(              "/> "));
    }

    string.append(STRREF(        "</td>"));
    string.append(STRREF(      "</tr>"));
    string.append(STRREF(    "</table>"));
    string.append(STRREF(  "</form>"));
    string.append(STRREF("</div>"));
  }
}

void Portal::overviewSystem(String& string) {
  auto system = reqSystem();
  if (!system) {
    failureMessage_ =
    LOG_C("Portal")
        << Logger::OUTPUTS
        << STRREF("system unavailable");

    return;
  }

  {
    string.append(STRREF("<div>"));
    string.append(STRREF(  "<h3>System</h3>"));
    string.append(STRREF(  "<form action=\"/reboot\" method=\"POST\" id=\"e\"></form>"));
    string.append(STRREF(  "<form action=\"/system\" method=\"POST\" id=\"l\">"));
    string.append(STRREF(    "<table>"));
    string.append(STRREF(      "<tr>"));
    string.append(STRREF(        "<td>Boot:</td>"));
    string.append(STRREF(        "<td>"));
    string.append(STRREF(          "<input type=\"submit\" value=\"Reboot\" onclick=\"document.getElementById('e').submit(); return false\" style=\"width: inherit\" /> "));
    String bootCause = system->bootCause();
    string.append(                 bootCause);
    string.append(STRREF(        "</td>"));
    string.append(STRREF(      "</tr>"));
    string.append(STRREF(      "<tr>"));
    string.append(STRREF(        "<td>Logs:</td>"));
    string.append(STRREF(        "<td>"));
    string.append(STRREF(          "<select name=\"logs\" onchange=\"document.getElementById('l').submit(); return false\">"));
    string.append(STRREF(            "<option value=\"F\""));
    if (Logger::logLevel() == Logger::LogLevel::F) {
      string.append(STRREF(                "selected"));
    }
    string.append(STRREF(                ">Fatal</option>"   ));
    string.append(STRREF(            "<option value=\"C\""));
    if (Logger::logLevel() == Logger::LogLevel::C) {
      string.append(STRREF(                "selected"));
    }
    string.append(STRREF(                ">Critical</option>"));
    string.append(STRREF(            "<option value=\"W\""));
    if (Logger::logLevel() == Logger::LogLevel::W) {
      string.append(STRREF(                "selected"));
    }
    string.append(STRREF(                ">Warning</option>" ));
    string.append(STRREF(            "<option value=\"I\""));
    if (Logger::logLevel() == Logger::LogLevel::I) {
      string.append(STRREF(                "selected"));
    }
    string.append(STRREF(                ">Info</option>"    ));
    string.append(STRREF(            "<option value=\"D\""));
    if (Logger::logLevel() == Logger::LogLevel::D) {
      string.append(STRREF(                "selected"));
    }
    string.append(STRREF(                ">Debug</option>"   ));
    string.append(STRREF(          "</select>"));
    string.append(STRREF(        "</td>"));
    string.append(STRREF(      "</tr>"));
    string.append(STRREF(      "<tr>"));
    string.append(STRREF(        "<td>Frag:</td>"));
    string.append(STRREF(        "<td class=\"f\">"));
    string.append(STRREF(          "<progress value=\""));
    String heapFragm = toString(system->heapFragm());
    string.append(                   heapFragm);
    string.append(STRREF(              "\" max=\"100\">"));
    string.append(                 heapFragm);
    string.append(STRREF(          "%</progress>"));
    string.append(STRREF(          "<div>"));
    string.append(                 heapFragm);
    string.append(STRREF(          "%</div>"));
    string.append(STRREF(        "</td>"));
    string.append(STRREF(      "</tr>"));
    string.append(STRREF(    "</table>"));
    string.append(STRREF(  "</form>"));
    string.append(STRREF("</div>"));
  }
}

void Portal::reset() {
  successMessage_.clear();
  failureMessage_.clear();
}

}  // namespace ambiramus
}  // namespace lvd
