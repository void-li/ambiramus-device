/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "persistence_impl.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <cstring>

#include <QByteArray>
#include <QFile>

// ----------

namespace {

const char* FILE_PATH = "config.dat";

}  // namespace

// ----------

namespace lvd {
namespace ambiramus {

class Persistence::Impl::Data {
 public:
  QByteArray qByteArray;
};

// ----------

Persistence::Impl::Impl(Persistence* persistence)
    : data_(std::make_unique<Data>()),
      persistence_(persistence) {
  erase();

  QFile qfile(FILE_PATH);
  qfile.open(QFile:: ReadOnly);

  if (qfile.isOpen()) {
    data_->qByteArray = qfile.readAll();
  }
}

Persistence::Impl::~Impl() {
  QFile qfile(FILE_PATH);
  qfile.open(QFile::WriteOnly);

  if (qfile.isOpen()) {
    qfile.write(data_->qByteArray);
    qfile.flush();
  }
}

bool Persistence::Impl::read (      Byte* bytes, std::size_t index, std::size_t count) {
  auto bound = index + count;
  if (bound > static_cast<std::size_t>(data_->qByteArray.size())) {
    return false;
  }

  std::memcpy(bytes, data_->qByteArray.data() + index, count);
  return true;
}

bool Persistence::Impl::write(const Byte* bytes, std::size_t index, std::size_t count) {
  auto bound = index + count;
  if (bound > static_cast<std::size_t>(data_->qByteArray.size())) {
    return false;
  }

  for (std::size_t i = 0; i < count; i++) {
    if ((bytes[i]) & ~data_->qByteArray[(int)(i + index)]) {
      throw std::runtime_error("FATAL");
    }
  }

  std::memcpy(data_->qByteArray.data() + index, bytes, count);
  return true;
}

bool Persistence::Impl::erase() {
  data_->qByteArray.resize(space());
  data_->qByteArray.fill(0xFF);

  return true;
}

auto Persistence::Impl::space() -> std::size_t {
  return 4096;
}

}  // namespace ambiramus
}  // namespace lvd
