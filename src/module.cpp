/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "module.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "logger.hpp"

// ----------

namespace lvd {
namespace ambiramus {

Module::Module(const String& moduleString)
    : moduleString_(moduleString) {}

void Module::loopAlways() {
  sigLoopAlways(this);
}

void Module::loopAlways(const String& module) {
  loopAlways();

  if (state_ != State::ALWAYS) {
    state_ = State::ALWAYS;

    LOG_D("Module")
        << Logger::NOQUOTE
        << module
        << STRREF("loopAlways");
  }
}

void Module::loopCyclic(                      Timer::Timeout timeout) {
  timer_.acquire();
  timer_.setTimeout(timeout);
  timer_.release();

  sigLoopCyclic(this);
}

void Module::loopCyclic(const String& module, Timer::Timeout timeout) {
  loopCyclic(timeout);

  if (state_ != State::CYCLIC) {
    state_ = State::CYCLIC;

    LOG_D("Module")
        << Logger::NOQUOTE
        << module
        << STRREF("loopCyclic");
  }
}

void Module::loopIdling() {
  sigLoopIdling(this);
}

void Module::loopIdling(const String& module) {
  loopIdling();

  if (state_ != State::IDLING) {
    state_ = State::IDLING;

    LOG_D("Module")
        << Logger::NOQUOTE
        << module
        << STRREF("loopIdling");
  }
}

void Module::activity() {
  activityTime_ = std::chrono::system_clock::now();
  activityFlag_ = true;
}

void Module::activity(const String& module) {
  activity();

  LOG_D("Module")
      << Logger::NOQUOTE
      << module
      << STRREF("activity");
}

void Module::dormant() {
  activityFlag_ = false;
}

void Module::dormant(const String& module) {
  dormant ();

  LOG_D("Module")
      << Logger::NOQUOTE
      << module
      << STRREF("dormant");
}

// ----------

template <>
String toString(const Module& module) {
  String string = STRREF("Module");
  string.append("{");

  string.append(module.moduleString());

  string.append("}");
  return string;
}

}  // namespace ambiramus
}  // namespace lvd
