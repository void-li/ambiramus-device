/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "update_impl.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <QFile>

// ----------

namespace {

const char* FILE_PATH = "update.dat";

}  // namespace

// ----------

namespace lvd {
namespace ambiramus {

class Update::Impl::Data {

};

// ----------

Update::Impl::Impl(Update* update)
    : data_(std::make_unique<Data>()),
      update_(update) {}

Update::Impl::~Impl() = default;

void Update::Impl::setup() {
  // void
}

void Update::Impl::loop() {
  // void
}

void Update::Impl::close() {
  // void
}

bool Update::Impl::updateSetup() {
  updateAbort();

  QFile qfile(FILE_PATH);
  qfile.open(QFile::Append);

  return qfile.isOpen();
}

bool Update::Impl::updateData(const Bytes& bytes) {
  QFile qfile(FILE_PATH);
  qfile.open(QFile::Append);

  qfile.write(
      reinterpret_cast<const char*>(
          bytes.data()
      ), bytes.size()
  );

  return qfile.isOpen();
}

bool Update::Impl::updateClose() {
  QFile qfile(FILE_PATH);
  qfile.open(QFile::Append);

  return qfile.isOpen();
}

bool Update::Impl::updateAbort() {
  QFile::remove(FILE_PATH);

  return true;
}

auto Update::Impl::successMessage() -> String {
  return String();
}

auto Update::Impl::failureMessage() -> String {
  return String();
}

}  // namespace ambiramus
}  // namespace lvd
