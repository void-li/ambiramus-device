/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "string.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <chrono>
#include <cstdio>
#include <string>

#include "bytes.hpp"
#include "hexify.hpp"

// ----------

namespace {
using namespace lvd::ambiramus;

template <class T>
String numberToString(T value, const char* format) {
  int size;

  size = snprintf(nullptr,             0, format, value);
  if (size < 0) {
    return String();
  }

  String string(size + 1, '\0');
  auto dataptr = (char*)string.data();

  size = snprintf(dataptr, string.size(), format, value);
  if (size < 0) {
    return String();
  }

  string.resize(string.size() - 1);

  return string;
}

}  // namespace

// ----------

namespace lvd {
namespace ambiramus {

template <>
String toString<nullptr_t>(const nullptr_t&) {
  return STRREF("nullptr");
}

// ----------

template <>
String toString<uint8_t >(const uint8_t & value) {
  return numberToString(value, "%hhu");
}
template <>
String toString< int8_t >(const  int8_t & value) {
  return numberToString(value, "%hhd");
}

template <>
String toString<uint16_t>(const uint16_t& value) {
  return numberToString(value,  "%hu");
}
template <>
String toString< int16_t>(const  int16_t& value) {
  return numberToString(value,  "%hd");
}

template <>
String toString<uint32_t>(const uint32_t& value) {
  return numberToString(value,   "%u");
}
template <>
String toString< int32_t>(const  int32_t& value) {
  return numberToString(value,   "%d");
}

template <>
String toString<uint64_t>(const uint64_t& value) {
  return numberToString(value,  "%lu");
}
template <>
String toString< int64_t>(const  int64_t& value) {
  return numberToString(value,  "%ld");
}

template <>
String toString<float   >(const float   & value) {
  return numberToString(value,   "%f");
}

template <>
String toString<double  >(const double  & value) {
  return numberToString(value,  "%lf");
}

// ----------

template <>
String toString<bool>(const bool& value) {
  return value ? STRREF("true") : STRREF("false");
}

template <>
String toString<char>(const char& value) {
  return String(1, value);
}

// ----------

#ifdef DEVICE
template <class... Ts>
void snp(char* data, size_t size, const String::Ref* fstr, Ts... ts) {
  snprintf_P(data, size, reinterpret_cast<      PGM_P>(fstr), std::forward<Ts>(ts)...);
}
#endif

#ifdef NATIVE
template <class... Ts>
void snp(char* data, size_t size, const String::Ref* fstr, Ts... ts) {
  snprintf  (data, size, reinterpret_cast<const char*>(fstr), std::forward<Ts>(ts)...);
}
#endif

template <>
String toString<std::chrono::system_clock::time_point>(
    const std::chrono::system_clock::time_point& value
) {
  char buffer[] = "yyyy-mm-dd-hh-mm-ss.yyy";

  auto ttime = std::chrono::system_clock::to_time_t(value);
  auto gtime = std::gmtime(&ttime);

  auto msecs = std::chrono::duration_cast<
      std::chrono::milliseconds
  >(value.time_since_epoch()).count() % 1000;

  snp(buffer, sizeof(buffer), STRREF("%04d-%02d-%02d-%02d.%02d.%02d.%03lld"),
      gtime->tm_year + 1900,
      gtime->tm_mon  +    1,
      gtime->tm_mday       ,
      gtime->tm_hour       ,
      gtime->tm_min        ,
      gtime->tm_sec        ,
      msecs);

  return String::construct(buffer);
}

template <>
String toString<std::chrono::     seconds>(
    const std::chrono::     seconds& value
) {
  return numberToString(value.count(), "%lld") +  "s";
}

template <>
String toString<std::chrono::milliseconds>(
    const std::chrono::milliseconds& value
) {
  return numberToString(value.count(), "%lld") + "ms";
}

template <>
String toString<std::chrono::microseconds>(
    const std::chrono::microseconds& value
) {
  return numberToString(value.count(), "%lld") + "us";
}

template <>
String toString<std::chrono:: nanoseconds>(
    const std::chrono:: nanoseconds& value
) {
  return numberToString(value.count(), "%lld") + "ns";
}

}  // namespace ambiramus
}  // namespace lvd
