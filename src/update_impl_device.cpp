/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "update_impl.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <Arduino.h>
#include <Stream.h>
#include <StreamString.h>
#include <Updater.h>

// ----------

namespace lvd {
namespace ambiramus {

class Update::Impl::Data {

};

// ----------

Update::Impl::Impl(Update* update)
    : data_(std::make_unique<Data>()),
      update_(update) {}

Update::Impl::~Impl() = default;

void Update::Impl::setup() {
  // void
}

void Update::Impl::loop() {
  // void
}

void Update::Impl::close() {
  // void
}

bool Update::Impl::updateSetup() {
  bool ok = ::Update.begin((ESP.getFreeSketchSpace() - 0x1000) & 0xFFFFF000);
  if (!ok) {
    return false;
  }

  return true;
}

bool Update::Impl::updateData(const Bytes& bytes) {
  auto ok = ::Update.write(const_cast<Byte*>(bytes.data()), bytes.size());
  if (!ok) {
    return false;
  }

  return true;
}

bool Update::Impl::updateClose() {
  bool ok = ::Update.end(true);
  if (!ok) {
    return false;
  }

  return true;
}

bool Update::Impl::updateAbort() {
  if (::Update.isRunning()) {
    bool ok = ::Update.end(false);
    if (ok) {
      return false;
    }
  }

  return true;
}

auto Update::Impl::successMessage() -> String {
  return String();
}

auto Update::Impl::failureMessage() -> String {
  StreamString  streamString;

  ::Update.printError(streamString);
  return String::construct(streamString.c_str());
}

}  // namespace ambiramus
}  // namespace lvd
