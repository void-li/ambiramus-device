/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include "device_id.hpp"
#include "device_preferences.hpp"
#include "preferences.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class DevicePreferencesManager {
 public:
  DevicePreferencesManager(const Preferences::SharedPtr& preferences,
                           const DeviceId                deviceId)
      : preferences_(preferences),
        deviceId_   (deviceId   ) {}

 public:
  auto load() -> DevicePreferences;
  void save(const DevicePreferences& devicePreferences);

 private:
  Preferences::SharedPtr preferences_;

 private:
  const DeviceId deviceId_;
};

}  // namespace ambiramus
}  // namespace lvd
