/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <type_traits>

// ----------

namespace lvd {
namespace ambiramus {

template <class T>
auto enum2type(T e) -> typename std::underlying_type<T>::type {
  using U = typename std::underlying_type<T>::type;
  return U(e);
}

template <class T>
auto type2enum(typename std::underlying_type<T>::type t) -> T {
  return T(t);
}

}  // namespace ambiramus
}  // namespace lvd
