/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "master_preferences.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "string.hpp"

// ----------

namespace lvd {
namespace ambiramus {

template <>
String toString<MasterPreferences>(const MasterPreferences& masterPreferences) {
  String string = STRREF("MasterPreferences");
  string.append("{");

  string.append("uuid='");
  string.append(toString(masterPreferences.uuid()));
  string.append("',");

  string.append("name='");
  string.append(toString(masterPreferences.name()));
  string.append("'" );

  string.append("}");
  return string;
}

}  // namespace ambiramus
}  // namespace lvd
