/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <memory>

#include "persistence.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class Persistence::Impl {
 public:
  Impl(Persistence* persistence);
  virtual ~Impl();

 public:
  virtual bool read (      Byte* bytes, std::size_t index, std::size_t count);
  virtual bool write(const Byte* bytes, std::size_t index, std::size_t count);
  virtual bool erase();

 public:
  static auto space() -> std::size_t;

#ifdef UNIT_TEST
//friend class TestPersistence;
  friend class TestPersistenceImpl;
#endif

 private:
  class           Data;
  std::unique_ptr<Data> data_;

  Persistence* const persistence_;
};

}  // namespace ambiramus
}  // namespace lvd
