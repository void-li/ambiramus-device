/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "ambira.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <algorithm>

#include "config.hpp"
#include "finally.hpp"
#include "hexify.hpp"
#include "logger.hpp"
#include "portal_preferences.hpp"
#include "portal_preferences_manager.hpp"
#include "preferences.hpp"
#include "signal.hpp"
#include "string.hpp"
#include "wifiap_preferences.hpp"
#include "wifiap_preferences_manager.hpp"

// ----------

namespace {
using namespace lvd::ambiramus;

bool compareModules(const Module* lhs,
                    const Module* rhs) {
  const auto& lhsInstant = lhs->timer().instant();
  const auto& rhsInstant = rhs->timer().instant();

  if (   lhsInstant.has_value()
      && rhsInstant.has_value()) {
    return lhsInstant.value() < rhsInstant.value();
  }

  if (   lhsInstant.has_value()) {
    return true;
  }

  return false;
}

void balanceModules(std::vector<Module*>&          modules,
                    std::vector<Module*>::iterator moduleIt) {
  bool done = false;

  if (!done) {
    for (; moduleIt != modules.begin(); moduleIt--) {
      auto moduleJt = moduleIt - 1;

      if (compareModules(*moduleIt, *moduleJt)) {
        std::iter_swap(moduleIt, moduleJt);
        done = true;
      } else {
        break;
      }
    }
  }

  if (!done) {
    for (; moduleIt != modules.end  (); moduleIt++) {
      auto moduleJt = moduleIt + 1;
      if (moduleJt == modules.end()) {
        break;
      }

      if (compareModules(*moduleJt, *moduleIt)) {
        std::iter_swap(moduleJt, moduleIt);
        done = true;
      } else {
        break;
      }
    }
  }
}

// ----------

String addrToText(uint32_t addr) {
  String string;

  string += toString(static_cast<uint8_t>(addr >> 24));
  string += ".";
  string += toString(static_cast<uint8_t>(addr >> 16));
  string += ".";
  string += toString(static_cast<uint8_t>(addr >>  8));
  string += ".";
  string += toString(static_cast<uint8_t>(addr >>  0));

  return string;
}

}  // namespace

// ----------

namespace lvd {
namespace ambiramus {

Ambira::Ambira(Construction,
               Preferences::SharedPtr preferences) {
  access_ = std::make_shared<Access>(this);
  Access::SharedPtr access = access_;

  deviceManager_ = DeviceManager::constructShared(preferences);
  outputManager_ = OutputManager::constructShared(preferences);

  system_ = System::constructShared();
  initializeModule(system_);

  reboot_ = Reboot::constructShared(system_);
  initializeModule(reboot_);

  WifiApPreferencesManager wifiApPreferencesManager(preferences);
  WifiApPreferences        wifiApPreferences = wifiApPreferencesManager.load();

  wifiAp_ = WifiAp::constructShared(             wifiApPreferences);
  initializeModule(wifiAp_);

  PortalPreferencesManager portalPreferencesManager(preferences);
  PortalPreferences        portalPreferences = portalPreferencesManager.load();

  portal_ = Portal::constructShared(preferences, portalPreferences);
  initializeModule(portal_);

  server_ = Server::constructShared();
  initializeModule(server_);

  client_ = Client::constructShared();
  initializeModule(client_);

  update_ = Update::constructShared();
  initializeModule(update_);

  Signal<DeviceManager*()> reqDeviceManager;
  reqDeviceManager.setSignal([access] {
    if (access && *access) {
      return access->access()->deviceManager_.get();
    }
    return static_cast<DeviceManager*>(nullptr);
  });

  Signal<OutputManager*()> reqOutputManager;
  reqOutputManager.setSignal([access] {
    if (access && *access) {
      return access->access()->outputManager_.get();
    }
    return static_cast<OutputManager*>(nullptr);
  });

  Signal<System       *()> reqSystem;
  reqSystem       .setSignal([access] {
    if (access && *access) {
      return access->access()->system_       .get();
    }

    return static_cast<System       *>(nullptr);
  });

  Signal<Reboot       *()> reqReboot;
  reqReboot       .setSignal([access] {
    if (access && *access) {
      return access->access()->reboot_       .get();
    }

    return static_cast<Reboot       *>(nullptr);
  });

  Signal<Update       *()> reqUpdate;
  reqUpdate       .setSignal([access] {
    if (access && *access) {
      return access->access()->update_       .get();
    }

    return static_cast<Update       *>(nullptr);
  });

  Signal<WifiAp       *()> reqWifiAp;
  reqWifiAp       .setSignal([access] {
    if (access && *access) {
      return access->access()->wifiAp_       .get();
    }

    return static_cast<WifiAp       *>(nullptr);
  });

  portal_->reqDeviceManager = reqDeviceManager;
  portal_->reqOutputManager = reqOutputManager;

  portal_->reqSystem        = reqSystem;
  portal_->reqReboot        = reqReboot;
  portal_->reqUpdate        = reqUpdate;
  portal_->reqWifiAp        = reqWifiAp;

  server_->sigMessage.setSignal([access](uint32_t addr, const Bytes& bytes) {
    if (access && *access) {
      access->access()->handleServerMessage(addr, bytes);
      return true;
    }
    return false;
  });

  client_->reqDeviceManager = reqDeviceManager;
  client_->reqOutputManager = reqOutputManager;

  update_->sigSuccess.setSignal([this] {
    reboot_->reboot();
  });

  update_->sigFailure.setSignal([    ] {

  });
}

Ambira::~Ambira() {
  access_->invalidate();
}

void Ambira::setup() {
  LOG_D("Ambira")
      << STRREF("setup");

  clearLoopAlways();
  clearLoopCyclic();

  system_->setup();

  wifiAp_->setup();
  portal_->setup();

  server_->setup();
  client_->setup();

  update_->setup();
}

void Ambira::loop() {
//LOG_D("Ambira")
//    << STRREF("loop");

  for (std::size_t i = 0; i < loopAlways_.size(); i++) {
    Module* module = loopAlways_[i];
    module->loop();

    if (loopAlways_[i] != module) {
      i--;
    }
  }

  for (std::size_t i = 0; i < loopCyclic_.size(); i=0) {
    Module* module = loopCyclic_[i];

    auto instant = module->timer().instant();
    if (!instant.has_value()) {
      break;
    }
    if ( instant.    value() > std::chrono::system_clock::now()) {
      break;
    }

    {
      module->timer().acquire();
      FINALLY {
        module->timer().release();
      };

      module->loop();
    }

    if (loopCyclic_[i] == module) {
      balanceModules(loopCyclic_, loopCyclic_.begin() + i);
    }

    break;
  }
}

void Ambira::close() {
  LOG_D("Ambira")
      << STRREF("close");

  FINALLY {
    clearLoopAlways();
    clearLoopCyclic();
  };

  if (update_) {
    update_->close();
    update_.reset();
  }

  if (client_) {
    client_->close();
    client_.reset();
  }

  if (server_) {
    server_->close();
    server_.reset();
  }

  if (portal_) {
    portal_->close();
    portal_.reset();
  }

  if (wifiAp_) {
    wifiAp_->close();
    wifiAp_.reset();
  }

  if (reboot_) {
    reboot_->close();
    reboot_.reset();
  }

  if (system_) {
    system_->close();
    system_.reset();
  }
}

template <class T>
void Ambira::initializeModule(std::shared_ptr<T>& module) {
  Access::SharedPtr access = access_;

  module->sigLoopAlways.setSignal([access](Module* module) {
    if (access && *access) {
      access->access()->onLoopAlways(module);
      return true;
    }

    return false;
  });

  module->sigLoopCyclic.setSignal([access](Module* module) {
    if (access && *access) {
      access->access()->onLoopCyclic(module);
      return true;
    }

    return false;
  });

  module->sigLoopIdling.setSignal([access](Module* module) {
    if (access && *access) {
      access->access()->onLoopIdling(module);
      return true;
    }

    return false;
  });
}

void Ambira::onLoopAlways(Module* module) {
  loopCyclic_.erase(
      std::remove_if(loopCyclic_.begin(),
                     loopCyclic_.end  (),
                     [module](const Module* testee) {
        return testee == module;
      }), loopCyclic_.end()
  );

  auto it = std::find(loopAlways_.begin(),
                      loopAlways_.end  (),
                      module);

  if (it == loopAlways_.end()) {
    loopAlways_.push_back(module);
    it = loopAlways_.end() - 1;
  }
}

void Ambira::onLoopCyclic(Module* module) {
  loopAlways_.erase(
      std::remove_if(loopAlways_.begin(),
                     loopAlways_.end  (),
                     [module](const Module* testee) {
        return testee == module;
      }), loopAlways_.end()
  );

  auto it = std::find(loopCyclic_.begin(),
                      loopCyclic_.end  (),
                      module);

  if (it == loopCyclic_.end()) {
    loopCyclic_.push_back(module);
    it = loopCyclic_.end() - 1;
  }

  balanceModules(loopCyclic_, it);
}

void Ambira::onLoopIdling(Module* module) {
  loopAlways_.erase(
      std::remove_if(loopAlways_.begin(),
                     loopAlways_.end  (),
                     [module](const Module* testee) {
        return testee == module;
      }), loopAlways_.end()
  );

  loopCyclic_.erase(
      std::remove_if(loopCyclic_.begin(),
                     loopCyclic_.end  (),
                     [module](const Module* testee) {
        return testee == module;
      }), loopCyclic_.end()
  );
}

template <class T, class U>
std::unique_ptr<T> unique_ptr_cast(std::unique_ptr<U>& ptr) {
  return std::unique_ptr<T>(static_cast<T*>(ptr.release()));
}

void Ambira::handleServerMessage(uint32_t addr, const Bytes& bytes) {
//LOG_D("Ambira")
//    << STRREF("handleServerMessage")
//    << addrToText(addr) << bytes;

  auto message = Message::decode(bytes);
  if (!message) {
    LOG_W("Ambira")
        << STRREF("invalid message")
        << STRREF("by")
        << addrToText(addr)
        << STRREF("with")
        << bytes;

    return;
  }

  switch (message->species()) {
    case Message::Species::COLORIZE         : {
      auto messageColorize         = unique_ptr_cast<Message::Colorize        >(message);
      handleServerMessage(addr, messageColorize        );
    } break;

    case Message::Species::ANNOUNCE_RESPONSE: {
      auto messageAnnounceResponse = unique_ptr_cast<Message::AnnounceResponse>(message);
      handleServerMessage(addr, messageAnnounceResponse);
    } break;

    case Message::Species::COLORIZE_RESPONSE: {
      auto messageColorizeResponse = unique_ptr_cast<Message::ColorizeResponse>(message);
      handleServerMessage(addr, messageColorizeResponse);
    } break;

    case Message::Species::ANNOUNCE         : {
      auto messageAnnounce         = unique_ptr_cast<Message::Announce        >(message);
      handleServerMessage(addr, messageAnnounce        );
    } break;

    default:
      LOG_W("Ambira")
          << STRREF("unknown message")
          << STRREF("by")
          << addrToText(addr)
          << STRREF("with")
          << bytes;
  }
}

void Ambira::handleServerMessage(uint32_t addr, const Message::Announce        ::UniquePtr& message) {
  LOG_W("Ambira")
      << STRREF("unexpected message")
      << STRREF("Announce"        )
      << STRREF("by")
      << addrToText(addr)
      << STRREF("with")
      << message->devicePreferencesItem().uuid()
      << message->devicePreferencesItem().name();
}

void Ambira::handleServerMessage(uint32_t addr, const Message::AnnounceResponse::UniquePtr& message) {
  LOG_I("Ambira")
      << STRREF(  "expected message")
      << STRREF("AnnounceResponse")
      << STRREF("by")
      << addrToText(addr)
      << STRREF("with")
      << message->masterPreferencesItem().uuid()
      << message->masterPreferencesItem().name();
}

void Ambira::handleServerMessage(uint32_t addr, const Message::Colorize        ::UniquePtr& message) {
//auto device = deviceManager_->search(message->deviceUuid());
//if (!device) {
//  LOG_W("Ambira")
//      << STRREF("unknown device")
//      << message->deviceUuid();

//  return;
//}

  auto output = outputManager_->search(message->outputUuid());
  if (!output) {
    LOG_W("Ambira")
        << STRREF("unknown output")
        << message->outputUuid();

    return;
  }

  output->colorize(
      message->colors()
  );

  if (client_) {
    client_->airmailMessage(
        addr, Message::ColorizeResponse::create(message)->encode()
    );
  }
}

void Ambira::handleServerMessage(uint32_t addr, const Message::ColorizeResponse::UniquePtr& message) {
  LOG_W("Ambira")
      << STRREF("unexpected message")
      << STRREF("ColorizeResponse")
      << STRREF("by")
      << addrToText(addr)
      << STRREF("with")
      << message->outputUuid();
}

void Ambira::clearLoopAlways() {
  loopAlways_.clear();
}

void Ambira::clearLoopCyclic() {
  loopCyclic_.clear();
}

}  // namespace ambiramus
}  // namespace lvd
