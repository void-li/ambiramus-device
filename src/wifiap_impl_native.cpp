/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "wifiap_impl.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <chrono>

#include <QTimer>

// ----------

namespace lvd {
namespace ambiramus {

class WifiAp::Impl::Data {
 public:
  bool   client = false;
  QTimer clientTimer;

  bool   access = false;

  bool   clscan = false;
  QTimer clscanTimer;
};

// ----------

WifiAp::Impl::Impl(WifiAp* wifiAp)
    : data_(std::make_unique<Data>()),
      wifiAp_(wifiAp) {
  data_->clientTimer.setSingleShot(false);
  data_->clientTimer.setInterval(std::chrono::milliseconds(30000));

  QTimer::connect(&data_->clientTimer, &QTimer::timeout, [this] {
    if (data_->client) {
      data_->client = false;
      wifiAp_->onDisconnected();
    } else {
      data_->client = true ;
      wifiAp_->onConnected   ();
    }
  });

  data_->clscanTimer.setSingleShot(true );
  data_->clscanTimer.setInterval(std::chrono::milliseconds( 3000));

  QTimer::connect(&data_->clscanTimer, &QTimer::timeout, [this] {
    data_->clscan = false;
  });
}

WifiAp::Impl::~Impl() {
  data_->clscanTimer.stop();
  data_->clientTimer.stop();
}

void WifiAp::Impl::setup() {
  // void
}

void WifiAp::Impl::loop() {
  // void
}

void WifiAp::Impl::close() {
  // void
}

void WifiAp::Impl::scan() {
  data_->clscan = true ;
  data_->clscanTimer.start();
}

void WifiAp::Impl::clientSetup() {
  data_->client = true ;
  data_->clientTimer.start();
}

void WifiAp::Impl::clientClose() {
  data_->client = false;
  data_->clientTimer.stop ();
}

bool WifiAp::Impl::clientAlive() const {
  return data_->client;
}

bool WifiAp::Impl::clientScans() const {
  return data_->clscan;
}

void WifiAp::Impl::accessSetup() {
  data_->access = true ;
}

void WifiAp::Impl::accessClose() {
  data_->access = false;
}

bool WifiAp::Impl::accessAlive() const {
  return data_->access;
}

auto WifiAp::Impl::accessCount() const -> std::size_t {
  return 0;
}

auto WifiAp::Impl::networkSize() const -> std::size_t {
  return 2;
}

auto WifiAp::Impl::networkSsid(std::size_t index) const -> String {
  if (index == 0) {
    return STRREF("Mera");
  }
  if (index == 1) {
    return STRREF("Luna");
  }

  return String();
}

auto WifiAp::Impl::ipAddress() const -> String {
  return STRREF("127.0.0.1");
}

}  // namespace ambiramus
}  // namespace lvd
