/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include "string.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class PortalPreferences {
  friend class PortalPreferencesManager;

 public:
  enum class Subject : uint16_t {
    USERNAME = 0x0000,
    PASSWORD = 0x0010,
  };

 public:
  auto username() const -> const String& {
    return username_;
  }
  void setUsername(const String& username) {
    username_ = username;
  }

  auto password() const -> const String& {
    return password_;
  }
  void setPassword(const String& password) {
    password_ = password;
  }

 private:
  String username_;
  String password_;
};

// ----------

inline bool operator==(const PortalPreferences& lhs,
                       const PortalPreferences& rhs) {
  if (lhs.username() != rhs.username()) {
    return false;
  }

  if (lhs.password() != rhs.password()) {
    return false;
  }

  return true;
}

inline bool operator!=(const PortalPreferences& lhs,
                       const PortalPreferences& rhs) {
  return !(lhs == rhs);
}

}  // namespace ambiramus
}  // namespace lvd
