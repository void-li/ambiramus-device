/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "output_manager.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <algorithm>

#include "config.hpp"
#include "logger.hpp"
#include "output_preferences.hpp"
#include "output_preferences_manager.hpp"
#include "uuid_util.hpp"

// ----------

namespace lvd {
namespace ambiramus {

OutputManager::OutputManager(Construction,
                             const Preferences::SharedPtr& preferences)
    : preferences_(preferences) {
  for (std::size_t i = 0; i < ARRAY_SIZE(config::OUTPUTS); i++) {
    auto outputId = config::OUTPUTS[i].outputId;

    OutputPreferencesManager outputPreferencesManager(preferences_, outputId);
    OutputPreferences        outputPreferences = outputPreferencesManager.load();

    if (!outputPreferences.uuid().valid()) {
      outputPreferences.setUuid(UuidUtil::constructRandom());
      outputPreferencesManager.save(outputPreferences);
    }

    outputs_.push_back(
        Output::constructShared(
          outputId, outputPreferences
        )
    );
  }

  std::sort(outputs_.begin(), outputs_.end(),
            [](const Output::SharedPtr& lhs,
               const Output::SharedPtr& rhs) {
    return lhs->outputId() < rhs->outputId();
  });
}

auto OutputManager::search(const Uuid& uuid) -> Output::SharedPtr {
  auto it = std::find_if(outputs_.begin(),
                         outputs_.end  (),
                         [&uuid](const Output::SharedPtr& output) {
    return output->uuid() == uuid;
  });

  if (it == outputs_.end()) {
    return nullptr;
  }

  return *it;
}

bool OutputManager::update(const Uuid& uuid,
                           const OutputPreferences& outputPreferences) {
  auto output = search(uuid);
  if (!output) {
    LOG_W("OutputManager")
        << STRREF("unknown output")
        << uuid;

    return false;
  }

  if (outputPreferences != output->outputPreferences()) {
    output->setOutputPreferences(outputPreferences);

    OutputPreferencesManager outputPreferencesManager(preferences_, output->outputId());
    outputPreferencesManager.save(outputPreferences);

    LOG_I("OutputManager")
        << STRREF("updated output")
        << uuid;
  }

  return true;
}

}  // namespace ambiramus
}  // namespace lvd
