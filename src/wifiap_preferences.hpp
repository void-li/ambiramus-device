/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include "string.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class WifiApPreferences {
  friend class WifiApPreferencesManager;

 public:
  enum class Subject : uint16_t {
    CLIENT_SSID     = 0x0000,
    CLIENT_PASSWORD = 0x0001,

    ACCESS_SSID     = 0x0010,
    ACCESS_PASSWORD = 0x0011,
  };

 public:
  auto clientSsid() const -> const String& {
    return clientSsid_;
  }
  void setClientSsid(const String& clientSsid) {
    clientSsid_ = clientSsid;
  }

  auto clientPassword() const -> const String& {
    return clientPassword_;
  }
  void setClientPassword(const String& clientPassword) {
    clientPassword_ = clientPassword;
  }

  auto accessSsid() const -> const String& {
    return accessSsid_;
  }
  void setAccessSsid(const String& accessSsid) {
    accessSsid_ = accessSsid;
  }

  auto accessPassword() const -> const String& {
    return accessPassword_;
  }
  void setAccessPassword(const String& accessPassword) {
    accessPassword_ = accessPassword;
  }

 private:
  String clientSsid_;
  String clientPassword_;

  String accessSsid_;
  String accessPassword_;
};

// ----------

inline bool operator==(const WifiApPreferences& lhs,
                       const WifiApPreferences& rhs) {
  if (lhs.clientSsid    () != rhs.clientSsid    ()) {
    return false;
  }
  if (lhs.clientPassword() != rhs.clientPassword()) {
    return false;
  }

  if (lhs.accessSsid    () != rhs.accessSsid    ()) {
    return false;
  }
  if (lhs.accessPassword() != rhs.accessPassword()) {
    return false;
  }

  return true;
}

inline bool operator!=(const WifiApPreferences& lhs,
                       const WifiApPreferences& rhs) {
  return !(lhs == rhs);
}

}  // namespace ambiramus
}  // namespace lvd
