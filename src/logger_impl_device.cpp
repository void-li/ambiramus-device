/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "logger_impl.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <HardwareSerial.h>

// ----------

namespace lvd {
namespace ambiramus {

class Logger::Impl::Data {

};

// ----------

Logger::Impl::Impl(Logger* logger)
    : data_(std::make_unique<Data>()),
      logger_(logger) {
  Serial.begin(115200);
}

Logger::Impl::~Impl() {
  Serial.end();
}

void Logger::Impl::log(const String& prefix,
                       const String& string) {
  Serial.write(prefix.data(), prefix.size());
  Serial.write(string.data(), string.size());

  Serial.println();
}

}  // namespace ambiramus
}  // namespace lvd
