/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "color.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "bytes.hpp"
#include "hexify.hpp"
#include "logger.hpp"

// ----------

namespace lvd {
namespace ambiramus {

auto Color::construct(const String& colorString) -> std::optional<Color> {
  if (colorString.size() != 7) {
    LOG_W("Color")
        << STRREF("invalid size")
        << colorString.size();

    return std::nullopt;
  }

  if (colorString[0] != '#') {
    LOG_W("Color")
        << STRREF("invalid hash")
        << colorString[0];

    return std::nullopt;
  }

  auto r = hexToChr(colorString[1], colorString[2]);
  if (!r.has_value()) {
    LOG_W("Color")
        << STRREF("invalid hex value")
        << colorString[1]
        << colorString[2];

    return std::nullopt;
  }

  auto g = hexToChr(colorString[3], colorString[4]);
  if (!g.has_value()) {
    LOG_W("Color")
        << STRREF("invalid hex value")
        << colorString[3]
        << colorString[4];

    return std::nullopt;
  }

  auto b = hexToChr(colorString[5], colorString[6]);
  if (!b.has_value()) {
    LOG_W("Color")
        << STRREF("invalid hex value")
        << colorString[5]
        << colorString[6];

    return std::nullopt;
  }

  return Color(
      r.value(), g.value(), b.value()
  );
}

auto Color::toHex() const -> String {
  uint16_t r = chrToHex(r_);
  uint16_t g = chrToHex(g_);
  uint16_t b = chrToHex(b_);

  String string;
  string.resize(7);

  string[0] = '#';

  string[1] = r >> 0x08;
  string[2] = r >> 0x00;

  string[3] = g >> 0x08;
  string[4] = g >> 0x00;

  string[5] = b >> 0x08;
  string[6] = b >> 0x00;

  return string;
}
// ----------

template <>
String toString<Color>(const Color& value) {
  String string = STRREF("Color");
  string.append("{");

  string.append("r='");
  string.append(toString(value.r()));
  string.append("',");

  string.append("g='");
  string.append(toString(value.g()));
  string.append("',");

  string.append("b=");
  string.append(toString(value.b()));
  string.append("'" );

  string.append("}");
  return string;
}

}  // namespace ambiramus
}  // namespace lvd
