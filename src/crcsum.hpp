/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <cstdint>

#include "bytes.hpp"

// ----------

namespace lvd {
namespace ambiramus {

using CrcSum = uint32_t;

CrcSum crcsum(const Byte * bytes, std::size_t count);

inline
CrcSum crcsum(const Bytes& bytes, std::size_t count) {
  return crcsum(bytes.data(), count       );
}

inline
CrcSum crcsum(const Bytes& bytes                   ) {
  return crcsum(bytes.data(), bytes.size());
}

}  // namespace ambiramus
}  // namespace lvd
