/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "persistence_impl.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <Arduino.h>
#include <Esp.h>

#include "finally.hpp"

// ----------

extern "C" uint32_t _EEPROM_start;

namespace {

const uint32_t SECTOR = (((uint32_t)&_EEPROM_start - 0x40200000) / SPI_FLASH_SEC_SIZE);
const uint32_t EEPROM = (((uint32_t)&_EEPROM_start - 0x40200000) / SPI_FLASH_SEC_SIZE) * SPI_FLASH_SEC_SIZE;

}  // namespace

// ----------

namespace lvd {
namespace ambiramus {

class Persistence::Impl::Data {

};

// ----------

Persistence::Impl::Impl(Persistence* persistence)
    : data_(std::make_unique<Data>()),
      persistence_(persistence) {}

Persistence::Impl::~Impl() = default;

bool Persistence::Impl::read (      Byte* bytes, std::size_t index, std::size_t count) {
  auto bound = index + count;
  if (bound > static_cast<std::size_t>(SPI_FLASH_SEC_SIZE)) {
    return false;
  }

  noInterrupts();
  FINALLY {
    interrupts();
  };

  bool ok = ESP.flashRead (EEPROM + index, reinterpret_cast<uint32_t*>(bytes), count);
  if (!ok) {
    return false;
  }

  return true;
}

bool Persistence::Impl::write(const Byte* bytes, std::size_t index, std::size_t count) {
  auto bound = index + count;
  if (bound > static_cast<std::size_t>(SPI_FLASH_SEC_SIZE)) {
    return false;
  }

  noInterrupts();
  FINALLY {
    interrupts();
  };

  Byte* mutab = const_cast<Byte*>(bytes);

  bool ok = ESP.flashWrite(EEPROM + index, reinterpret_cast<uint32_t*>(mutab), count);
  if (!ok) {
    return false;
  }

  return true;
}

bool Persistence::Impl::erase() {
  noInterrupts();
  FINALLY {
    interrupts();
  };

  bool ok = ESP.flashEraseSector(SECTOR);
  if (!ok) {
    return false;
  }

  return true;
}

auto Persistence::Impl::space() -> std::size_t {
  return SPI_FLASH_SEC_SIZE;
}

}  // namespace ambiramus
}  // namespace lvd
