/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <memory>
#include <vector>

#include "module.hpp"
#include "object.hpp"
#include "wifiap_preferences.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class WifiAp : public Object<WifiAp>, public Module {
 public:
  class Network;
  using Networks = std::vector<Network>;

 public:
  WifiAp(Construction,
         const WifiApPreferences& wifiApPreferences);
  ~WifiAp();

 public:
  virtual void setup() override;
  virtual void loop () override;
  virtual void close() override;

 public:
  void scan();
  bool scanning();

  auto networks() -> Networks;

 private:
  void onConnected   ();
  void onDisconnected();

 private:
  void clientSetup();
  void clientClose();

  void accessSetup();
  void accessClose();

 public:
  auto wifiApPreferences() const -> const WifiApPreferences& {
    return wifiApPreferences_;
  }
  void setWifiApPreferences(const WifiApPreferences& wifiApPreferences);

 private:
  class           Impl;
  std::unique_ptr<Impl> impl_;

 private:
  WifiApPreferences wifiApPreferences_;

 private:
  bool     scanning_ = false;
  Networks networks_ = {   };

 private:
  bool clientAlive_ = false;
  bool accessAlive_ = false;
};

// ----------

class WifiAp::Network {
 public:
  Network(const String& ssid)
      : ssid_(ssid) {}

 public:
  auto ssid() const -> const String& {
    return ssid_;
  }

 private:
  String ssid_;
};

}  // namespace ambiramus
}  // namespace lvd
