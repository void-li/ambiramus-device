/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <optional>

#include "string.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class Number {
 public:
  template <class T>
  Number(T number)
      : number_(number) {}

 public:
  static auto construct(const String& numberString) -> std::optional<Number>;

 public:
  template <class T>
  T to() {
    return number_;
  }

 public:
  template <class T>
  operator T() const {
    return number_;
  }

 private:
  const uint64_t number_;
};

}  // namespace ambiramus
}  // namespace lvd
