/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <cstdlib>
#include <functional>

#include "logger.hpp"
#include "string.hpp"

// ----------

namespace lvd {
namespace ambiramus {

template <class R>
class Signal;

template <class R, class... Args>
class Signal<R(Args...)> {
 public:
  auto operator()(Args... args) -> R {
    if (!signal_) {
      for (std::size_t i = 0; i < 42; i++) {
        LOG_F("Signal")
            << STRREF("signal unset");
      }

      std::abort();
    }

    return signal_(std::forward<Args>(args)...);
  }

 public:
  void setSignal(const std::function<R(Args...)>& signal) {
    signal_ = signal ;
  }

 private:
  std::function<R(Args...)> signal_;
};

}  // namespace ambiramus
}  // namespace lvd
