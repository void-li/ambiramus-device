/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include "object.hpp"
#include "string.hpp"

// ----------

namespace lvd {
namespace ambiramus {

template <class T>
class Access : public Object<Access<T>> {
 public:
  Access(T* access)
      : access_(access) {}

 public:
  void invalidate() {
    access_ = nullptr;
  }

 public:
  operator bool() const {
    return access_ != nullptr;
  }

 public:
  T* access() const {
    return access_;
  }

 private:
  T* access_ = nullptr;
};

// ----------

template <class T>
String toString(const Access<T>& value) {
  return (value) ? toString(*value.access()) : toString(nullptr);
}

}  // namespace ambiramus
}  // namespace lvd
