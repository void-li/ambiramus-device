/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "device_manager.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <algorithm>

#include "config.hpp"
#include "device_preferences.hpp"
#include "device_preferences_manager.hpp"
#include "logger.hpp"
#include "uuid_util.hpp"

// ----------

namespace lvd {
namespace ambiramus {

DeviceManager::DeviceManager(Construction,
                             const Preferences::SharedPtr& preferences)
    : preferences_(preferences) {
  for (std::size_t i = 0; i < ARRAY_SIZE(config::DEVICES); i++) {
    auto deviceId = config::DEVICES[i].deviceId;

    DevicePreferencesManager devicePreferencesManager(preferences_, deviceId);
    DevicePreferences        devicePreferences = devicePreferencesManager.load();

    if (!devicePreferences.uuid().valid()) {
      devicePreferences.setUuid(UuidUtil::constructRandom());
      devicePreferencesManager.save(devicePreferences);
    }

    devices_.push_back(
        Device::constructShared(
          deviceId, devicePreferences
        )
    );
  }

  std::sort(devices_.begin(), devices_.end(),
            [](const Device::SharedPtr& lhs,
               const Device::SharedPtr& rhs) {
    return lhs->deviceId() < rhs->deviceId();
  });
}

auto DeviceManager::search(const Uuid& uuid) -> Device::SharedPtr {
  auto it = std::find_if(devices_.begin(),
                         devices_.end  (),
                         [&uuid](const Device::SharedPtr& device) {
    return device->uuid() == uuid;
  });

  if (it == devices_.end()) {
    return nullptr;
  }

  return *it;
}

bool DeviceManager::update(const Uuid& uuid,
                           const DevicePreferences& devicePreferences) {
  auto device = search(uuid);
  if (!device) {
    LOG_W("DeviceManager")
        << STRREF("unknown device")
        << uuid;

    return false;
  }

  if (devicePreferences != device->devicePreferences()) {
    device->setDevicePreferences(devicePreferences);

    DevicePreferencesManager devicePreferencesManager(preferences_, device->deviceId());
    devicePreferencesManager.save(devicePreferences);

    LOG_I("DeviceManager")
        << STRREF("updated device")
        << uuid;
  }

  return true;
}

}  // namespace ambiramus
}  // namespace lvd
