/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "uuid.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "bytes.hpp"
#include "hexify.hpp"
#include "logger.hpp"

// ----------

namespace {
using namespace lvd::ambiramus;

std::optional<uint64_t> parseDigits(
    const String& uuid, std::size_t& index, std::size_t count
) {
  uint64_t value = 0;

  for (std::size_t i = index; i < index + count * 2; i += 2) {
    auto byte = lvd::ambiramus::hexToChr(uuid[i + 0], uuid[i + 1]);
    if (!byte.has_value()) {
      return std::nullopt;
    }

    value = (value << 0x08) | byte.value();
  }

  index += count * 2;
  return value;
}

}  // namespace

// ----------

namespace lvd {
namespace ambiramus {

const std::size_t SIZE = 36;

auto Uuid::construct(const String& uuidString) -> std::optional<Uuid> {
  if (uuidString.size() != SIZE) {
    LOG_W("Uuid")
        << STRREF("invalid size");

    return std::nullopt;
  }

  std::size_t index = 0;

  auto data0 = parseDigits(uuidString, index, 4);
  if (!data0.has_value()) {
    LOG_W("Uuid")
        << STRREF("invalid uuid");

    return std::nullopt;
  }

  if (uuidString[index] != '-') {
    LOG_W("Uuid")
        << STRREF("invalid uuid");

    return std::nullopt;
  }
  index++;

  auto data1 = parseDigits(uuidString, index, 2);
  if (!data1.has_value()) {
    LOG_W("Uuid")
        << STRREF("invalid uuid");

    return std::nullopt;
  }

  if (uuidString[index] != '-') {
    LOG_W("Uuid")
        << STRREF("invalid uuid");

    return std::nullopt;
  }
  index++;

  auto data2 = parseDigits(uuidString, index, 2);
  if (!data2.has_value()) {
    LOG_W("Uuid")
        << STRREF("invalid uuid");

    return std::nullopt;
  }

  if (uuidString[index] != '-') {
    LOG_W("Uuid")
        << STRREF("invalid uuid");

    return std::nullopt;
  }
  index++;

  auto data3 = parseDigits(uuidString, index, 2);
  if (!data3.has_value()) {
    LOG_W("Uuid")
        << STRREF("invalid uuid");

    return std::nullopt;
  }

  if (uuidString[index] != '-') {
    LOG_W("Uuid")
        << STRREF("invalid uuid");

    return std::nullopt;
  }
  index++;

  auto data4 = parseDigits(uuidString, index, 6);
  if (!data4.has_value()) {
    LOG_W("Uuid")
        << STRREF("invalid uuid");

    return std::nullopt;
  }

  uint64_t part0 = static_cast<uint64_t>(data0.value()) << 32
                 | static_cast<uint64_t>(data1.value()) << 16
                 | static_cast<uint64_t>(data2.value());

  uint64_t part1 = static_cast<uint64_t>(data3.value()) << 48
                 | static_cast<uint64_t>(data4.value());

  Uuid uuid(part0, part1);
  uuid.uuid_  = uuidString;
  uuid.valid_ = true;

  return uuid;
}

// ----------

template <>
String toString<Uuid>(const Uuid& value) {
  if (value.uuid_.empty()) {
    Bytes::Encoder encoder;

    encoder.bytes().reserve(
        + sizeof(value.part0_)
        + sizeof(value.part1_)
    );

    encoder.encode(value.part0_);
    encoder.encode(value.part1_);

    Bytes bytes = encoder.bytes();

    for (std::size_t i = 0; i < bytes.size(); i++) {
      int16_t vchar = chrToHex(bytes[i], HexifyCase::LOWER_CASE);

      value.uuid_.push_back(vchar >> 0x08);
      value.uuid_.push_back(vchar &  0xFF);

      if (i == 3 || i == 5 || i == 7 || i == 9) {
        value.uuid_.push_back('-');
      }
    }
  }

  return value.uuid_;
}

// ----------

template <>
void Bytes::Encoder::encode(const Uuid& value) {
  encode(value.part0());
  encode(value.part1());
}

template <>
auto Bytes::Decoder::decode() -> std::optional<Uuid> {
  auto upperBits = decode<uint64_t>();
  if (!upperBits.has_value()) {
    return std::nullopt;
  }

  auto lowerBits = decode<uint64_t>();
  if (!lowerBits.has_value()) {
    return std::nullopt;
  }

  return Uuid(
      upperBits.value(),
      lowerBits.value()
  );
}

template <>
auto Bytes::Encoder::length(const Uuid& value) -> std::size_t {
  return 0
+ sizeof(value.part0())
+ sizeof(value.part1());
}

}  // namespace ambiramus
}  // namespace lvd
