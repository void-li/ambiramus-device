/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <chrono>
#include <cstdint>
#include <vector>

#include "bytes.hpp"
#include "colors.hpp"
#include "device_preferences.hpp"
#include "master_preferences.hpp"
#include "name.hpp"
#include "object.hpp"
#include "output_preferences.hpp"
#include "string.hpp"
#include "uuid.hpp"
#include "vers.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class Message : public Object<Message> {
 public:
  enum class Species : uint16_t {
    ANNOUNCE          = 0x0000,
    ANNOUNCE_RESPONSE = 0x1000,
    COLORIZE          = 0x1010,
    COLORIZE_RESPONSE = 0x0010,
  };
  /**/ using Variant = uint16_t;

  /**/ using Instant = std::chrono::system_clock::time_point;

 public:
  class Announce;
  class AnnounceResponse;

  class Colorize;
  class ColorizeResponse;

 protected:
  Message(Species species, Variant variant);

 public:
  virtual
  ~Message();

 public:
  static  auto decode(const Bytes& bytes) -> UniquePtr;
  /****/  auto encode() const -> Bytes;

 protected:
  virtual void encodeBytes(Bytes::Encoder& encoder) const = 0;

 public:
  virtual String toString() const = 0;

 public:
  Species species() const {
    return species_;
  }
  Variant variant() const {
    return variant_;
  }

  Instant instant() const {
    return instant_;
  }

 private:
  const Species species_;
  const Variant variant_;

 private:
  /***/ Instant instant_ = std::chrono::system_clock::now();
};

// ----------

class Message::Announce : public Message, public Object<Announce> {
 public:
  using Construction   = Object<Announce>::Construction;

  using ConstSharedPtr = Object<Announce>::ConstSharedPtr;
  using      SharedPtr = Object<Announce>::     SharedPtr;

  using ConstUniquePtr = Object<Announce>::ConstUniquePtr;
  using      UniquePtr = Object<Announce>::     UniquePtr;

  template <class... U>
  static SharedPtr constructShared(U&&... args) {
    return Object<Announce>::constructShared(std::forward(args)...);
  }

  template <class... U>
  static UniquePtr constructUnique(U&&... args) {
    return Object<Announce>::constructUnique(std::forward(args)...);
  }

 public:
  enum class Variant : Message::Variant {
    VVOID = 0x0000,
    V0001 = 0x0001,
  };

 public:
  using DevicePreferencesItem =             DevicePreferences;
  using OutputPreferencesItem =             OutputPreferences;
  using OutputPreferencesList = std::vector<OutputPreferences>;

 public:
  Announce(Construction);

 public:
  static UniquePtr decodeBytes(Bytes::Decoder& decoder, Message::Variant variant);
  static UniquePtr decodeV0001(Bytes::Decoder& decoder);

  void             encodeBytes(Bytes::Encoder& encoder) const override;
  void             encodeV0001(Bytes::Encoder& encoder) const;

 public:
  String toString() const override;

 public:
  auto name() const -> const Name& {
    return name_;
  }
  void setName(const Name& name) {
    name_ = name;
  }

  auto vers() const -> const Vers& {
    return vers_;
  }
  void setVers(const Vers& vers) {
    vers_ = vers;
  }

  auto devicePreferencesItem() const -> const DevicePreferencesItem& {
    return devicePreferencesItem_;
  }
  void setDevicePreferencesItem(const DevicePreferencesItem& devicePreferencesItem) {
    devicePreferencesItem_ = devicePreferencesItem;
  }

  auto outputPreferencesList() const -> const OutputPreferencesList& {
    return outputPreferencesList_;
  }
  void setOutputPreferencesList(const OutputPreferencesList& outputPreferencesList) {
    outputPreferencesList_ = outputPreferencesList;
  }

  void addOutputPreferencesItem(const OutputPreferencesItem& outputPreferencesItem) {
    outputPreferencesList_.push_back(outputPreferencesItem);
  }

 private:
  Name name_;
  Vers vers_;

  DevicePreferencesItem devicePreferencesItem_;
  OutputPreferencesList outputPreferencesList_;
};

// ----------

class Message::AnnounceResponse : public Message, public Object<AnnounceResponse> {
 public:
  using Construction   = Object<AnnounceResponse>::Construction;

  using ConstSharedPtr = Object<AnnounceResponse>::ConstSharedPtr;
  using      SharedPtr = Object<AnnounceResponse>::     SharedPtr;

  using ConstUniquePtr = Object<AnnounceResponse>::ConstUniquePtr;
  using      UniquePtr = Object<AnnounceResponse>::     UniquePtr;

  template <class... U>
  static SharedPtr constructShared(U&&... args) {
    return Object<AnnounceResponse>::constructShared(std::forward(args)...);
  }

  template <class... U>
  static UniquePtr constructUnique(U&&... args) {
    return Object<AnnounceResponse>::constructUnique(std::forward(args)...);
  }

 public:
  enum class Variant : Message::Variant {
    VVOID = 0x0000,
    V0001 = 0x0001,
  };

 public:
  using MasterPreferencesItem = MasterPreferences;

 public:
  AnnounceResponse(Construction);

 public:
  static UniquePtr decodeBytes(Bytes::Decoder& decoder, Message::Variant variant);
  static UniquePtr decodeV0001(Bytes::Decoder& decoder);

  void             encodeBytes(Bytes::Encoder& encoder) const override;
  void             encodeV0001(Bytes::Encoder& encoder) const;

 public:
  String toString() const override;

 public:
  auto masterPreferencesItem() const -> const MasterPreferencesItem& {
    return masterPreferencesItem_;
  }
  void setMasterPreferencesItem(const MasterPreferencesItem& masterPreferencesItem) {
    masterPreferencesItem_ = masterPreferencesItem;
  }

 private:
  MasterPreferencesItem masterPreferencesItem_;
};

// ----------

class Message::Colorize : public Message, public Object<Colorize> {
 public:
  using Construction   = Object<Colorize>::Construction;

  using ConstSharedPtr = Object<Colorize>::ConstSharedPtr;
  using      SharedPtr = Object<Colorize>::     SharedPtr;

  using ConstUniquePtr = Object<Colorize>::ConstUniquePtr;
  using      UniquePtr = Object<Colorize>::     UniquePtr;

  template <class... U>
  static SharedPtr constructShared(U&&... args) {
    return Object<Colorize>::constructShared(std::forward(args)...);
  }

  template <class... U>
  static UniquePtr constructUnique(U&&... args) {
    return Object<Colorize>::constructUnique(std::forward(args)...);
  }

 public:
  enum class Variant : Message::Variant {
    VVOID = 0x0000,
    V0001 = 0x0001,
  };

 public:
  Colorize(Construction);

 public:
  static UniquePtr decodeBytes(Bytes::Decoder& decoder, Message::Variant variant);
  static UniquePtr decodeV0001(Bytes::Decoder& decoder);

  void             encodeBytes(Bytes::Encoder& encoder) const override;
  void             encodeV0001(Bytes::Encoder& encoder) const;

 public:
  String toString() const override;

 public:
//auto deviceUuid() const -> const Uuid& {
//  return deviceUuid_;
//}
//void setDeviceUuid(const Uuid& deviceUuid) {
//  deviceUuid_ = deviceUuid;
//}
//void setDeviceUuid(      Uuid&& deviceUuid) {
//  deviceUuid_ = std::move(deviceUuid);
//}

  auto outputUuid() const -> const Uuid& {
    return outputUuid_;
  }
  void setOutputUuid(const Uuid&  outputUuid) {
    outputUuid_ =           outputUuid ;
  }
  void setOutputUuid(      Uuid&& outputUuid) {
    outputUuid_ = std::move(outputUuid);
  }

  auto colors() const -> const Colors& {
    return colors_;
  }
  void setColors(const Colors&  colors) {
    colors_ =           colors ;
  }
  void setColors(      Colors&& colors) {
    colors_ = std::move(colors);
  }
  void addColor (const Color &  color ) {
    colors_.push_back(color);
  }

 private:
//Uuid deviceUuid_;
  Uuid outputUuid_;

  Colors colors_;
};

// ----------

class Message::ColorizeResponse : public Message, public Object<ColorizeResponse> {
 public:
  using Construction   = Object<ColorizeResponse>::Construction;

  using ConstSharedPtr = Object<ColorizeResponse>::ConstSharedPtr;
  using      SharedPtr = Object<ColorizeResponse>::     SharedPtr;

  using ConstUniquePtr = Object<ColorizeResponse>::ConstUniquePtr;
  using      UniquePtr = Object<ColorizeResponse>::     UniquePtr;

  template <class... U>
  static SharedPtr constructShared(U&&... args) {
    return Object<ColorizeResponse>::constructShared(std::forward(args)...);
  }

  template <class... U>
  static UniquePtr constructUnique(U&&... args) {
    return Object<ColorizeResponse>::constructUnique(std::forward(args)...);
  }

 public:
  enum class Variant : Message::Variant {
    VVOID = 0x0000,
    V0001 = 0x0001,
  };

 public:
  ColorizeResponse(Construction);

 public:
  static auto create(const Message::Colorize           * message) -> UniquePtr;

  static auto create(const Message::Colorize::UniquePtr& message) -> UniquePtr {
    return create(message.get());
  }
  static auto create(const Message::Colorize::SharedPtr& message) -> UniquePtr {
    return create(message.get());
  }

 public:
  static UniquePtr decodeBytes(Bytes::Decoder& decoder, Message::Variant variant);
  static UniquePtr decodeV0001(Bytes::Decoder& decoder);

  void             encodeBytes(Bytes::Encoder& encoder) const override;
  void             encodeV0001(Bytes::Encoder& encoder) const;

 public:
  String toString() const override;

 public:
//auto deviceUuid() const -> const Uuid& {
//  return deviceUuid_;
//}
//void setDeviceUuid(const Uuid&  deviceUuid) {
//  deviceUuid_ =           deviceUuid ;
//}
//void setDeviceUuid(      Uuid&& deviceUuid) {
//  deviceUuid_ = std::move(deviceUuid);
//}

  auto outputUuid() const -> const Uuid& {
    return outputUuid_;
  }
  void setOutputUuid(const Uuid&  outputUuid) {
    outputUuid_ =           outputUuid ;
  }
  void setOutputUuid(      Uuid&& outputUuid) {
    outputUuid_ = std::move(outputUuid);
  }

  auto colors() const -> const Colors& {
    return colors_;
  }
  void setColors(const Colors&  colors) {
    colors_ =           colors ;
  }
  void setColors(      Colors&& colors) {
    colors_ = std::move(colors);
  }
  void addColor (const Color &  color ) {
    colors_.push_back(color);
  }

 private:
//Uuid deviceUuid_;
  Uuid outputUuid_;

  Colors colors_;
};

}  // namespace ambiramus
}  // namespace lvd
