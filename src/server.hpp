/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <cstdint>
#include <memory>

#include "bytes.hpp"
#include "module.hpp"
#include "object.hpp"
#include "signal.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class Server : public Object<Server>, public Module {
 public:
  Server(Construction);
  ~Server();

 public:
  virtual void setup() override;
  virtual void loop () override;
  virtual void close() override;

 public:
  Signal<void(uint32_t, const Bytes&)> sigMessage;

 private:
  class           Impl;
  std::unique_ptr<Impl> impl_;
};

}  // namespace ambiramus
}  // namespace lvd
