/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "portal_preferences_manager.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "decoder_util.hpp"
#include "encoder_util.hpp"
#include "enum.hpp"
#include "logger.hpp"

// ----------

namespace lvd {
namespace ambiramus {

auto PortalPreferencesManager::load() -> PortalPreferences {
  LOG_D("PortalPreferencesManager")
      << STRREF("load");

  const Bytes& bytes = preferences_->load(
      Preferences::Species::PORTAL_PREFERENCES, 0x0000
  );

  Bytes::Decoder decoder    ( bytes );
  DecoderUtil    decoderUtil(decoder);

  PortalPreferences portalPreferences;

  for (auto subject = decoderUtil.decode<PortalPreferences::Subject>();
            subject.has_value();
            subject = decoderUtil.decode<PortalPreferences::Subject>()) {
    switch (subject.value()) {
      case PortalPreferences::Subject::USERNAME: {
        auto username = decoderUtil.data<decltype(portalPreferences.username_)>();
        if (username.has_value()) {
          portalPreferences.setUsername(username.value());
        }
      } break;

      case PortalPreferences::Subject::PASSWORD: {
        auto password = decoderUtil.data<decltype(portalPreferences.password_)>();
        if (password.has_value()) {
          portalPreferences.setPassword(password.value());
        }
      } break;

      default:
        LOG_W("PortalPreferencesManager")
            << STRREF("unknown subject")
            << enum2type(subject.value());
    }
  }

  return portalPreferences;
}

void PortalPreferencesManager::save(const PortalPreferences& portalPreferences) {
  LOG_D("PortalPreferencesManager")
      << STRREF("save");

  Bytes::Encoder encoder;
  EncoderUtil    encoderUtil(encoder);

  encoder.bytes().reserve(
      + EncoderUtil::METASIZE + Bytes::Encoder::length(portalPreferences.username())
      + EncoderUtil::METASIZE + Bytes::Encoder::length(portalPreferences.password())
  );

  encoderUtil.encode(PortalPreferences::Subject::USERNAME,
                     portalPreferences.username());

  encoderUtil.encode(PortalPreferences::Subject::PASSWORD,
                     portalPreferences.password());

  preferences_->save(
      Preferences::Species::PORTAL_PREFERENCES, 0x0000, encoder.bytes()
  );

  preferences_->commit();
}

}  // namespace ambiramus
}  // namespace lvd
