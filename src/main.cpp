/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "main.hpp"
#include "core.hpp"  // IWYU pragma: keep

#ifdef DEVICE
#include <Arduino.h>
#endif

#include "ambira.hpp"
#include "config.hpp"
#include "persistence.hpp"
#include "preferences.hpp"

// ----------

#ifndef UNIT_TEST

lvd::ambiramus::Ambira::UniquePtr ambira;

void setup() {
#ifdef DEVICE
  delay(1337);
#endif

  LOG_A("Main")
      << lvd::ambiramus::config::AMBIRA_NAME
      << lvd::ambiramus::config::AMBIRA_VERS;

  lvd::ambiramus::Preferences::SharedPtr preferences =
      lvd::ambiramus::Preferences::constructShared(
        lvd::ambiramus::Persistence::instance()
      );

  ambira = lvd::ambiramus::Ambira::constructUnique(preferences);
  ambira->setup();
}

void loop() {
  ambira->loop ();
}

void close() {
  ambira->close();
  ambira = nullptr;
}

#endif
