/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "server_impl.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <cstring>

#include <QByteArray>
#include <QNetworkDatagram>
#include <QUdpSocket>

#include "config.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class Server::Impl::Data {
 public:
  QUdpSocket qUdpSocket;
};

// ----------

Server::Impl::Impl(Server* server)
    : data_(std::make_unique<Data>()),
      server_(server) {}

Server::Impl::~Impl() = default;

void Server::Impl::setup() {
  data_->qUdpSocket.bind(config::SERVER_PORT);
}

void Server::Impl::loop() {
  while (data_->qUdpSocket.hasPendingDatagrams()) {
    auto datagram = data_->qUdpSocket.receiveDatagram();
    auto data     = datagram         .       data    ();

    uint32_t addr = datagram.senderAddress().toIPv4Address();

    std::size_t size = data.size();
    Bytes bytes(size);

    std::memcpy(bytes.data(), data.data(), bytes.size());

    server_->sigMessage(addr, bytes);
  }
}

void Server::Impl::close() {
  data_->qUdpSocket.close();
}

}  // namespace ambiramus
}  // namespace lvd
