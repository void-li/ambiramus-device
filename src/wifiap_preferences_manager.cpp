/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "wifiap_preferences_manager.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "config.hpp"
#include "decoder_util.hpp"
#include "encoder_util.hpp"
#include "enum.hpp"
#include "logger.hpp"

// ----------

namespace lvd {
namespace ambiramus {

auto WifiApPreferencesManager::load() -> WifiApPreferences {
  LOG_D("WifiApPreferencesManager")
      << STRREF("load");

  const Bytes& bytes = preferences_->load(
      Preferences::Species::WIFIAP_PREFERENCES, 0x0000
  );

  Bytes::Decoder decoder    ( bytes );
  DecoderUtil    decoderUtil(decoder);

  WifiApPreferences wifiApPreferences;

  for (auto subject = decoderUtil.decode<WifiApPreferences::Subject>();
            subject.has_value();
            subject = decoderUtil.decode<WifiApPreferences::Subject>()) {
    switch (subject.value()) {
      case WifiApPreferences::Subject::CLIENT_SSID    : {
        auto clientSsid     = decoderUtil.data<decltype(wifiApPreferences.clientSsid_    )>();
        if (clientSsid    .has_value()) {
          wifiApPreferences.setClientSsid    (clientSsid    .value());
        }
      } break;

      case WifiApPreferences::Subject::CLIENT_PASSWORD: {
        auto clientPassword = decoderUtil.data<decltype(wifiApPreferences.clientPassword_)>();
        if (clientPassword.has_value()) {
          wifiApPreferences.setClientPassword(clientPassword.value());
        }
      } break;

      case WifiApPreferences::Subject::ACCESS_SSID    : {
        auto accessSsid     = decoderUtil.data<decltype(wifiApPreferences.accessSsid_    )>();
        if (accessSsid    .has_value()) {
          wifiApPreferences.setAccessSsid    (accessSsid    .value());
        }
      } break;

      case WifiApPreferences::Subject::ACCESS_PASSWORD: {
        auto accessPassword = decoderUtil.data<decltype(wifiApPreferences.accessPassword_)>();
        if (accessPassword.has_value()) {
          wifiApPreferences.setAccessPassword(accessPassword.value());
        }
      } break;

      default:
        LOG_W("WifiApPreferencesManager")
            << STRREF("unknown subject")
            << enum2type(subject.value());
    }
  }

  if (wifiApPreferences.clientSsid    ().empty()) {
    wifiApPreferences.setClientSsid    (String::construct(config::WIFIAP_CLIENT_SSID    ));
  }
  if (wifiApPreferences.clientPassword().empty()) {
    wifiApPreferences.setClientPassword(String::construct(config::WIFIAP_CLIENT_PASSWORD));
  }

  if (wifiApPreferences.accessSsid    ().empty()) {
    wifiApPreferences.setAccessSsid    (String::construct(config::WIFIAP_ACCESS_SSID    ));
  }
  if (wifiApPreferences.accessPassword().empty()) {
    wifiApPreferences.setAccessPassword(String::construct(config::WIFIAP_ACCESS_PASSWORD));
  }

  return wifiApPreferences;
}

void WifiApPreferencesManager::save(const WifiApPreferences& wifiApPreferences) {
  LOG_D("WifiApPreferencesManager")
      << STRREF("save");

  Bytes::Encoder encoder;
  EncoderUtil    encoderUtil(encoder);

  encoder.bytes().reserve(
      + EncoderUtil::METASIZE + Bytes::Encoder::length(wifiApPreferences.clientSsid    ())
      + EncoderUtil::METASIZE + Bytes::Encoder::length(wifiApPreferences.clientPassword())
      + EncoderUtil::METASIZE + Bytes::Encoder::length(wifiApPreferences.accessSsid    ())
      + EncoderUtil::METASIZE + Bytes::Encoder::length(wifiApPreferences.accessPassword())
  );

  encoderUtil.encode(WifiApPreferences::Subject::CLIENT_SSID,
                     wifiApPreferences.clientSsid    ());
  encoderUtil.encode(WifiApPreferences::Subject::CLIENT_PASSWORD,
                     wifiApPreferences.clientPassword());

  encoderUtil.encode(WifiApPreferences::Subject::ACCESS_SSID,
                     wifiApPreferences.accessSsid    ());
  encoderUtil.encode(WifiApPreferences::Subject::ACCESS_PASSWORD,
                     wifiApPreferences.accessPassword());

  preferences_->save(
      Preferences::Species::WIFIAP_PREFERENCES, 0x0000, encoder.bytes()
  );

  preferences_->commit();
}

}  // namespace ambiramus
}  // namespace lvd
