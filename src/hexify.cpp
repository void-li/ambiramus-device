/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "hexify.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include "logger.hpp"

// ----------

namespace lvd {
namespace ambiramus {

auto chrToHex(Byte     value, HexifyCase hexifyCase) -> uint16_t {
  if (hexifyCase == HexifyCase::UPPER_CASE) {
    static const char digits[] = "0123456789ABCDEF";

    int16_t h = digits[value >> 0x04];
    int16_t l = digits[value &  0x0F];

    return (h << 8) | l;
  } else {
    static const char digits[] = "0123456789abcdef";

    int16_t h = digits[value >> 0x04];
    int16_t l = digits[value &  0x0F];

    return (h << 8) | l;
  }
}

auto hexToChr(uint16_t value) -> std::optional<Byte> {
  char h = value >> 0x08;
  char l = value &  0xFF;

  Byte byte = 0;

  for (char c : { h, l }) {
    switch (c) {
      case '0': case '1': case '2': case '3': case '4':
      case '5': case '6': case '7': case '8': case '9':
        byte = (byte << 4) | (c - '0'     );
        break;

      case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
        byte = (byte << 4) | (c - 'A' + 10);
        break;

      case 'a': case 'b': case 'c': case 'd': case 'e': case 'f':
        byte = (byte << 4) | (c - 'a' + 10);
        break;

      default:
        LOG_W("Hexify")
            << STRREF("invalid hex value")
            << c;

        return std::nullopt;
    }
  }

  return byte;
}

auto hexToChr(char h, char l) -> std::optional<Byte> {
  uint16_t /****/ value = 0;

  value |= h;
  value <<= 0x08;
  value |= l;

  return hexToChr(value);
}

}  // namespace ambiramus
}  // namespace lvd
