/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "logger.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <chrono>

#include "logger_impl.hpp"

// ----------

namespace lvd {
namespace ambiramus {

Logger::Logger(Construction)
    : impl_(std::make_unique<Impl>(this)) {}

Logger::SharedPtr
Logger::instance_;

Logger::SharedPtr
Logger::instance() {
  if (!instance_) {
    instance_ = Logger::constructShared();
  }

  return instance_;
}

Logger::LogLevel Logger::logLevel_ = Logger::LogLevel::W;

// ----------

template <>
String toString<Logger::LogLevel>(const Logger::LogLevel& value) {
  switch (value) {
    case Logger::LogLevel::F:
      return STRREF("F");
      break;

    case Logger::LogLevel::C:
      return STRREF("C");
      break;

    case Logger::LogLevel::W:
      return STRREF("W");
      break;

    case Logger::LogLevel::I:
      return STRREF("I");
      break;

    case Logger::LogLevel::D:
      return STRREF("D");
      break;

    default:
      return STRREF(" ");
  }
}

// ----------

Logger::LogProxy::~LogProxy() {
  if (string_.empty()) {
    return;
  }

  if (ourLevel_ <= logLevel_) {
    if (string_.back() == ' ') {
      string_.resize(
          string_.size() - 1
      );
    }

    String prefix;
    prefix.push_back('[');
    prefix.append(toString(std::chrono::system_clock::now()));
    prefix.push_back(']');

    prefix.push_back(' ');

    prefix.append(toString(ourLevel_));

    prefix.push_back(' ');

    auto  lineno = toString(lineno_);
    if (lineno.size() < 4) {
      prefix.insert(prefix.end(), 4 - lineno.size(), ' ');
    }

    prefix.append(lineno);
    prefix.push_back(' ');

    auto& naming =         (naming_);

    prefix.append(naming);
    prefix.push_back(' ');

    Logger::instance()->impl_->log(prefix, string_);
  }
}

void Logger::LogProxy::reset() {
  doquote_ = false;
  noquote_ = false;

  dospace_ = false;
  nospace_ = false;
}

}  // namespace ambiramus
}  // namespace lvd
