/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "server_impl.hpp"
#include "core.hpp"  // IWYU pragma: keep

#include <WiFiUdp.h>

#include "config.hpp"

// ----------

namespace lvd {
namespace ambiramus {

class Server::Impl::Data {
 public:
  WiFiUDP wiFiUDP;
};

// ----------

Server::Impl::Impl(Server* server)
    : data_(std::make_unique<Data>()),
      server_(server) {}

Server::Impl::~Impl() = default;

void Server::Impl::setup() {
  data_->wiFiUDP.begin(config::SERVER_PORT);
}

void Server::Impl::loop() {
  for (int packetSize = data_->wiFiUDP.parsePacket();
           packetSize > 0;
           packetSize = data_->wiFiUDP.parsePacket()) {
    uint32_t addr = data_->wiFiUDP.remoteIP();

    std::size_t size = packetSize;
    Bytes bytes(size);

    data_->wiFiUDP.read(bytes.data(), bytes.size());

    server_->sigMessage(addr, bytes);
  }
}

void Server::Impl::close() {
  data_->wiFiUDP.stop();
}

}  // namespace ambiramus
}  // namespace lvd
