#!/bin/sh
set -e -x

PORTAL=portal.html

HEADER=header.html.hppd
FOOTER=footer.html.hppd

SYNC_SVG=sync.svg.hppd
WIFI_SVG=wifi.svg.hppd

minify                      \
  --html-keep-document-tags \
  --html-keep-end-tags      \
  --html-keep-quotes        \
  -o $PORTAL.tmp $PORTAL

rm -f $HEADER
echo -n 'R"('                                                 >> $HEADER
cat $PORTAL.tmp | tr '\n' ' ' | awk -F  '<body>' '{print $1}' >> $HEADER
echo -n  '<body>'                                             >> $HEADER
echo -n  '<div id="r">'                                       >> $HEADER
echo -n  ')"'                                                 >> $HEADER

rm -f $FOOTER
echo -n 'R"('                                                 >> $FOOTER
echo -n '</div>'                                              >> $FOOTER
echo -n '</body>'                                             >> $FOOTER
cat $PORTAL.tmp | tr '\n' ' ' | awk -F '</body>' '{print $2}' >> $FOOTER
echo -n  ')"'                                                 >> $FOOTER

rm -f $SYNC_SVG
echo -n 'R"('           >> $SYNC_SVG
echo -n $(cat sync.svg) >> $SYNC_SVG
echo -n  ')"'           >> $SYNC_SVG

rm -f $WIFI_SVG
echo -n 'R"('           >> $WIFI_SVG
echo -n $(cat wifi.svg) >> $WIFI_SVG
echo -n  ')"'           >> $WIFI_SVG

rm -f $PORTAL.tmp
